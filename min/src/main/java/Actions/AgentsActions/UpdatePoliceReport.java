package Actions.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 1/20/2016.
 */
@WebServlet("/policeReport")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class UpdatePoliceReport extends HttpServlet {

    Connection connection;
    PreparedStatement statement;
    private static final String SAVE_DIR = "policeReports";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }
        String id = request.getParameter("id");
        Part filePart = request.getPart("file");
        String fileName = extractFileName(filePart);
        filePart.write(savePath + File.separator + fileName);


        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        response.setContentType("text/html");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");


        String query = "UPDATE workers SET police_report = '"+fileName+"' WHERE workers_id = '"+id+"'";

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.prepareStatement(query);
            int out = statement.executeUpdate();

            if(out != 0){
                jsonObject.addProperty("mark",Boolean.TRUE);
            }else{
                jsonObject.addProperty("mark",Boolean.FALSE);
            }
        }catch(Exception ex){
            jsonObject.addProperty("mark", Boolean.FALSE);
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

        writer.println(jsonObject.toString());
    }
}
