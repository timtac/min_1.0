package Actions.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by user on 1/13/2016.
 */
@WebServlet("/editAgentAction")
public class EditAgentAction extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
        doGet(request,response);
    }
    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String username = request.getParameter("username");

        HttpSession session = request.getSession();
        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select * from agents where agent_username = '"+username+"'");

            while(resultSet.next()){
                session.setAttribute("fullname", resultSet.getString("agent_fullName"));
                session.setAttribute("username",resultSet.getString("agent_username"));
                session.setAttribute("company",resultSet.getString("agent_Company_Name"));
            }

            //dispatcher = context.getRequestDispatcher("/updateAgent.jsp");
            //dispatcher.forward(request,response);
            response.sendRedirect("/min/updateAgent.jsp");
        }catch(Exception ex){
            ex.printStackTrace();
        } 
        finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
