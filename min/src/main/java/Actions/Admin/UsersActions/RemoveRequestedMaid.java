package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 2/17/2016.
 */
@WebServlet("/remove")
public class RemoveRequestedMaid extends HttpServlet {
    Connection connection;
    Statement statement;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String maid = request.getParameter("maid");

        JsonObject jsonObject = new JsonObject();
        PrintWriter writer = response.getWriter();


        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();

            statement.executeUpdate("DELETE from requestedMaids where workers_id_id = '"+maid+"' AND users_id_id = '"+username+"'");


        }catch(Exception ex){
            ex.printStackTrace();

        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

        response.sendRedirect("/min/Requests.jsp");
    }
}
