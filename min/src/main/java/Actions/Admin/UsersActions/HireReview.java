package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 1/10/2017.
 */
@WebServlet("/hireReview")
public class HireReview extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();
        String workerId = request.getParameter("workerId");

        String query = "select * from workers w INNER JOIN agents a ON w.agent_Company_Name = a.agent_Company_Name where w.worker_id ='"+workerId+"'";

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                object.addProperty("","");
                object.addProperty("","");
                object.addProperty("","");
                object.addProperty("","");
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doGet(request, response);
    }
}
