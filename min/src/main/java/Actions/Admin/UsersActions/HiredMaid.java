package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/3/2016.
 */
@WebServlet("/hiredMaid")
public class HiredMaid extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");

        String query = "select w.workers_fullname,w.agent_Company_Name, w.worker_occupation,h.hired_date,w.workers_id,h.replaceMaid from hiredMaids h INNER JOIN workers w ON h.workers_id_id = w.workers_id LEFT JOIN users u ON h.users_id_id = u.users_id where h.users_id_id = '"+userId+"' ORDER BY h.hired_date";

        JsonArray jsonArray = new JsonArray();
        PrintWriter writer = response.getWriter();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("workerName",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("compName",resultSet.getString("agent_Company_Name"));
                jsonObject.addProperty("occupation",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("hireDate",resultSet.getString("hired_date"));
                jsonObject.addProperty("workerId",resultSet.getString("workers_id"));
                jsonObject.addProperty("replace",resultSet.getString("replaceMaid"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
