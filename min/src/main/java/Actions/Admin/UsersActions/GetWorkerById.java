package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 6/9/2016.
 */
@WebServlet("/updateWorkerProf")
public class GetWorkerById extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");

        HttpSession session = request.getSession();

        String query="Select * from workers where workers_id = '"+id+"'";
        try{
            RequestDispatcher dispatcher;
            ServletContext context = getServletContext();
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("id",resultSet.getString("workers_id"));
                session.setAttribute("first",resultSet.getString("workers_fullname"));
                session.setAttribute("age",resultSet.getString("worker_age"));
                session.setAttribute("compName",resultSet.getString("agent_Company_Name"));
                session.setAttribute("mobile",resultSet.getString("worker_mobile"));
                session.setAttribute("occupation",resultSet.getString("worker_occupation"));
            }

            dispatcher = context.getRequestDispatcher("/EmployerUpdateWorkerProfile.jsp");
            dispatcher.forward(request,response);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }

}
