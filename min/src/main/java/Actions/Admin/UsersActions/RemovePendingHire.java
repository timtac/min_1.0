package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 6/23/2016.
 */
@WebServlet("/removePendingHire")
public class RemovePendingHire extends HttpServlet {

    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String workerId = request.getParameter("workerId");

        String query = "Delete from pendingHire where workers_id = workerId";

        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            statement.executeUpdate("update workers set employed = 'Unemployed' where workers_id ='"+workerId+"'");


        }catch (Exception ex){
            ex.printStackTrace();
        }

        response.sendRedirect("/min/PendingHire.jsp");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
