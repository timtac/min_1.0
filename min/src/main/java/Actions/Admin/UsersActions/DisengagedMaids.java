package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/6/2016.
 */
@WebServlet("/disengagedMaids")
public class DisengagedMaids extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user_id = request.getParameter("userId");

        JsonArray jsonArray = new JsonArray();
        PrintWriter writer = response.getWriter();

        System.out.println(user_id);

        String query = "Select w.workers_fullname,w.workers_id,d.date_disengaged, w.worker_occupation from disengagedmaid d INNER JOIN workers w ON d.worker_id_id = w.workers_id LEFT JOIN users u ON d.users_id_id = u.users_id where d.users_id_id = '"+user_id+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("workerId",resultSet.getString("workers_id"));
                jsonObject.addProperty("workerName",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("occupation",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("dateDisengaged",resultSet.getString("date_disengaged"));

                jsonArray.add(jsonObject);

            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
