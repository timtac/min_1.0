package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 6/15/2016.
 */
@WebServlet("/confirmReplace")
public class ConfirmReplace extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String workerId = request.getParameter("workerId");

        String query = "select * from workers w inner join hiredMaids h on h.workers_id_id = w.workers_id where w.workers_id = '"+workerId+"'";

        HttpSession session = request.getSession();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("workerId",resultSet.getString("workers_id"));
                session.setAttribute("workername",resultSet.getString("workers_fullname"));
                session.setAttribute("occu",resultSet.getString("worker_occupation"));
                session.setAttribute("agentName",resultSet.getString("agent_Company_Name"));
                session.setAttribute("hiredDate",resultSet.getString("hired_date"));
            }

        } catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/ConfirmReplace.jsp");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
