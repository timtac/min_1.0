package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 3/16/2016.
 */
@WebServlet("/home")
public class Home extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        HttpSession session = request.getSession();
        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from users where users_username = '"+username+"'");


            while(resultSet.next()){
                session.setAttribute("Userfullname",resultSet.getString("users_fullname"));
                session.setAttribute("location",resultSet.getString("users_location"));
                session.setAttribute("userImage",resultSet.getString("user_image"));

            }



        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        response.sendRedirect("/min/UserHomePage.jsp");
    }

}
