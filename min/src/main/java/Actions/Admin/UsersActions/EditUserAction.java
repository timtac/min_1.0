package Actions.Admin.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
/**
 * Created by user on 1/13/2016.
 */
@WebServlet("/editUserAction")
public class EditUserAction extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        HttpSession session = request.getSession();
        try{
            RequestDispatcher dispatcher;
            ServletContext context = getServletContext();

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select * FROM users WHERE users_username = '"+username+"'");

            while(resultSet.next()){
                session.setAttribute("full", resultSet.getString("users_fullname"));
                session.setAttribute("username",resultSet.getString("users_username"));
            }
            dispatcher = context.getRequestDispatcher("/updateUser.jsp");
            dispatcher.forward(request,response);

        }
        catch (Exception ex){
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
