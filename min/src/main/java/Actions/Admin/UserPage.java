package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 4/9/2016.
 */
@WebServlet("/userPage")
public class UserPage extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from users where users_username = '"+username+"'");

            while(resultSet.next()){
                session.setAttribute("fullname",resultSet.getString("users_fullname"));
                session.setAttribute("user",resultSet.getString("users_username"));
                session.setAttribute("location",resultSet.getString("users_location"));
                session.setAttribute("email",resultSet.getString("user_email"));
                session.setAttribute("mobile",resultSet.getString("user_mobile"));
                session.setAttribute("image",resultSet.getString("user_image"));
                session.setAttribute("title",resultSet.getString("user_title"));
                session.setAttribute("lastSeen",resultSet.getString("lastSeen"));
                session.setAttribute("signUp",resultSet.getString("signupDate"));
            }

            response.sendRedirect("/min/UserPage.jsp");
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
