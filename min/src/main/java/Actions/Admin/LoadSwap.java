package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 7/12/2016.
 */
@WebServlet("/loadSwap")
public class LoadSwap extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
        String id = request.getParameter("newId");

        String query = "select * from workers where workers_id = '"+id+"'";
        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();

        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id",resultSet.getString("workers_id"));
                jsonObject.addProperty("name",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("agentName",resultSet.getString("agent_Company_Name"));
                jsonObject.addProperty("occupation",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("mobile",resultSet.getString("worker_mobile"));
                jsonObject.addProperty("age",resultSet.getString("worker_age"));
                jsonObject.addProperty("sex",resultSet.getString("worker_sex"));
                jsonObject.addProperty("agency_fee",resultSet.getString("agency_fee"));
                jsonObject.addProperty("fee_type",resultSet.getString("agency_fee_type"));
                jsonObject.addProperty("salary",resultSet.getString("expected_salary"));
                System.out.println(resultSet.getString("workers_id"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
        doPost(request, response);
    }
}
