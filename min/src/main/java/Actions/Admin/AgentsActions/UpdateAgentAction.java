package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Created by user on 1/13/2016.
 */
@WebServlet("/updateAgentProfile")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class UpdateAgentAction extends HttpServlet {

    Connection connection;
    Statement statement;

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String username= request.getParameter("username");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

        String query = "UPDATE agents SET agent_image = '"+fileName+"' WHERE agent_username = '"+username+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            int out = statement.executeUpdate(query);

            if(out != 0){
                response.sendRedirect("/min/uploadConfirmation.jsp");
            }
            else{
                jsonObject.addProperty("success",false);
            }
        }catch(Exception ex){
            jsonObject.addProperty("success",false);
        }
        finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(jsonObject.toString());
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
