package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 4/7/2016.
 */
@WebServlet("/agentHome")
public class AgentHome extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("compName");
        HttpSession session = request.getSession();
        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from agents where agent_Company_Name = '"+username+"'");


            while(resultSet.next()){
                session.setAttribute("company",resultSet.getString("agent_Company_Name"));
                session.setAttribute("location",resultSet.getString("agents_location"));
                session.setAttribute("mail",resultSet.getString("agent_email"));
                session.setAttribute("agentImage",resultSet.getString("agent_image"));
                session.setAttribute("username",resultSet.getString("agent_username"));
            }



        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        response.sendRedirect("/min/AllWorkers.jsp");
    }
}
