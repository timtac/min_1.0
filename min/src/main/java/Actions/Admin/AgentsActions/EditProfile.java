package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 4/10/2016.
 */
@WebServlet("/editAgentProfile")
public class EditProfile extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        String query = "SELECT * from agents where agent_username = '"+username+"'";

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("fullname",resultSet.getString("agent_fullName"));
                session.setAttribute("company",resultSet.getString("agent_Company_Name"));
                session.setAttribute("e_mail",resultSet.getString("agent_email"));
                session.setAttribute("address",resultSet.getString("company_address"));
                session.setAttribute("phone",resultSet.getString("agent_mobile"));

            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/EditAgentProfile.jsp");
    }
}
