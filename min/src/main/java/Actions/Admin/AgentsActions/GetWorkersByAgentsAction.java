package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 1/11/2016.
 */
@WebServlet("/getAgentsWorkers")
public class GetWorkersByAgentsAction extends HttpServlet{
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String agentCompName = request.getParameter("agentCompName");

            PrintWriter out = response.getWriter();
            JsonArray jsonArray = new JsonArray();

        String query = "select * from workers w left join hiredMaids h on w.workers_id = h.workers_id_id left join users u on h.users_id_id = u.users_id where w.agent_Company_Name = '"+agentCompName+"' ORDER BY h.hired_date DESC ";
        try{
            
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            resultSet = statement.executeQuery(query);

            System.out.println(agentCompName);
            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id",resultSet.getString("workers_id"));
                jsonObject.addProperty("first",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("age",resultSet.getString("worker_age"));
                jsonObject.addProperty("occupation",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("location",resultSet.getString("worker_location"));
                jsonObject.addProperty("phone",resultSet.getString("worker_mobile"));
                jsonObject.addProperty("sex",resultSet.getString("worker_sex"));
                jsonObject.addProperty("employer",resultSet.getString("users_fullname"));
                jsonObject.addProperty("employed",resultSet.getString("employed"));
                jsonObject.addProperty("date",resultSet.getString("hired_date"));

                jsonArray.add(jsonObject);
            }
                
            //dispatcher = context.getRequestDispatcher("/AllWorkers.jsp");
            //dispatcher.forward(request,response);
        }
        catch(Exception ex){
             ex.printStackTrace();
        }finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        out.println(jsonArray.toString());
    }
}
