package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import EncrytHelper.EncrytoClass;
import Mailer.MailHelper;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 4/4/2016.
 */
@WebServlet("/resetPsw")
public class ResetPassword extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user = request.getParameter("user");
        String token = request.getParameter("token");

        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        try{
            EncrytoClass encrytoClass = new EncrytoClass();
            String encToken = encrytoClass.encrypt(user,token);
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeQuery("update agent set agent_password = '"+encToken+"' where agent_username = '"+user+"'");

            resultSet = statement.executeQuery("SELECT agent_email FROM agents where agent_username = '"+user+"'");

            while (resultSet.next()){
                MailHelper mailHelper = new MailHelper();
                mailHelper.sendMessage(resultSet.getString("agent_username"),"This is to notify you that your password has been changed successfully","Password Change");
            }

            response.sendRedirect("/min/changepassword.jsp");

        }catch (Exception ex){
            ex.printStackTrace();
            //response.sendRedirect("/min/Error500.jsp");
        }

        response.sendRedirect("/min/changepassword.jsp");
    }
}
