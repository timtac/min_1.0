package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

/**
 * Created by user on 4/10/2016.
 */
@WebServlet("/agentSaveProfile")
public class SaveProfile extends HttpServlet {
    Connection connection;
    Statement statement;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String company = request.getParameter("company");

        String query = "UPDATE agents set agent_fullName = '"+fullname+"',agent_Company_Name = '"+company+"', company_address = '"+address+"', agent_email = '"+email+"', agent_mobile = '"+mobile+"' where agent_username = '"+username+"'";

        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            response.sendRedirect("/min/AgentHomePage.jsp");
        } catch (Exception ex){
            ex.printStackTrace();
            object.addProperty("success",false);
        }
    }
}
