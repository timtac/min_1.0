package Actions.Admin.AgentsActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;


/**
 * Created by user on 1/18/2016.
 */
@WebServlet("/deleteWorker")
public class DeleteWorkerInfo extends HttpServlet {
    Connection connection;
    PreparedStatement statement;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        
        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();

        String query = "DELETE FROM workers w INNER JOIN hiredMaids h ON h.workers_id_id = w.workers_id INNER JOIN requestedMaids r ON r.workers_id_id = w.workers_id INNER JOIN pendingHire p ON p.workers_id = w.workers_id INNER JOIN disengagedmaid d ON d.worker_id_id = w.workers_id WHERE w.workers_id = '"+id+"'";

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.prepareStatement(query);
            int out = statement.executeUpdate();
            if(out ==1){
                jsonObject.addProperty("status",true);
            }
            else{
                jsonObject.addProperty("status",false);
            }
        }catch(Exception ex){
            jsonObject.addProperty("status",false);
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(jsonObject.toString());
    }
}
