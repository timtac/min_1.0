package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.sql.*;

/**
 * Created by user on 4/13/2016.
 */
@WebServlet("/deleteUnassign")
public class deleteUnassigned extends HttpServlet {

    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");

        String query="Delete from unassignworkers where workerId = '"+id+"'";

        JsonObject jsonObject = new JsonObject();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            response.sendRedirect("/min/adminPage.jsp");
        }catch(Exception ex){
            jsonObject.addProperty("status",false);
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
