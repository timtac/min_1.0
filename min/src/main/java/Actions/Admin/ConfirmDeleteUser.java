package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 7/23/2016.
 */
@WebServlet("/deleteUser")
public class ConfirmDeleteUser extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        String query="SELECT * from users where users_username = '"+username+"'";
        HttpSession session = request.getSession();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("fullname",resultSet.getString("users_fullName"));
                session.setAttribute("title",resultSet.getString("users_title"));
                session.setAttribute("signUp",resultSet.getString("signupDate"));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        session.setAttribute("user_username",username);
        response.sendRedirect("/min/ConfirmDeleteUser.jsp");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
