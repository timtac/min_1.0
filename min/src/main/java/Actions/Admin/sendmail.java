package Actions.Admin;

import Mailer.SimpleMailSender;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 4/12/2016.
 */
@WebServlet("/sendMail")
public class sendmail extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mail = request.getParameter("e_mail");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        try{
           // MailHelper mailHelper = new MailHelper();
            //mailHelper.sendMessage(mail,message,subject);
           // SendGridMailer mailer = new SendGridMailer();
           // mailer.sendGridMessage(mail,subject,message);

            SimpleMailSender sender = new SimpleMailSender();
            sender.sendMessage(mail,subject,message);

            jsonObject.addProperty("sent",true);
        }catch (Exception ex){
            ex.printStackTrace();
            jsonObject.addProperty("sent",false);
        }
        writer.println(jsonObject.toString());
    }
}
