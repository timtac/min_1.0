package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 4/9/2016.
 */
@WebServlet("/agentPage")
public class AgentPage extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String compName = request.getParameter("compName");

        String query = "select * from agents where agent_Company_Name = '"+compName+"'";

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("first",resultSet.getString("agent_fullName"));
                session.setAttribute("company",resultSet.getString("agent_Company_Name"));
                session.setAttribute("user",resultSet.getString("agent_username"));
                session.setAttribute("email",resultSet.getString("agent_email"));
                session.setAttribute("address",resultSet.getString("company_address"));
                session.setAttribute("mobile",resultSet.getString("agent_mobile"));
                session.setAttribute("image",resultSet.getString("agent_image"));
                session.setAttribute("signUp",resultSet.getString("signupDate"));
                session.setAttribute("lastSeen",resultSet.getString("lastSeen"));
                session.setAttribute("title",resultSet.getString("title"));
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/AgentPage.jsp");
    }
}
