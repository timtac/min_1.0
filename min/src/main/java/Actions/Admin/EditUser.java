package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.sql.*;
/**
 * Created by user on 4/22/2016.
 */
@WebServlet("/editUser")
public class EditUser extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");

        String query = "select * from users where users_username = '"+username+"'";

        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("user_fullname", resultSet.getString("users_fullname"));
                session.setAttribute("user",resultSet.getString("users_username"));
                session.setAttribute("gender", resultSet.getString("users_gender"));
                session.setAttribute("add", resultSet.getString("users_address"));
                session.setAttribute("email", resultSet.getString("user_email"));
                session.setAttribute("mobile", resultSet.getString("user_mobile"));
                session.setAttribute("location", resultSet.getString("users_location"));
            }
            response.sendRedirect("/min/AdminEditUser.jsp");

        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
