package Actions.Admin.statistics;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 7/13/2016.
 */
@WebServlet("/totalWorkers")
public class TotalWorkers extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String query = "SELECT count(workers_id) from workers";
        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("count",resultSet.getString("count(workers_id)"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){ex.printStackTrace();}
        writer.println(jsonArray.toString());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
