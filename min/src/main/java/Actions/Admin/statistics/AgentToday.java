package Actions.Admin.statistics;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 7/9/2016.
 */
@WebServlet("/agentToday")
public class AgentToday extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Calendar date = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String regdate = sdf.format(date.getTime());


        String query = "Select count(agent_Company_Name) from agents where signupDate = '"+regdate+"'";

        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("number",resultSet.getString("count(agent_Company_Name)"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
