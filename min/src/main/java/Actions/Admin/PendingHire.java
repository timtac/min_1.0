package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/19/2016.
 */
@WebServlet("/pendingHires")
public class PendingHire extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = "SELECT * from pendingHire p inner JOIN workers w on p.workers_id = w.workers_id LEFT JOIN users u ON p.users_id = u.users_id";

        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("empId",resultSet.getString("users_id"));
                jsonObject.addProperty("empName",resultSet.getString("users_fullname"));
                jsonObject.addProperty("workerId",resultSet.getString( "workers_id"));
                jsonObject.addProperty("workername",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("agency",resultSet.getString("agent_Company_Name"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
