package Actions.Admin;

import Dao.Workers_dao;
import Models.Workers;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 1/21/2016.
 */
@WebServlet("/adminAllWorkers")
public class AllAgentsWorkers extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException,IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();
        try{
            Workers_dao dao = new Workers_dao();
            List<Workers> list = dao.getAllWorkers();

            for(int i=0;i < list.size();i++){
                Workers workers = list.get(i);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id",workers.getWorkers_id());
                jsonObject.addProperty("first",workers.getWorkers_fullname());
                jsonObject.addProperty("agent",workers.getAgent_Company_Name().getAgent_Company_Name());
                jsonObject.addProperty("occupation",workers.getWorker_occupation());
                jsonObject.addProperty("sex",workers.getWorker_sex());
                jsonObject.addProperty("age",workers.getWorker_age());
                //jsonObject.addProperty("employer",workers.getEmployer());
                jsonObject.addProperty("employed",workers.getEmployed());

                jsonArray.add(jsonObject);
            }

        }catch(Exception ex) {
            ex.printStackTrace();
        }
        writer.println(jsonArray.toString());
    }
}
