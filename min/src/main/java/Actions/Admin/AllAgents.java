package Actions.Admin;

import Dao.Agents_dao;
import Models.Agents;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 1/21/2016.
 */
@WebServlet("/allAgents")
public class AllAgents extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException,IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();
        try{

            Agents_dao dao = new Agents_dao();
            List<Agents> list = dao.allAgents();
            for(int i=0;i < list.size();i++){
                Agents agents = list.get(i);

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("title",agents.getTitle());
                jsonObject.addProperty("gender",agents.getGender());
                jsonObject.addProperty("first",agents.getAgent_fullName());
                jsonObject.addProperty("company",agents.getAgent_Company_Name());
                jsonObject.addProperty("username",agents.getAgent_username());
                jsonObject.addProperty("password",agents.getAgent_password());
                jsonObject.addProperty("location",agents.getAgents_location());
                jsonObject.addProperty("address",agents.getCompany_address());
                jsonObject.addProperty("phone",agents.getAgent_mobile());
                jsonObject.addProperty("email",agents.getAgent_email());

                jsonArray.add(jsonObject);
            }

        }catch(Exception ex){

        }
        writer.println(jsonArray.toString());
    }
    
}
