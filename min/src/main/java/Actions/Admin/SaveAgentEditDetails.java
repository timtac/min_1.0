package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.sql.*;
/**
 * Created by user on 4/22/2016.
 */
@WebServlet("/adminSaveAgentDetails")
public class SaveAgentEditDetails extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("usernam");
        String compName = request.getParameter("comp_name");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");

        String query = "update agents set agent_fullName = '"+fullname+"', agent_Company_Name = '"+compName+"', company_address = '"+address+"',agent_email = '"+email+"', agent_mobile = '"+mobile+"' where agent_username = '"+username+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

        response.sendRedirect("/min/adminPage.jsp");
    }
}
