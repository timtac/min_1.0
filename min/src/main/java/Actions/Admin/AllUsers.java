package Actions.Admin;

import Dao.Users_dao;
import Models.Users;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 2/10/2016.
 */
@WebServlet("/allUsers")
public class AllUsers extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws  ServletException,IOException {
        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();
        try{
            Users_dao dao = new Users_dao();
            List<Users> list = dao.getUsers();
            for(int i =0;i<list.size(); i++){
                Users users = list.get(i);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id",users.getUsers_id());
                jsonObject.addProperty("title",users.getUser_title());
                jsonObject.addProperty("gender",users.getUsers_gender());
                jsonObject.addProperty("first",users.getUsers_fullname());
                jsonObject.addProperty("username",users.getUsers_username());
                jsonObject.addProperty("password",users.getUsers_password());
                jsonObject.addProperty("email",users.getUser_email());
                jsonObject.addProperty("phone",users.getUser_mobile());
                jsonObject.addProperty("location",users.getUsers_location());

                jsonArray.add(jsonObject);
            }
        }catch(Exception ex){
               ex.printStackTrace();
        }
        writer.println(jsonArray.toString());
    }
}
