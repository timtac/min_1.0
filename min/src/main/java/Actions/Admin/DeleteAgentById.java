package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 4/4/2016.
 */
@WebServlet("/deleteAgentConfirmed")
public class DeleteAgentById extends HttpServlet {
    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();

        String query = "DELETE FROM agents a INNER  JOIN workers w ON a.agent_Company_Name = w.agent_Company_Name WHERE agent_username = '"+username+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            response.sendRedirect("/min/adminPage.jsp");
        }catch(Exception ex){
            jsonObject.addProperty("status",false);
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

    }
}
