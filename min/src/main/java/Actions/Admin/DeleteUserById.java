package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 4/4/2016.
 */
@WebServlet("/deleteUserConfirmed")
public class DeleteUserById extends HttpServlet {
    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("username");

        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();

        String query = "DELETE FROM users u INNER JOIN hiredMaids h on h.users_id_id = u.users_id INNER JOIN requestedMaids r ON r.users_id_id = u.users_id INNER JOIN disengagedmaid d ON d.users_id_id = u.users_id INNER JOIN pendingHire p ON p.users_id = u.users_id WHERE u.users_username = '"+id+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            response.sendRedirect("/min/adminPage.jsp");
        }catch(Exception ex){
            jsonObject.addProperty("status",false);
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
