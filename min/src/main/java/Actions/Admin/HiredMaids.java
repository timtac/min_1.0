package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/18/2016.
 */
@WebServlet("/adminHiredStaffs")
public class HiredMaids extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String query = "SELECT * from workers w, users u,hiredMaids h where h.workers_id_id = w.workers_id and h.users_id_id = u.users_id";

        JsonArray jsonArray = new JsonArray();
        PrintWriter writer = response.getWriter();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("workerName",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("agency",resultSet.getString("agent_Company_Name"));
                jsonObject.addProperty("age",resultSet.getString("worker_age"));
                jsonObject.addProperty("sex",resultSet.getString("worker_sex"));
                jsonObject.addProperty("occu",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("add",resultSet.getString("worker_address"));
                jsonObject.addProperty("mob",resultSet.getString("worker_mobile"));
                jsonObject.addProperty("sal",resultSet.getString("expected_salary"));
                jsonObject.addProperty("agentFee",resultSet.getString("agency_fee"));
                jsonObject.addProperty("type",resultSet.getString("agency_fee_type"));
                jsonObject.addProperty("userFullname",resultSet.getString("users_fullname"));
                jsonObject.addProperty("userMob",resultSet.getString("user_mobile"));
                jsonObject.addProperty("hire",resultSet.getString("hired_date"));


                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
