package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.sql.*;
/**
 * Created by user on 4/22/2016.
 */
@WebServlet("/adminSaveUserEdit")
public class SaveUserEditDetails extends HttpServlet {

    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String gender = request.getParameter("gender");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String username = request.getParameter("user");
        String location =  request.getParameter("location");

        String query = "update users set users_fullname = '"+fullname+"',users_gender = '"+gender+"',users_address = '"+address+"'," +
                "user_email = '"+email+"',user_mobile = '"+mobile+"', users_location = '"+location+"' where users_username = '"+username+"'";


        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            int out = statement.executeUpdate(query);


        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

        response.sendRedirect("/min/adminPage.jsp");
    }
}
