package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.sql.*;
/**
 * Created by user on 4/19/2016.
 */
@WebServlet("/saveWorkerEdit")
public class SaveWorkerEditedDetails extends HttpServlet {

    Connection connection;
    Statement statement;


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("first");
        String age = request.getParameter("age");
        String mobile = request.getParameter("mobile");
        String occupation = request.getParameter("occupation");
        String compName = request.getParameter("compName");
        String agencyFee = request.getParameter("agencyFee");
        String salary = request.getParameter("expectedSalary");


        String updatQuery = "UPDATE workers set workers_fullname = '"+name+"', worker_age = '"+age+"',worker_mobile = '"+mobile+"'," +
                "worker_occupation = '"+occupation+"',agent_Company_Name = '"+compName+"', agency_fee = '"+agencyFee+"', expected_salary = '"+salary+"' where workers_id = '"+id+"'";
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            int out = statement.executeUpdate(updatQuery);
            if(out != 0){
                response.sendRedirect("/min/AdminAllWorkers.jsp");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
