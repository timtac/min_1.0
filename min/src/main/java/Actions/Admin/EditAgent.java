package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 4/22/2016.
 */
@WebServlet("/editAgent")
public class EditAgent extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");

        String query = "select * from agents where agent_username = '"+username+"'";

        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        HttpSession session = request.getSession();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("compName", resultSet.getString("agent_Company_Name"));
                session.setAttribute("fullname", resultSet.getString("agent_fullName"));
                session.setAttribute("name", username);
                session.setAttribute("location", resultSet.getString("agents_location"));
                session.setAttribute("address", resultSet.getString("company_address"));
                session.setAttribute("email", resultSet.getString("agent_email"));
                session.setAttribute("mobile", resultSet.getString("agent_mobile"));
            }
            response.sendRedirect("/min/AdminEditAgent.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
        }finally{

        }

    }
}
