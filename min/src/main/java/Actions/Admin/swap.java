package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 7/5/2016.
 */
@WebServlet("/swap")
public class swap extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String oldId = request.getParameter("workerId");
        String newId = request.getParameter("newWorkerId");

        String query0 ="update workers set workers_id ='0' where workers_id = '"+newId+"'";
        String query1="update workers set workers.workers_id = '"+newId+"', employed = 'Unemployed' where workers.workers_id = '"+oldId+"'";
        String query2="update workers set workers_id = '"+oldId+"' where workers_id = '0'";

        HttpSession session = request.getSession();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query0);
            statement.executeUpdate(query1);
            statement.executeUpdate(query2);
            resultSet = statement.executeQuery("SELECT * from workers where workers_id = '"+oldId+"'");

            while(resultSet.next()){
                session.setAttribute("id",resultSet.getString("workers_id"));
                session.setAttribute("name",resultSet.getString("workers_fullname"));
                session.setAttribute("agentName",resultSet.getString("agent_Company_Name"));
                session.setAttribute("occupation",resultSet.getString("worker_occupation"));
                session.setAttribute("mobile",resultSet.getString("worker_mobile"));
                session.setAttribute("age",resultSet.getString("worker_age"));
                session.setAttribute("sex",resultSet.getString("worker_sex"));
                session.setAttribute("agency_fee",resultSet.getString("agency_fee"));
                session.setAttribute("fee_type",resultSet.getString("agency_fee_type"));
                session.setAttribute("salary",resultSet.getString("expected_salary"));
                System.out.println(resultSet.getString("workers_id"));
                System.out.println(resultSet.getString("workers_fullname"));
            }

            //session.setAttribute("oldId",oldId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/AuthoriseHire.jsp");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
