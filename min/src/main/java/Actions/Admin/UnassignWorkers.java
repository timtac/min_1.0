package Actions.Admin;

import Dao.WorkerDao;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.List;

/**
 * Created by user on 4/7/2016.
 */
@WebServlet("/unassign")
public class UnassignWorkers extends HttpServlet {
    Connection connection;
    Statement statement;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();

        try{
            WorkerDao workerDao = new WorkerDao();
            List<Models.UnassignWorkers> list = workerDao.getAllWorker();

            for(int i = 0;i < list.size();i++){
                Models.UnassignWorkers unassignWorkers = list.get(i);
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("id",unassignWorkers.getWorkerId());
                jsonObject.addProperty("name",unassignWorkers.getName());
                jsonObject.addProperty("location",unassignWorkers.getLocation());
                jsonObject.addProperty("occupation",unassignWorkers.getOccupation());
                jsonObject.addProperty("email",unassignWorkers.getAddress());
                jsonObject.addProperty("gender",unassignWorkers.getGender());
                jsonObject.addProperty("mobile",unassignWorkers.getMobile());
                jsonObject.addProperty("age",unassignWorkers.getImage());

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        writer.println(jsonArray.toString());
    }
}
