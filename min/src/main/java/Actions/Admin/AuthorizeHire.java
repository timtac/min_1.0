package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Dao.HiredMaidDao;
import Dao.Users_dao;
import Dao.Workers_dao;
import Models.HiredMaid;
import Models.Users;
import Models.Workers;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 6/19/2016.
 */
@WebServlet("/authoriseHire")
public class AuthorizeHire extends HttpServlet{

    Connection connection;
    Statement statement;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String workerId = request.getParameter("workerId");

        String query = "update workers set employed = 'employed' where workers_id = '"+workerId+"'";

        //java.util.Date date = new java.util.Date();
        Calendar date = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String dateEmployed = sdf.format(date.getTime());
        System.out.println(dateEmployed);

        date.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        date.add(Calendar.YEAR,1);
        sdf = new SimpleDateFormat("dd.MM.yyyy");
        String Duedate = sdf.format(date.getTime());
        System.out.println(Duedate);

        JsonObject jsonObject = new JsonObject();
        PrintWriter writer = response.getWriter();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);
            statement.executeUpdate("DELETE from pendingHire where users_id = '"+userId+"' and workers_id = '"+workerId+"'");

            Users users = new Users();
            users.setUsers_id(Integer.parseInt(userId));
            Users_dao usersDao = new Users_dao(users);
            usersDao.refreshUsers();

            Workers workers = new Workers();
            workers.setWorkers_id(Integer.parseInt(workerId));
            Workers_dao workersDao = new Workers_dao(workers);
            workersDao.refreshWorker();

            HiredMaid hiredMaid = new HiredMaid();
            hiredMaid.setUsers_id(users);
            hiredMaid.setWorkers_id(workers);
            hiredMaid.setHired_date(dateEmployed);
            hiredMaid.setDue_date(Duedate);
            HiredMaidDao hiredMaidDao = new HiredMaidDao(hiredMaid);
            hiredMaidDao.createHiredMaid();

            response.sendRedirect("/min/AdminConfirmation.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
            jsonObject.addProperty("mark",false);
        }
        writer.println(jsonObject.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
