package Actions.Admin;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 7/5/2016.
 */
@WebServlet("/review")
public class ReviewHire extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String userId = request.getParameter("super");

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from workers where workers_id= '"+id+"'");

            while(resultSet.next()){
                session.setAttribute("id",resultSet.getString("workers_id"));
                session.setAttribute("name",resultSet.getString("workers_fullname"));
                session.setAttribute("agentName",resultSet.getString("agent_Company_Name"));
                session.setAttribute("occupation",resultSet.getString("worker_occupation"));
                session.setAttribute("mobile",resultSet.getString("worker_mobile"));
                session.setAttribute("age",resultSet.getString("worker_age"));
                session.setAttribute("sex",resultSet.getString("worker_sex"));
                session.setAttribute("agency_fee",resultSet.getString("agency_fee"));
                session.setAttribute("fee_type",resultSet.getString("agency_fee_type"));
                session.setAttribute("salary",resultSet.getString("expected_salary"));
                session.setAttribute("employed",resultSet.getString("employed"));

            }
            session.setAttribute("userId",userId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }

        response.sendRedirect("/min/AuthoriseHire.jsp");
    }

}
