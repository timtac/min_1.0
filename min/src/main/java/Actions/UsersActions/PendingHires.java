package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/21/2016.
 */
@WebServlet("/userPendingHires")
public class PendingHires extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String query = "select * from pendingHire p INNER JOIN workers w ON p.workers_id = w.workers_id where p.users_id = '"+userId+"' ORDER BY p.pending_id";

        PrintWriter writer = response.getWriter();
        JsonArray jsonArray = new JsonArray();

        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            //System.out.println(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("workerId",resultSet.getString("workers_id"));
                jsonObject.addProperty("work",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("agency",resultSet.getString("agent_Company_Name"));
                jsonObject.addProperty("employed",resultSet.getString("employed"));

                jsonArray.add(jsonObject);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
