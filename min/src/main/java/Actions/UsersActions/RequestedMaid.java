package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 6/2/2016.
 */
@WebServlet("/requestedMaid")
public class RequestedMaid extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");

        String query = "select w.workers_fullname,w.workers_id, w.worker_occupation,w.agent_Company_Name from requestedMaids r inner JOIN workers w ON r.workers_id_id = w.workers_id LEFT JOIN users u ON r.users_id_id = u.users_id where r.users_id_id = '"+userId+"' ORDER BY r.request_id";

        JsonArray jsonArray = new JsonArray();
        PrintWriter writer = response.getWriter();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("workersName",resultSet.getString("workers_fullname"));
                jsonObject.addProperty("occupation",resultSet.getString("worker_occupation"));
                jsonObject.addProperty("workerId",resultSet.getString("workers_id"));
                jsonObject.addProperty("agent",resultSet.getString("agent_Company_Name"));

                jsonArray.add(jsonObject);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        writer.println(jsonArray.toString());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
