package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Part;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by user on 1/13/2016.
 */
@WebServlet("/updateUser")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class UpdateUserAction extends HttpServlet {
    Connection connection;
    Statement statement;

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String username = request.getParameter("username");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

        String query = "UPDATE users SET user_image = '"+fileName+"' WHERE users_username = '"+username+"'";

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();

            statement.executeUpdate(query);

            response.sendRedirect("/min/uploadConfirmation.jsp");

        }catch (Exception ex){
            jsonObject.addProperty("success",false);
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(jsonObject.toString());
    }
}
