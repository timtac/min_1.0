package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;

/**
 * Created by user on 6/5/2016.
 */
@WebServlet("/writeReference")
public class WriteReference extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String workerId = request.getParameter("workerName");

        String query = "select w.workers_fullname,w.workers_id,w.agent_Company_Name,w.worker_occupation,h.hired_date from workers w LEFT JOIN hiredMaids h ON w.workers_id = h.workers_id_id LEFT JOIN users u ON h.users_id_id = u.users_id where h.workers_id_id = '"+workerId+"'";

        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            java.util.Date date = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String date_disengaged = sdf.format(date);

            while(resultSet.next()){
                session.setAttribute("workId",resultSet.getString("workers_id"));
                session.setAttribute("funame",resultSet.getString("workers_fullname"));
                session.setAttribute("agent",resultSet.getString("agent_Company_Name"));
                session.setAttribute("occution",resultSet.getString("worker_occupation"));
                session.setAttribute("dateEmyed",resultSet.getString("hired_date"));
                session.setAttribute("dateDisgaged",date_disengaged);

            }
            response.sendRedirect("/min/writeReference.jsp");
        } catch (Exception ex){
            ex.printStackTrace();
        }


    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
