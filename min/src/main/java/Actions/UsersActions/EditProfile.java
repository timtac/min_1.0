package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.io.PrintWriter;
import java.sql.*;
/**
 * Created by user on 4/10/2016.
 */
@WebServlet("/editProfile")
public class EditProfile extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        String query = "SELECT * from users where users_id = '"+username+"'";

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("fullname",resultSet.getString("users_fullname"));
                session.setAttribute("username",resultSet.getString("users_username"));
                session.setAttribute("e_mail",resultSet.getString("user_email"));
                session.setAttribute("phone",resultSet.getString("user_mobile"));
                session.setAttribute("userImage",resultSet.getString("user_image"));
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/EditProfile.jsp");
    }
}
