package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 6/9/2016.
 */
@WebServlet("/reviewDisengaged")
public class ReviewDisengaged extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        String workerId = request.getParameter("workerId");

        String query = "Select * from workers w LEFT JOIN hiredMaids h ON h.workers_id_id = w.workers_id where workers_id = '"+workerId+"'";

        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){
                session.setAttribute("workerId",resultSet.getString("workers_id"));
                session.setAttribute("fullname",resultSet.getString("workers_fullname"));
                session.setAttribute("occupation",resultSet.getString("worker_occupation"));
                session.setAttribute("age",resultSet.getString("worker_age"));
                session.setAttribute("sex",resultSet.getString("worker_sex"));
                session.setAttribute("employed",resultSet.getString("employed"));
                session.setAttribute("agent",resultSet.getString("agent_Company_Name"));
                session.setAttribute("hiredid",resultSet.getString("hiredmaid_id"));
            }

            response.sendRedirect("/min/ReviewDisengaged.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        doPost(request, response);
    }
}
