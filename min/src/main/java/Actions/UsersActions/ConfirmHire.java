package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 6/5/2016.
 */
@WebServlet("/confirmHire")
public class ConfirmHire extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String workerName = request.getParameter("workerName");

        JsonArray jsonArray = new JsonArray();
        HttpSession session = request.getSession();

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from workers w LEFT JOIN hiredMaids h ON h.workers_id_id = w.workers_id where workers_id = '"+workerName+"'");

            while(resultSet.next()){

                session.setAttribute("worker_id",resultSet.getString("workers_id"));
                session.setAttribute("fullname", resultSet.getString("workers_fullname"));
                session.setAttribute("agent", resultSet.getString("agent_Company_Name"));
                session.setAttribute("age", resultSet.getString("worker_age"));
                session.setAttribute("sex", resultSet.getString("worker_sex"));
                session.setAttribute("relationship", resultSet.getString("worker_relationship"));
                session.setAttribute("occupation", resultSet.getString("worker_occupation"));
                session.setAttribute("fee", resultSet.getString("agency_fee"));
                session.setAttribute("salary", resultSet.getString("expected_salary"));
                session.setAttribute("image",resultSet.getString("worker_image"));
                session.setAttribute("preference",resultSet.getString("preference"));

                session.setAttribute("employed",resultSet.getString("employed"));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        String url = response.encodeRedirectURL( "/min/ConfirmHire.jsp");
        response.sendRedirect(url);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
