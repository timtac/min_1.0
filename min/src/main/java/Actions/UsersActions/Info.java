package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.DbConstants;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

/**
 * Created by user on 1/11/2017.
 */
@WebServlet("/completeReg")
public class Info extends HttpServlet {

    Connection connection;
    Statement statement;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String userId = request.getParameter("userId");
        String gender = request.getParameter("sex");
        String firstName = request.getParameter("first");
        String username = request.getParameter("username");
        String address = request.getParameter("address");
        String location = request.getParameter("location");
        String phone = request.getParameter("mobile");
        String email = request.getParameter("email");

        String query = "UPDATE users SET user_title = '"+title+"', users_gender = '"+gender+"', users_fullname = '"+firstName+"', users_address = '"+address+"', user_email = '"+email+"', users_location = '"+location+"', user_mobile = '"+phone+"' where users_id = '"+userId+"'";

        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate(query);

            response.sendRedirect("/min/.jsp");
        } catch (Exception ex){
            ex.printStackTrace();
            object.addProperty("success",false);
        }

        writer.println(object.toString());
    }
}
