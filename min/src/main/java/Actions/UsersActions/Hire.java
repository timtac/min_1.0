package Actions.UsersActions;

import Context.ConnectionDriver;
import Dao.*;
import Mailer.MailHelper;
import Mailer.MessageLogics.MailConstants;
import Mailer.SendGridMailer;
import Mailer.SimpleMailSender;
import Models.PendingHire;
import Models.Users;
import Models.Workers;

import java.util.Date;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.*;
/**
 * Created by user on 2/4/2016.
 */
@WebServlet("/hire")
public class Hire extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username =  request.getParameter("username");
        String userId = request.getParameter("userId");
        String workerId = request.getParameter("workerId");
        String occupation = request.getParameter("occupation");
        String maidName = request.getParameter("maid");
        String employerName = request.getParameter("name");
        String usermail = request.getParameter("mail");
        String salary = request.getParameter("salary");
        String agentName = request.getParameter("agent");
        String agencyFee = request.getParameter("agencyFee");
        String checkbox = request.getParameter("checkbox");


       // String updatequery = "UPDATE users set hired_maid = '"+maid+"' WHERE users_username = '"+username+"'";
        String AgentMail = "<style type=\"text/css\">\n" +
                "    /* Take care of image borders and formatting, client hacks */\n" +
                "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                "    a img { border: none; }\n" +
                "    table { border-collapse: collapse !important;}\n" +
                "    #outlook a { padding:0; }\n" +
                "    .ReadMsgBody { width: 100%; }\n" +
                "    .ExternalClass { width: 100%; }\n" +
                "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                "    table td { border-collapse: collapse; }\n" +
                "    .ExternalClass * { line-height: 115%; }\n" +
                "    .container-for-gmail-android { min-width: 600px; }\n" +
                "\n" +
                "\n" +
                "    /* General styling */\n" +
                "    * {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "    }\n" +
                "\n" +
                "    body {\n" +
                "      -webkit-font-smoothing: antialiased;\n" +
                "      -webkit-text-size-adjust: none;\n" +
                "      width: 100% !important;\n" +
                "      margin: 0 !important;\n" +
                "      height: 100%;\n" +
                "      color: #676767;\n" +
                "    }\n" +
                "\n" +
                "    td {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "      font-size: 14px;\n" +
                "      color: #777777;\n" +
                "      text-align: center;\n" +
                "      line-height: 21px;\n" +
                "    }\n" +
                "\n" +
                "    a {\n" +
                "      color: #676767;\n" +
                "      text-decoration: none !important;\n" +
                "    }\n" +
                "\n" +
                "    .pull-left {\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .pull-right {\n" +
                "      text-align: right;\n" +
                "    }\n" +
                "\n" +
                "    .header-lg,\n" +
                "    .header-md,\n" +
                "    .header-sm {\n" +
                "      font-size: 32px;\n" +
                "      font-weight: 700;\n" +
                "      line-height: normal;\n" +
                "      padding: 35px 0 0;\n" +
                "      color: #4d4d4d;\n" +
                "    }\n" +
                "\n" +
                "    .header-md {\n" +
                "      font-size: 24px;\n" +
                "    }\n" +
                "\n" +
                "    .header-sm {\n" +
                "      padding: 5px 0;\n" +
                "      font-size: 18px;\n" +
                "      line-height: 1.3;\n" +
                "      padding-bottom: 30px;\n" +
                "      font-weight: 400;\n" +
                "    }\n" +
                "\n" +
                "    .content-padding {\n" +
                "      padding: 20px 0 30px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-right {\n" +
                "      width: 290px;\n" +
                "      text-align: right;\n" +
                "      padding-left: 10px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-left {\n" +
                "      width: 290px;\n" +
                "      text-align: left;\n" +
                "      padding-left: 10px;\n" +
                "      padding-bottom: 8px;\n" +
                "    }\n" +
                "\n" +
                "    .free-text {\n" +
                "      width: 100% !important;\n" +
                "      padding: 10px 60px 0px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #e5e5e5;\n" +
                "      vertical-align: top;\n" +
                "    }\n" +
                "\n" +
                "    .button {\n" +
                "      padding: 55px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .info-block {\n" +
                "      padding: 0 20px;\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block-container {\n" +
                "      padding: 30px 50px;\n" +
                "      width: 500px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block {\n" +
                "      background-color: #ffffff;\n" +
                "      width: 498px;\n" +
                "      border: 1px solid #e1e1e1;\n" +
                "      border-radius: 5px;\n" +
                "      padding: 25px 70px 60px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .info-img {\n" +
                "      width: 258px;\n" +
                "      border-radius: 5px 5px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-img {\n" +
                "      width: 480px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-full {\n" +
                "      width: 600px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .user-img img {\n" +
                "      width: 82px;\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #cccccc;\n" +
                "    }\n" +
                "\n" +
                "    .user-img {\n" +
                "      width: 92px;\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .user-msg {\n" +
                "      width: 236px;\n" +
                "      font-size: 14px;\n" +
                "      text-align: left;\n" +
                "      font-style: italic;\n" +
                "    }\n" +
                "\n" +
                "    .code-block {\n" +
                "      padding: 10px 0;\n" +
                "      border: 1px solid #cccccc;\n" +
                "      width: 20px;\n" +
                "      color: #4d4d4d;\n" +
                "      font-weight: bold;\n" +
                "      font-size: 18px;\n" +
                "    }\n" +
                "\n" +
                "    .center-txt {\n" +
                "      padding-left: 30px;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-gmail {\n" +
                "      min-width:600px;\n" +
                "      height: 0px !important;\n" +
                "      line-height: 1px !important;\n" +
                "      font-size: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @media screen {\n" +
                "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                "      * {\n" +
                "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                "    /* Mobile styles */\n" +
                "    @media only screen and (max-width: 480px) {\n" +
                "\n" +
                "      table[class*=\"container-for-gmail-android\"] {\n" +
                "        min-width: 290px !important;\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      table[class=\"w320\"] {\n" +
                "        width: 320px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-gmail\"] {\n" +
                "        display: none !important;\n" +
                "        width: 0 !important;\n" +
                "        height: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-left\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-left: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-right\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-right: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-lg\"] {\n" +
                "        font-size: 24px !important;\n" +
                "        padding: 10px 50px 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-md\"] {\n" +
                "        font-size: 18px !important;\n" +
                "        padding-bottom: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"content-padding\"] {\n" +
                "        padding: 5px 0 30px !important;\n" +
                "      }\n" +
                "\n" +
                "       td[class=\"button\"] {\n" +
                "        padding: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"free-text\"] {\n" +
                "        padding: 10px 18px 30px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-img\"],\n" +
                "      img[class=\"force-width-full\"] {\n" +
                "        display: none !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-block\"] {\n" +
                "        display: block !important;\n" +
                "        width: 280px !important;\n" +
                "        padding-bottom: 40px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-img\"],\n" +
                "      img[class=\"info-img\"] {\n" +
                "        width: 278px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block-container\"] {\n" +
                "        padding: 8px 20px !important;\n" +
                "        width: 280px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block\"] {\n" +
                "        padding: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-img\"] {\n" +
                "        display: block !important;\n" +
                "        text-align: center !important;\n" +
                "        width: 100% !important;\n" +
                "        padding-bottom: 10px;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-msg\"] {\n" +
                "        display: block !important;\n" +
                "        padding-bottom: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"center-txt\"] {\n" +
                "        padding-left: 3px !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body bgcolor=\"#f7f7f7\">\n" +
                "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                "  <tr>\n" +
                "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                "      <center>\n" +
                "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                "          <tr>\n" +
                "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                "            <!--[if gte mso 9]>\n" +
                "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                "              <v:textbox inset=\"0,0,0,0\">\n" +
                "            <![endif]-->\n" +
                "              <center>\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                "                  <tr>\n" +
                "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
                "                  </tr>\n" +
                "                </table>\n" +
                "              </center>\n" +
                "              <!--[if gte mso 9]>\n" +
                "              </v:textbox>\n" +
                "            </v:rect>\n" +
                "            <![endif]-->\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td class=\"header-lg\">\n" +
                "              Notification!\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"free-text\">\n" +
                " \n\n\n " +
                "Dear "+agentName+",<br />" +
                "The user "+employerName+" has requested to hire the following worker:<br />" +
                "Ref: MIN0"+workerId+"\n<br />" +
                "Worker's Fullname: "+maidName+"\n<br />"+
                "Occupation: "+occupation+"\n<br />" +
                "Expected Salary: "+salary+"\n<br />" +
                "Agency Fee: "+agencyFee+"\n<br />" +
                "<br />"+
                "\n" +
                "Please contact "+employerName+" to discuss arrangements for the hire.\n<br />" +
                "\n" +
                "Please do not hesitate to contact MaidInNigeria if you have any questions. Also if this worker is no longer available ,please inform us so we can do a swap.\n<br />" +
                "\n" +
                "Thank you,\n<br />" +
                "MaidInNigeria\n"+
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"mini-block-container\">\n" +
                
                "            </td>\n" +

                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td style=\"padding: 25px 0 25px\">\n" +
                "              <strong>Maid In Nigeria</strong><br />\n" +
                "              Lagos, Nigeria <br /> contact@maidinnigeria.com.ng\n" +
                "               <br /><br />\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "</table>\n" ;

        String AdminMail = "<style type=\"text/css\">\n" +
                "    /* Take care of image borders and formatting, client hacks */\n" +
                "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                "    a img { border: none; }\n" +
                "    table { border-collapse: collapse !important;}\n" +
                "    #outlook a { padding:0; }\n" +
                "    .ReadMsgBody { width: 100%; }\n" +
                "    .ExternalClass { width: 100%; }\n" +
                "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                "    table td { border-collapse: collapse; }\n" +
                "    .ExternalClass * { line-height: 115%; }\n" +
                "    .container-for-gmail-android { min-width: 600px; }\n" +
                "\n" +
                "\n" +
                "    /* General styling */\n" +
                "    * {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "    }\n" +
                "\n" +
                "    body {\n" +
                "      -webkit-font-smoothing: antialiased;\n" +
                "      -webkit-text-size-adjust: none;\n" +
                "      width: 100% !important;\n" +
                "      margin: 0 !important;\n" +
                "      height: 100%;\n" +
                "      color: #676767;\n" +
                "    }\n" +
                "\n" +
                "    td {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "      font-size: 14px;\n" +
                "      color: #777777;\n" +
                "      text-align: center;\n" +
                "      line-height: 21px;\n" +
                "    }\n" +
                "\n" +
                "    a {\n" +
                "      color: #676767;\n" +
                "      text-decoration: none !important;\n" +
                "    }\n" +
                "\n" +
                "    .pull-left {\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .pull-right {\n" +
                "      text-align: right;\n" +
                "    }\n" +
                "\n" +
                "    .header-lg,\n" +
                "    .header-md,\n" +
                "    .header-sm {\n" +
                "      font-size: 32px;\n" +
                "      font-weight: 700;\n" +
                "      line-height: normal;\n" +
                "      padding: 35px 0 0;\n" +
                "      color: #4d4d4d;\n" +
                "    }\n" +
                "\n" +
                "    .header-md {\n" +
                "      font-size: 24px;\n" +
                "    }\n" +
                "\n" +
                "    .header-sm {\n" +
                "      padding: 5px 0;\n" +
                "      font-size: 18px;\n" +
                "      line-height: 1.3;\n" +
                "      padding-bottom: 30px;\n" +
                "      font-weight: 400;\n" +
                "    }\n" +
                "\n" +
                "    .content-padding {\n" +
                "      padding: 20px 0 30px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-right {\n" +
                "      width: 290px;\n" +
                "      text-align: right;\n" +
                "      padding-left: 10px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-left {\n" +
                "      width: 290px;\n" +
                "      text-align: left;\n" +
                "      padding-left: 10px;\n" +
                "      padding-bottom: 8px;\n" +
                "    }\n" +
                "\n" +
                "    .free-text {\n" +
                "      width: 100% !important;\n" +
                "      padding: 10px 60px 0px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #e5e5e5;\n" +
                "      vertical-align: top;\n" +
                "    }\n" +
                "\n" +
                "    .button {\n" +
                "      padding: 55px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .info-block {\n" +
                "      padding: 0 20px;\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block-container {\n" +
                "      padding: 30px 50px;\n" +
                "      width: 500px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block {\n" +
                "      background-color: #ffffff;\n" +
                "      width: 498px;\n" +
                "      border: 1px solid #e1e1e1;\n" +
                "      border-radius: 5px;\n" +
                "      padding: 25px 70px 60px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .info-img {\n" +
                "      width: 258px;\n" +
                "      border-radius: 5px 5px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-img {\n" +
                "      width: 480px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-full {\n" +
                "      width: 600px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .user-img img {\n" +
                "      width: 82px;\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #cccccc;\n" +
                "    }\n" +
                "\n" +
                "    .user-img {\n" +
                "      width: 92px;\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .user-msg {\n" +
                "      width: 236px;\n" +
                "      font-size: 14px;\n" +
                "      text-align: left;\n" +
                "      font-style: italic;\n" +
                "    }\n" +
                "\n" +
                "    .code-block {\n" +
                "      padding: 10px 0;\n" +
                "      border: 1px solid #cccccc;\n" +
                "      width: 20px;\n" +
                "      color: #4d4d4d;\n" +
                "      font-weight: bold;\n" +
                "      font-size: 18px;\n" +
                "    }\n" +
                "\n" +
                "    .center-txt {\n" +
                "      padding-left: 30px;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-gmail {\n" +
                "      min-width:600px;\n" +
                "      height: 0px !important;\n" +
                "      line-height: 1px !important;\n" +
                "      font-size: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @media screen {\n" +
                "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                "      * {\n" +
                "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                "    /* Mobile styles */\n" +
                "    @media only screen and (max-width: 480px) {\n" +
                "\n" +
                "      table[class*=\"container-for-gmail-android\"] {\n" +
                "        min-width: 290px !important;\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      table[class=\"w320\"] {\n" +
                "        width: 320px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-gmail\"] {\n" +
                "        display: none !important;\n" +
                "        width: 0 !important;\n" +
                "        height: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-left\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-left: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-right\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-right: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-lg\"] {\n" +
                "        font-size: 24px !important;\n" +
                "        padding: 10px 50px 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-md\"] {\n" +
                "        font-size: 18px !important;\n" +
                "        padding-bottom: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"content-padding\"] {\n" +
                "        padding: 5px 0 30px !important;\n" +
                "      }\n" +
                "\n" +
                "       td[class=\"button\"] {\n" +
                "        padding: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"free-text\"] {\n" +
                "        padding: 10px 18px 30px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-img\"],\n" +
                "      img[class=\"force-width-full\"] {\n" +
                "        display: none !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-block\"] {\n" +
                "        display: block !important;\n" +
                "        width: 280px !important;\n" +
                "        padding-bottom: 40px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-img\"],\n" +
                "      img[class=\"info-img\"] {\n" +
                "        width: 278px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block-container\"] {\n" +
                "        padding: 8px 20px !important;\n" +
                "        width: 280px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block\"] {\n" +
                "        padding: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-img\"] {\n" +
                "        display: block !important;\n" +
                "        text-align: center !important;\n" +
                "        width: 100% !important;\n" +
                "        padding-bottom: 10px;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-msg\"] {\n" +
                "        display: block !important;\n" +
                "        padding-bottom: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"center-txt\"] {\n" +
                "        padding-left: 3px !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body bgcolor=\"#f7f7f7\">\n" +
                "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                "  <tr>\n" +
                "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                "      <center>\n" +
                "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                "          <tr>\n" +
                "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                "            <!--[if gte mso 9]>\n" +
                "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                "              <v:textbox inset=\"0,0,0,0\">\n" +
                "            <![endif]-->\n" +
                "              <center>\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                "                  <tr>\n" +
                "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
                "                  </tr>\n" +
                "                </table>\n" +
                "              </center>\n" +
                "              <!--[if gte mso 9]>\n" +
                "              </v:textbox>\n" +
                "            </v:rect>\n" +
                "            <![endif]-->\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td class=\"header-lg\">\n" +
                "              Notification!\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"free-text\">\n" +
                " \n\n\n " +
                "Dear Administrator,\n" +
                "The user "+employerName+" has requested to hire the following worker from "+agentName+":\n" +
                "Ref: MIN0"+workerId+" \n" +
                "Occupation: "+occupation+"\n" +
                "Expected Salary: "+salary+"\n" +
                "Agency Fee: "+agencyFee+"\n" +
                "\n" +
                "Please follow up with "+agentName+" and "+employerName+" to ensure arrangements for the interview have been made.\n" +
                "\n" +
                "Thank you,\n" +
                "MaidInNigeria\n\n"+
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"mini-block-container\">\n" +

                "            </td>\n" +

                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td style=\"padding: 25px 0 25px\">\n" +
                "              <strong>Maid In Nigeria</strong><br />\n" +
                "              Lagos, Nigeria <br /> contact@maidinnigeria.com.ng\n" +
                "               <br /><br />\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "</table>\n" ;

        String EmployerMail = "<style type=\"text/css\">\n" +
                "    /* Take care of image borders and formatting, client hacks */\n" +
                "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                "    a img { border: none; }\n" +
                "    table { border-collapse: collapse !important;}\n" +
                "    #outlook a { padding:0; }\n" +
                "    .ReadMsgBody { width: 100%; }\n" +
                "    .ExternalClass { width: 100%; }\n" +
                "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                "    table td { border-collapse: collapse; }\n" +
                "    .ExternalClass * { line-height: 115%; }\n" +
                "    .container-for-gmail-android { min-width: 600px; }\n" +
                "\n" +
                "\n" +
                "    /* General styling */\n" +
                "    * {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "    }\n" +
                "\n" +
                "    body {\n" +
                "      -webkit-font-smoothing: antialiased;\n" +
                "      -webkit-text-size-adjust: none;\n" +
                "      width: 100% !important;\n" +
                "      margin: 0 !important;\n" +
                "      height: 100%;\n" +
                "      color: #676767;\n" +
                "    }\n" +
                "\n" +
                "    td {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "      font-size: 14px;\n" +
                "      color: #777777;\n" +
                "      text-align: center;\n" +
                "      line-height: 21px;\n" +
                "    }\n" +
                "\n" +
                "    a {\n" +
                "      color: #676767;\n" +
                "      text-decoration: none !important;\n" +
                "    }\n" +
                "\n" +
                "    .pull-left {\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .pull-right {\n" +
                "      text-align: right;\n" +
                "    }\n" +
                "\n" +
                "    .header-lg,\n" +
                "    .header-md,\n" +
                "    .header-sm {\n" +
                "      font-size: 32px;\n" +
                "      font-weight: 700;\n" +
                "      line-height: normal;\n" +
                "      padding: 35px 0 0;\n" +
                "      color: #4d4d4d;\n" +
                "    }\n" +
                "\n" +
                "    .header-md {\n" +
                "      font-size: 24px;\n" +
                "    }\n" +
                "\n" +
                "    .header-sm {\n" +
                "      padding: 5px 0;\n" +
                "      font-size: 18px;\n" +
                "      line-height: 1.3;\n" +
                "      padding-bottom: 30px;\n" +
                "      font-weight: 400;\n" +
                "    }\n" +
                "\n" +
                "    .content-padding {\n" +
                "      padding: 20px 0 30px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-right {\n" +
                "      width: 290px;\n" +
                "      text-align: right;\n" +
                "      padding-left: 10px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-left {\n" +
                "      width: 290px;\n" +
                "      text-align: left;\n" +
                "      padding-left: 10px;\n" +
                "      padding-bottom: 8px;\n" +
                "    }\n" +
                "\n" +
                "    .free-text {\n" +
                "      width: 100% !important;\n" +
                "      padding: 10px 60px 0px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #e5e5e5;\n" +
                "      vertical-align: top;\n" +
                "    }\n" +
                "\n" +
                "    .button {\n" +
                "      padding: 55px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .info-block {\n" +
                "      padding: 0 20px;\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block-container {\n" +
                "      padding: 30px 50px;\n" +
                "      width: 500px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block {\n" +
                "      background-color: #ffffff;\n" +
                "      width: 498px;\n" +
                "      border: 1px solid #e1e1e1;\n" +
                "      border-radius: 5px;\n" +
                "      padding: 25px 70px 60px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .info-img {\n" +
                "      width: 258px;\n" +
                "      border-radius: 5px 5px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-img {\n" +
                "      width: 480px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-full {\n" +
                "      width: 600px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .user-img img {\n" +
                "      width: 82px;\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #cccccc;\n" +
                "    }\n" +
                "\n" +
                "    .user-img {\n" +
                "      width: 92px;\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .user-msg {\n" +
                "      width: 236px;\n" +
                "      font-size: 14px;\n" +
                "      text-align: left;\n" +
                "      font-style: italic;\n" +
                "    }\n" +
                "\n" +
                "    .code-block {\n" +
                "      padding: 10px 0;\n" +
                "      border: 1px solid #cccccc;\n" +
                "      width: 20px;\n" +
                "      color: #4d4d4d;\n" +
                "      font-weight: bold;\n" +
                "      font-size: 18px;\n" +
                "    }\n" +
                "\n" +
                "    .center-txt {\n" +
                "      padding-left: 30px;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-gmail {\n" +
                "      min-width:600px;\n" +
                "      height: 0px !important;\n" +
                "      line-height: 1px !important;\n" +
                "      font-size: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @media screen {\n" +
                "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                "      * {\n" +
                "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                "    /* Mobile styles */\n" +
                "    @media only screen and (max-width: 480px) {\n" +
                "\n" +
                "      table[class*=\"container-for-gmail-android\"] {\n" +
                "        min-width: 290px !important;\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      table[class=\"w320\"] {\n" +
                "        width: 320px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-gmail\"] {\n" +
                "        display: none !important;\n" +
                "        width: 0 !important;\n" +
                "        height: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-left\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-left: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-right\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-right: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-lg\"] {\n" +
                "        font-size: 24px !important;\n" +
                "        padding: 10px 50px 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-md\"] {\n" +
                "        font-size: 18px !important;\n" +
                "        padding-bottom: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"content-padding\"] {\n" +
                "        padding: 5px 0 30px !important;\n" +
                "      }\n" +
                "\n" +
                "       td[class=\"button\"] {\n" +
                "        padding: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"free-text\"] {\n" +
                "        padding: 10px 18px 30px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-img\"],\n" +
                "      img[class=\"force-width-full\"] {\n" +
                "        display: none !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-block\"] {\n" +
                "        display: block !important;\n" +
                "        width: 280px !important;\n" +
                "        padding-bottom: 40px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-img\"],\n" +
                "      img[class=\"info-img\"] {\n" +
                "        width: 278px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block-container\"] {\n" +
                "        padding: 8px 20px !important;\n" +
                "        width: 280px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block\"] {\n" +
                "        padding: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-img\"] {\n" +
                "        display: block !important;\n" +
                "        text-align: center !important;\n" +
                "        width: 100% !important;\n" +
                "        padding-bottom: 10px;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-msg\"] {\n" +
                "        display: block !important;\n" +
                "        padding-bottom: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"center-txt\"] {\n" +
                "        padding-left: 3px !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body bgcolor=\"#f7f7f7\">\n" +
                "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                "  <tr>\n" +
                "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                "      <center>\n" +
                "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                "          <tr>\n" +
                "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                "            <!--[if gte mso 9]>\n" +
                "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                "              <v:textbox inset=\"0,0,0,0\">\n" +
                "            <![endif]-->\n" +
                "              <center>\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                "                  <tr>\n" +
                "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
                "                  </tr>\n" +
                "                </table>\n" +
                "              </center>\n" +
                "              <!--[if gte mso 9]>\n" +
                "              </v:textbox>\n" +
                "            </v:rect>\n" +
                "            <![endif]-->\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td class=\"header-lg\">\n" +
                "              Notification!\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"free-text\">\n" +
                " \n\n\n " +
                "Dear "+employerName+",<br />\n" +
                "Your request to hire the following worker has been received:<br />\n" +
                "Ref: MIN0"+workerId+" <br />\n" +
                "Occupation: "+occupation+"\n<br />" +
                "Expected Salary: "+salary+"\n<br />" +
                "Agency Fee: "+agencyFee+"\n<br />" +
                "\n" +
                "The agent will be in contact with you shortly to discuss arrangements for the hire.\n<br />" +
                "\n" +
                "Please do not hesitate to contact us if you have any questions.\n<br />" +
                "\n" +
                "Thank you,\n<br />" +
                "MaidInNigeria\n"+
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"mini-block-container\">\n" +

                "            </td>\n" +

                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td style=\"padding: 25px 0 25px\">\n" +
                "              <strong>Maid In Nigeria</strong><br />\n" +
                "              Lagos, Nigeria <br /> contact@maidinnigeria.com.ng\n" +
                "               <br /><br />\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "</table>\n" ;


        String deleteRequested = "Delete requestedMaids from requestedMaids where requestedMaids.workers_id_id = '"+workerId+"' AND requestedMaids.users_id_id = '"+userId+"'" ;

        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            statement.executeUpdate("update workers set employed = 'pending' where workers_id = '"+workerId+"'");
            resultSet = statement.executeQuery("select a.agent_email from agents a INNER JOIN workers w ON a.agent_Company_Name = w.agent_Company_Name WHERE w.workers_fullname = '"+maidName+"'");

            while(resultSet.next()){

                System.out.println(resultSet.getString("agent_email"));
//                 MailHelper mailHelper = new MailHelper();
//                mailHelper.sendMessage(resultSet.getString("agent_email"), AgentMail, "Hire Alert");
//                mailHelper.sendMessage(MailConstants.REQUEST_MAIL, AdminMail, "Hire Alert");
//                mailHelper.sendMessage(usermail,EmployerMail,"Hire Alert");

                SimpleMailSender mailer = new SimpleMailSender();
                mailer.sendMessage(resultSet.getString("agent_email"), "Hire Alert", AgentMail);
                mailer.sendMessage(MailConstants.REQUEST_MAIL, "Hire Alert", AdminMail);
                mailer.sendMessage(usermail, "Hire Alert",EmployerMail);

                SendGridMailer mailer1 = new SendGridMailer();
                mailer1.sendGridMessage(resultSet.getString("agent_email"), "Hire Alert", AgentMail);
                mailer1.sendGridMessage(MailConstants.REQUEST_MAIL, "Hire Alert", AdminMail);
                mailer1.sendGridMessage(usermail, "Hire Alert",EmployerMail);
            }

            Workers workers = new Workers();
            workers.setWorkers_id(Integer.parseInt(workerId));
            Workers_dao workersDao = new Workers_dao(workers);
            workersDao.refreshWorker();

            Users users = new Users();
            users.setUsers_id(Integer.parseInt(userId));
            Users_dao usersDao = new Users_dao(users);
            usersDao.refreshUsers();

            PendingHire pendingHire = new PendingHire();
            pendingHire.setWorkers_id(workers);
            pendingHire.setUsers_id(users);

            PendingHireDao pendingHireDao = new PendingHireDao(pendingHire);
            pendingHireDao.createPending();

            statement.executeUpdate(deleteRequested);
            System.out.println("deleted requested");
//            statement.executeUpdate("DELETE from requestedmaids where employersUsername = '"+username+"' AND workersName = '"+maid+"'");
            HttpSession session = request.getSession();

            session.setAttribute("maid",workerId);
            session.setAttribute("occupation",occupation);
            if(checkbox.equals("cancel")){
                System.out.println(checkbox);
                statement.executeUpdate("Delete requestedMaids from requestedMaids INNER JOIN users ON requestedMaids.users_id_id = users.users_id INNER JOIN workers w ON workers.workers_id =requestedMaids.workers_id_id WHERE workers.worker_occupation = '"+occupation+"'");
            }
            response.sendRedirect("/min/HireConfirmation.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
            response.sendRedirect("/min/HireConfirmation.jsp");
        }finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
