package Maillogics;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Mailer.SendGridMailer;
import Mailer.SimpleMailSender;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 1/13/2016.
 */
@WebServlet("/mailAllAgents")
public class MailAllAgents extends HttpServlet {
        Connection connection;
        Statement statement;
        ResultSet resultSet;

        @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request,response);
    }

        @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();

        String query = "select agent_email from agents";
        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            SimpleMailSender mailer = new SimpleMailSender();
            SendGridMailer mailer1 = new SendGridMailer();
            while(resultSet.next()){
//                MailHelper mailHelper = new MailHelper();
//                mailHelper.sendMessage(resultSet.getString("agent_email"),message,subject);

                mailer.sendMessage(resultSet.getString("agent_email"),subject,message);
            }
            object.addProperty("success",true);
        }catch(Exception ex){
            object.addProperty("success",false);
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(object.toString());
    }

}
