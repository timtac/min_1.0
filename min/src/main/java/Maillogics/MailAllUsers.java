package Maillogics;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Mailer.MailHelper;
import Mailer.SendGridMailer;
import Mailer.SimpleMailSender;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 4/22/2016.
 */
@WebServlet("/mailAllUsers")
public class MailAllUsers extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();

        String query = "select user_email from users";
        try{

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            MailHelper mailHelper = new MailHelper();
            SimpleMailSender mailer = new SimpleMailSender();
            SendGridMailer mailer1 = new SendGridMailer();

            while(resultSet.next()){
                mailer.sendMessage(resultSet.getString("user_email"),subject,message);
//                mailHelper.sendMessage(resultSet.getString("user_email"),message,subject);
            }
            object.addProperty("success",true);
        }catch(Exception ex){
            object.addProperty("success",false);
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        writer.println(object.toString());
    }
}
