package Maillogics;

import Mailer.MailUtils;
import Mailer.MessageLogics.MailConstants;
import utils.postmark.test.TestClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 6/1/2016.
 */
@WebServlet("/mailUs")
public class MailUs extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String status = request.getParameter("status");

        System.out.println("got here before try block");
        try{

            TestClient testClient = new TestClient();

//            MailUtils.sendMail("timtac66@yahoo.com".split(","), status + " " + subject, name + " " + email + " " + message);
//
            response.sendRedirect("/min/mail.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
