package Maillogics;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Dao.RequestedMaidDao;
import Dao.Users_dao;
import Dao.Workers_dao;
import Mailer.MessageLogics.MailConstants;
import Mailer.SendGridMailer;
import Mailer.SimpleMailSender;
import Models.RequestedMaids;
import Models.Users;
import Models.Workers;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 10/16/2015.
 */
@WebServlet("/sendMailReqWorker")
public class RequestForWorkerMail extends HttpServlet{

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request,response);
    }


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String workerId = request.getParameter("workerid");
        String agentName = request.getParameter("agentName");
        String workerName = request.getParameter("workerName");
        String occupation = request.getParameter("occupation");
        String mobile = request.getParameter("mobile");
        String empMail = request.getParameter("mail");
        String employerName = request.getParameter("employerName");
        String userId = request.getParameter("userId");
        String salary = request.getParameter("salary");
        String agencyFee = request.getParameter("agencyFee");

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();

            JsonObject jsonObject = new JsonObject();

        String ADM_REQ_MSG = "<style type=\"text/css\">\n" +
                "    /* Take care of image borders and formatting, client hacks */\n" +
                "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                "    a img { border: none; }\n" +
                "    table { border-collapse: collapse !important;}\n" +
                "    #outlook a { padding:0; }\n" +
                "    .ReadMsgBody { width: 100%; }\n" +
                "    .ExternalClass { width: 100%; }\n" +
                "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                "    table td { border-collapse: collapse; }\n" +
                "    .ExternalClass * { line-height: 115%; }\n" +
                "    .container-for-gmail-android { min-width: 600px; }\n" +
                "\n" +
                "\n" +
                "    /* General styling */\n" +
                "    * {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "    }\n" +
                "\n" +
                "    body {\n" +
                "      -webkit-font-smoothing: antialiased;\n" +
                "      -webkit-text-size-adjust: none;\n" +
                "      width: 100% !important;\n" +
                "      margin: 0 !important;\n" +
                "      height: 100%;\n" +
                "      color: #676767;\n" +
                "    }\n" +
                "\n" +
                "    td {\n" +
                "      font-family: Helvetica, Arial, sans-serif;\n" +
                "      font-size: 14px;\n" +
                "      color: #777777;\n" +
                "      text-align: center;\n" +
                "      line-height: 21px;\n" +
                "    }\n" +
                "\n" +
                "    a {\n" +
                "      color: #676767;\n" +
                "      text-decoration: none !important;\n" +
                "    }\n" +
                "\n" +
                "    .pull-left {\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .pull-right {\n" +
                "      text-align: right;\n" +
                "    }\n" +
                "\n" +
                "    .header-lg,\n" +
                "    .header-md,\n" +
                "    .header-sm {\n" +
                "      font-size: 32px;\n" +
                "      font-weight: 700;\n" +
                "      line-height: normal;\n" +
                "      padding: 35px 0 0;\n" +
                "      color: #4d4d4d;\n" +
                "    }\n" +
                "\n" +
                "    .header-md {\n" +
                "      font-size: 24px;\n" +
                "    }\n" +
                "\n" +
                "    .header-sm {\n" +
                "      padding: 5px 0;\n" +
                "      font-size: 18px;\n" +
                "      line-height: 1.3;\n" +
                "      padding-bottom: 30px;\n" +
                "      font-weight: 400;\n" +
                "    }\n" +
                "\n" +
                "    .content-padding {\n" +
                "      padding: 20px 0 30px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-right {\n" +
                "      width: 290px;\n" +
                "      text-align: right;\n" +
                "      padding-left: 10px;\n" +
                "    }\n" +
                "\n" +
                "    .mobile-header-padding-left {\n" +
                "      width: 290px;\n" +
                "      text-align: left;\n" +
                "      padding-left: 10px;\n" +
                "      padding-bottom: 8px;\n" +
                "    }\n" +
                "\n" +
                "    .free-text {\n" +
                "      width: 100% !important;\n" +
                "      padding: 10px 60px 0px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #e5e5e5;\n" +
                "      vertical-align: top;\n" +
                "    }\n" +
                "\n" +
                "    .button {\n" +
                "      padding: 55px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .info-block {\n" +
                "      padding: 0 20px;\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block-container {\n" +
                "      padding: 30px 50px;\n" +
                "      width: 500px;\n" +
                "    }\n" +
                "\n" +
                "    .mini-block {\n" +
                "      background-color: #ffffff;\n" +
                "      width: 498px;\n" +
                "      border: 1px solid #e1e1e1;\n" +
                "      border-radius: 5px;\n" +
                "      padding: 25px 70px 60px;\n" +
                "    }\n" +
                "\n" +
                "    .block-rounded {\n" +
                "      width: 260px;\n" +
                "    }\n" +
                "\n" +
                "    .info-img {\n" +
                "      width: 258px;\n" +
                "      border-radius: 5px 5px 0 0;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-img {\n" +
                "      width: 480px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-full {\n" +
                "      width: 600px;\n" +
                "      height: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "    .user-img img {\n" +
                "      width: 82px;\n" +
                "      border-radius: 5px;\n" +
                "      border: 1px solid #cccccc;\n" +
                "    }\n" +
                "\n" +
                "    .user-img {\n" +
                "      width: 92px;\n" +
                "      text-align: left;\n" +
                "    }\n" +
                "\n" +
                "    .user-msg {\n" +
                "      width: 236px;\n" +
                "      font-size: 14px;\n" +
                "      text-align: left;\n" +
                "      font-style: italic;\n" +
                "    }\n" +
                "\n" +
                "    .code-block {\n" +
                "      padding: 10px 0;\n" +
                "      border: 1px solid #cccccc;\n" +
                "      width: 20px;\n" +
                "      color: #4d4d4d;\n" +
                "      font-weight: bold;\n" +
                "      font-size: 18px;\n" +
                "    }\n" +
                "\n" +
                "    .center-txt {\n" +
                "      padding-left: 30px;\n" +
                "    }\n" +
                "\n" +
                "    .force-width-gmail {\n" +
                "      min-width:600px;\n" +
                "      height: 0px !important;\n" +
                "      line-height: 1px !important;\n" +
                "      font-size: 1px !important;\n" +
                "    }\n" +
                "\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"screen\">\n" +
                "    @media screen {\n" +
                "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                "      * {\n" +
                "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "\n" +
                "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                "    /* Mobile styles */\n" +
                "    @media only screen and (max-width: 480px) {\n" +
                "\n" +
                "      table[class*=\"container-for-gmail-android\"] {\n" +
                "        min-width: 290px !important;\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      table[class=\"w320\"] {\n" +
                "        width: 320px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-gmail\"] {\n" +
                "        display: none !important;\n" +
                "        width: 0 !important;\n" +
                "        height: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-left\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-left: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"mobile-header-padding-right\"] {\n" +
                "        width: 160px !important;\n" +
                "        padding-right: 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-lg\"] {\n" +
                "        font-size: 24px !important;\n" +
                "        padding: 10px 50px 0 !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"header-md\"] {\n" +
                "        font-size: 18px !important;\n" +
                "        padding-bottom: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"content-padding\"] {\n" +
                "        padding: 5px 0 30px !important;\n" +
                "      }\n" +
                "\n" +
                "       td[class=\"button\"] {\n" +
                "        padding: 5px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class*=\"free-text\"] {\n" +
                "        padding: 10px 18px 30px !important;\n" +
                "      }\n" +
                "\n" +
                "      img[class=\"force-width-img\"],\n" +
                "      img[class=\"force-width-full\"] {\n" +
                "        display: none !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-block\"] {\n" +
                "        display: block !important;\n" +
                "        width: 280px !important;\n" +
                "        padding-bottom: 40px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"info-img\"],\n" +
                "      img[class=\"info-img\"] {\n" +
                "        width: 278px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block-container\"] {\n" +
                "        padding: 8px 20px !important;\n" +
                "        width: 280px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"mini-block\"] {\n" +
                "        padding: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-img\"] {\n" +
                "        display: block !important;\n" +
                "        text-align: center !important;\n" +
                "        width: 100% !important;\n" +
                "        padding-bottom: 10px;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"user-msg\"] {\n" +
                "        display: block !important;\n" +
                "        padding-bottom: 20px !important;\n" +
                "      }\n" +
                "\n" +
                "      td[class=\"center-txt\"] {\n" +
                "        padding-left: 3px !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body bgcolor=\"#f7f7f7\">\n" +
                "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                "  <tr>\n" +
                "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                "      <center>\n" +
                "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                "          <tr>\n" +
                "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                "            <!--[if gte mso 9]>\n" +
                "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                "              <v:textbox inset=\"0,0,0,0\">\n" +
                "            <![endif]-->\n" +
                "              <center>\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                "                  <tr>\n" +
                "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
                "                  </tr>\n" +
                "                </table>\n" +
                "              </center>\n" +
                "              <!--[if gte mso 9]>\n" +
                "              </v:textbox>\n" +
                "            </v:rect>\n" +
                "            <![endif]-->\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td class=\"header-lg\">\n" +
                "              Notification!\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"free-text\">\n" +
                "Dear Administrator,\n<br />" +
                "The user "+employerName+" has requested to interview the following worker from "+agentName+":\n<br />" +
                "Ref: MIN0 "+workerId+"\n<br />" +
                "Occupation: "+occupation+"\n<br />" +
                "Expected Salary: "+salary+"\n<br />" +
                "Agency Fee: "+agencyFee+"\n<br />" +
                "Employer e_mail:"+empMail+"<br />" +
                "Please follow up with "+agentName+" and "+employerName+" to ensure arrangements for the interview have been made.<br />" +
                "\n" +
                "Thank you,\n<br />" +
                "MaidInNigeria\n" +

                "            </td>\n" +
                "          </tr>\n" +
                "          <tr>\n" +
                "            <td class=\"mini-block-container\">\n" +
                "            </td>\n" +

                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
                "      <center>\n" +
                "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                "          <tr>\n" +
                "            <td style=\"padding: 25px 0 25px\">\n" +
                "              <strong>Maid In Nigeria</strong><br />\n" +
                "              Lagos, Nigeria <br /> contact@maidinnigeria.com.ng\n" +
                "               <br /><br />\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </table>\n" +
                "      </center>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "</table>\n"
                ;

        String USER_REQ_MSG = "<style type=\"text/css\">\n" +
                "\n" +           "                /* Take care of image borders and formatting, client hacks */\n\" +\n" +
                "                img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n\" +\n" +
                "                a img { border: none; }\n\" +\n" +
                "                table { border-collapse: collapse !important;}\n \n" +
                "                #outlook a { padding:0; }\n\n" +
                "                .ReadMsgBody { width: 100%; }\n\n" +
                "                .ExternalClass { width: 100%; }\n\n" +
                "                .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n\n" +
                "                table td { border-collapse: collapse; }\n\n" +
                "                .ExternalClass * { line-height: 115%; }\n\n" +
                "                .container-for-gmail-android { min-width: 600px; }\n\n" +
                "            \n\n" +
                "            \n\n" +
                "                /* General styling */\n\n" +
                "                * {\n\n" +
                "                  font-family: Helvetica, Arial, sans-serif;\n\n" +
                "                }\n\n" +
                "            \n \n" +
                "                body {\n\n" +
                "                  -webkit-font-smoothing: antialiased;\n\n" +
                "                  -webkit-text-size-adjust: none;\n" +
                "                  width: 100% !important;\n\n" +
                "                  margin: 0 !important;\n\n" +
                "                  height: 100%;\n\n" +
                "                  color: #676767;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                td {\n\n" +
                "                  font-family: Helvetica, Arial, sans-serif;\n\n" +
                "                  font-size: 14px;\n\n" +
                "                  color: #777777;\n\n" +
                "                  text-align: center;\n\n" +
                "                  line-height: 21px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                a {\n\n" +
                "                  color: #676767;\n\n" +
                "                  text-decoration: none !important;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .pull-left {\n\n" +
                "                  text-align: left;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .pull-right {\n\n" +
                "                  text-align: right;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .header-lg,\n\n" +
                "                .header-md,\n\n" +
                "                .header-sm {\n\n" +
                "                  font-size: 32px;\n\n" +
                "                  font-weight: 700;\n\n" +
                "                  line-height: normal;\n\n" +
                "                  padding: 35px 0 0;\n\n" +
                "                  color: #4d4d4d;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .header-md {\n\n" +
                "                  font-size: 24px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .header-sm {\n\n" +
                "                  padding: 5px 0;\n\n" +
                "                  font-size: 18px;\n\n" +
                "                  line-height: 1.3;\n\n" +
                "                  padding-bottom: 30px;\n\n" +
                "                  font-weight: 400;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .content-padding {\n\n" +
                "                  padding: 20px 0 30px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .mobile-header-padding-right {\n\n" +
                "                  width: 290px;\n\n" +
                "                  text-align: right;\n\n" +
                "                  padding-left: 10px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .mobile-header-padding-left {\n\n" +
                "                  width: 290px;\n\n" +
                "                  text-align: left;\n\n" +
                "                  padding-left: 10px;\n\n" +
                "                  padding-bottom: 8px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .free-text {\n\n" +
                "                  width: 100% !important;\n\n" +
                "                  padding: 10px 60px 0px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .block-rounded {\n\n" +
                "                  border-radius: 5px;\n\n" +
                "                  border: 1px solid #e5e5e5;\n\n" +
                "                  vertical-align: top;\n\n" +
                "                }\n\n" +
                "            \n \n" +
                "                .button {\n\n" +
                "                  padding: 55px 0 0;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .info-block {\n\n" +
                "                  padding: 0 20px;\n\n" +
                "                  width: 260px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .mini-block-container {\n\n" +
                "                  padding: 30px 50px;\n\n" +
                "                  width: 500px;\n\n" +
                "                }\n\n" +
                "            \n\n" +
                "                .mini-block {\n" +
                "                  background-color: #ffffff;\n" +
                "                  width: 498px;\n" +
                "                  border: 1px solid #e1e1e1;\n" +
                "                  border-radius: 5px;\n" +
                "                  padding: 25px 70px 60px;\n" +
                "                }\n" +
                "            \n" +
                "                .block-rounded {\n" +
                "                  width: 260px;\n" +
                "                }\n" +
                "            \n" +
                "                .info-img {\n" +
                "                  width: 258px;\n" +
                "                  border-radius: 5px 5px 0 0;\n" +
                "                }" +
                "             " +
                "                .force-width-img { " +
                "                  width: 480px; " +
                "                  height: 1px !important; " +
                "                } " +
                "             " +
                "                .force-width-full { " +
                "                  width: 600px; " +
                "                  height: 1px !important; " +
                "                } " +
                "             " +
                "                .user-img img { " +
                "                  width: 82px; " +
                "                  border-radius: 5px; " +
                "                  border: 1px solid #cccccc; " +
                "                } " +
                "             " +
                "                .user-img { " +
                "                  width: 92px; " +
                "                  text-align: left; " +
                "                } " +
                "             " +
                "                .user-msg { " +
                "                  width: 236px; " +
                "                  font-size: 14px; " +
                "                  text-align: left; " +
                "                  font-style: italic; " +
                "                } " +
                "             " +
                "                .code-block { " +
                "                  padding: 10px 0; " +
                "                  border: 1px solid #cccccc; " +
                "                  width: 20px; " +
                "                  color: #4d4d4d; " +
                "                  font-weight: bold; " +
                "                  font-size: 18px; " +
                "                } " +
                "             " +
                "                .center-txt { " +
                "                  padding-left: 30px; " +
                "                } " +
                "             " +
                "                .force-width-gmail { " +
                "                  min-width:600px; " +
                "                  height: 0px !important; " +
                "                  line-height: 1px !important; " +
                "                  font-size: 1px !important; " +
                "                } " +
                "             " +
                "              </style> " +
                "             " +
                "              <style type=\\\"text/css\\\" media=\\\"screen\\\"> " +
                "                @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700); " +
                "              </style> " +
                "             " +
                "              <style type=\\\"text/css\\\" media=\\\"screen\\\"> " +
                "                @media screen { " +
                "                  /* Thanks Outlook 2013! http://goo.gl/XLxpyl */ " +
                "                  * { " +
                "                    font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\\n\" +\n" +
                "                  } " +
                "                } " +
                "              </style> " +
                "             " +
                "              <style type=\\\"text/css\\\" media=\\\"only screen and (max-width: 480px)\\\"> " +
                "                /* Mobile styles */ " +
                "                @media only screen and (max-width: 480px) { " +
                "             " +
                "                  table[class*=\\\"container-for-gmail-android\\\"] { " +
                "                    min-width: 290px !important; " +
                "                    width: 100% !important; " +
                "                  } " +
                "             " +
                "                  table[class=\\\"w320\\\"] { " +
                "                    width: 320px !important;" +
                "                  } " +
                "             " +
                "                  img[class=\\\"force-width-gmail\\\"] { " +
                "                    display: none !important; " +
                "                    width: 0 !important; " +
                "                    height: 0 !important; " +
                "                  } " +
                "             " +
                "                  td[class*=\\\"mobile-header-padding-left\\\"] { " +
                "                    width: 160px !important; " +
                "                    padding-left: 0 !important; " +
                "                  } " +
                "             " +
                "                  td[class*=\\\"mobile-header-padding-right\\\"] { " +
                "                    width: 160px !important; " +
                "                    padding-right: 0 !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"header-lg\\\"] { " +
                "                    font-size: 24px !important; " +
                "                    padding: 10px 50px 0 !important; \n" +
                "                  } " +
                "             " +
                "                  td[class=\\\"header-md\\\"] { " +
                "                    font-size: 18px !important; " +
                "                    padding-bottom: 5px !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"content-padding\\\"] {" +
                "                    padding: 5px 0 30px !important; " +
                "                  } " +
                "             " +
                "                   td[class=\\\"button\\\"] { " +
                "                    padding: 5px !important; " +
                "                  } " +
                "             " +
                "                  td[class*=\\\"free-text\\\"] { " +
                "                    padding: 10px 18px 30px !important; " +
                "                  } " +
                "             " +
                "                  img[class=\\\"force-width-img\\\"], " +
                "                  img[class=\\\"force-width-full\\\"] { " +
                "                    display: none !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"info-block\\\"] { " +
                "                    display: block !important; " +
                "                    width: 280px !important; " +
                "                    padding-bottom: 40px !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"info-img\\\"], " +
                "                  img[class=\\\"info-img\\\"] { " +
                "                    width: 278px !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"mini-block-container\\\"] { " +
                "                    padding: 8px 20px !important; " +
                "                    width: 280px !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"mini-block\\\"] { " +
                "                    padding: 20px !important; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"user-img\\\"] { " +
                "                    display: block !important; " +
                "                    text-align: center !important; " +
                "                    width: 100% !important; " +
                "                    padding-bottom: 10px; " +
                "                  } " +
                "             " +
                "                  td[class=\\\"user-msg\\\"] { " +
                "                    display: block !important; " +
                "                    padding-bottom: 20px !important; " +
                "                  }\\n\" +\n" +
                "            \\n\" +\n" +
                "                  td[class=\\\"center-txt\\\"] { " +
                "                    padding-left: 3px !important; " +
                "                  } " +
                "                } " +
                "              </style> " +
                "            </head> " +
                "             " +
                "            <body bgcolor=\\\"#f7f7f7\\\"> " +
                "            <table align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"container-for-gmail-android\\\" width=\\\"100%\\\"> " +
                "              <tr> " +
                "               <td align=\\\"left\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\\\"> " +
                "                  <center> " +
                "                  <img src=\\\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\\\" class=\\\"force-width-gmail\\\"> " +
                "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"100%\\\" bgcolor=\\\"#ffffff\\\" background=\\\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\\\" style=\\\"background-color:transparent\\\">  " +
                "                      <tr> " +
                "                        <td width=\\\"100%\\\" height=\\\"80\\\" valign=\\\"top\\\" style=\\\"text-align: center; vertical-align:middle;\\\"> " +
                "                        <!--[if gte mso 9]> " +
                "                        <v:rect xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" fill=\\\"true\\\" stroke=\\\"false\\\" style=\\\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\\\"> " +
                "                          <v:fill type=\\\"tile\\\" src=\\\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\\\" color=\\\"#ffffff\\\" /> " +
                "                          <v:textbox inset=\\\"0,0,0,0\\\"> " +
                "                        <![endif]--> " +
                "                          <center> " +
                "                            <table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\"> " +
                "                              <tr> " +
                "                                <td class=\\\"pull-left mobile-header-padding-left\\\" style=\\\"vertical-align: middle;\\\"> " +
                "                                  <a href=\\\"\\\"><h3>Maid In Nigeria</h3></a> " +
                "                                </td> " +
/*                "                                <td class=\\\"pull-right mobile-header-padding-right\\\" style=\\\"color: #4d4d4d;\\\"> " +
                "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-twitter.png\\\" alt=\\\"twitter\\\" /></a> " +
                "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-fb.png\\\" alt=\\\"facebook\\\" /></a> " +
                "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-feed.png\\\" alt=\\\"rss\\\" /></a> " +
                "                                </td> "+*/
                "                              </tr>" +
                "                            </table>" +
                "                          </center>" +
                "                          <!--[if gte mso 9]>" +
                "                          </v:textbox>" +
                "                        </v:rect>" +
                "                        <![endif]-->" +
                "                        </td>" +
                "                      </tr>" +
                "                    </table>" +
                "                  </center>" +
                "                </td>" +
                "              </tr>" +
                "              <tr>" +
                "                <td align=\\\"center\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background-color: #f7f7f7;\\\" class=\\\"content-padding\\\">" +
                "                  <center> " +
                "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\"> " +
                "                      <tr> " +
                "                        <td class=\\\"header-lg\\\"> " +
                "                          Notification! " +
                "                        </td> " +
                "                      </tr> " +
                "                      <tr> " +
                "                        <td class=\\\"free-text\\\"> " +
                "            Dear "+employerName+",  <br />" +
                "            You have requested to interview the following worker:\n <br />" +
                "            Ref: MIN0"+workerId+"\n\n <br />" +
                "            Occupation: "+occupation+"\n\n <br />" +
                "            Expected Salary: "+salary+"\n\n <br />" +
                "            Agency Fee: "+agencyFee+"\n\n <br />" +
                "            \n\n <br />" +
                "            The agent will be in contact with you shortly to discuss arrangements for the interview.<br /><br />" +
                "            Please do not hesitate to contact us if you have any questions.<br /><br />\n" +
                "            \n\n\n" +
                "            Thank you,<br />" +
                "            MaidInNigeria" +
                "\n" +
                "                        </td>\n\n" +
                "                      </tr>\n\n" +
                "                      <tr>\n\n" +
                "                        <td class=\\\"mini-block-container\\\">\n\n" +
                "            Note:You are advised to complete the hire process from the portal <br />" +
                "             MaidInNigeria will not be responsible for any negligency..." +
                "                        </td>" +
                "\n" +           "                      </tr>" +
                "                   </table>\n\n" +
                "                  </center>\n\n" +
                "                </td>\n\n" +
                "              </tr>\n\n" +
                "              <tr>\n\n" +
                "                <td align=\"center\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">" +
                "                  <center>\n\n" +
                "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\"> " +
                "                      <tr>" +
                "                        <td style=\\\"padding: 25px 0 25px\\\"> " +
                "                          <strong>Maid In Nigeria</strong><br /> " +
                "                          Lagos, Nigeria <br />contact@maidinnigeria.com.ng " +
                "                           <br /><br />" +
                "                        </td>" +
                "                      </tr>" +
                "                    </table>" +
                "                  </center>" +
                "                </td>" +
                "              </tr>" +
                "            </table>";

            try {
                String Agentmessage = "<style type=\"text/css\">\n" +
                        "    /* Take care of image borders and formatting, client hacks */\n" +
                        "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                        "    a img { border: none; }\n" +
                        "    table { border-collapse: collapse !important;}\n" +
                        "    #outlook a { padding:0; }\n" +
                        "    .ReadMsgBody { width: 100%; }\n" +
                        "    .ExternalClass { width: 100%; }\n" +
                        "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                        "    table td { border-collapse: collapse; }\n" +
                        "    .ExternalClass * { line-height: 115%; }\n" +
                        "    .container-for-gmail-android { min-width: 600px; }\n" +
                        "\n" +
                        "\n" +
                        "    /* General styling */\n" +
                        "    * {\n" +
                        "      font-family: Helvetica, Arial, sans-serif;\n" +
                        "    }\n" +
                        "\n" +
                        "    body {\n" +
                        "      -webkit-font-smoothing: antialiased;\n" +
                        "      -webkit-text-size-adjust: none;\n" +
                        "      width: 100% !important;\n" +
                        "      margin: 0 !important;\n" +
                        "      height: 100%;\n" +
                        "      color: #676767;\n" +
                        "    }\n" +
                        "\n" +
                        "    td {\n" +
                        "      font-family: Helvetica, Arial, sans-serif;\n" +
                        "      font-size: 14px;\n" +
                        "      color: #777777;\n" +
                        "      text-align: center;\n" +
                        "      line-height: 21px;\n" +
                        "    }\n" +
                        "\n" +
                        "    a {\n" +
                        "      color: #676767;\n" +
                        "      text-decoration: none !important;\n" +
                        "    }\n" +
                        "\n" +
                        "    .pull-left {\n" +
                        "      text-align: left;\n" +
                        "    }\n" +
                        "\n" +
                        "    .pull-right {\n" +
                        "      text-align: right;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-lg,\n" +
                        "    .header-md,\n" +
                        "    .header-sm {\n" +
                        "      font-size: 32px;\n" +
                        "      font-weight: 700;\n" +
                        "      line-height: normal;\n" +
                        "      padding: 35px 0 0;\n" +
                        "      color: #4d4d4d;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-md {\n" +
                        "      font-size: 24px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-sm {\n" +
                        "      padding: 5px 0;\n" +
                        "      font-size: 18px;\n" +
                        "      line-height: 1.3;\n" +
                        "      padding-bottom: 30px;\n" +
                        "      font-weight: 400;\n" +
                        "    }\n" +
                        "\n" +
                        "    .content-padding {\n" +
                        "      padding: 20px 0 30px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mobile-header-padding-right {\n" +
                        "      width: 290px;\n" +
                        "      text-align: right;\n" +
                        "      padding-left: 10px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mobile-header-padding-left {\n" +
                        "      width: 290px;\n" +
                        "      text-align: left;\n" +
                        "      padding-left: 10px;\n" +
                        "      padding-bottom: 8px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .free-text {\n" +
                        "      width: 100% !important;\n" +
                        "      padding: 10px 60px 0px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .block-rounded {\n" +
                        "      border-radius: 5px;\n" +
                        "      border: 1px solid #e5e5e5;\n" +
                        "      vertical-align: top;\n" +
                        "    }\n" +
                        "\n" +
                        "    .button {\n" +
                        "      padding: 55px 0 0;\n" +
                        "    }\n" +
                        "\n" +
                        "    .info-block {\n" +
                        "      padding: 0 20px;\n" +
                        "      width: 260px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mini-block-container {\n" +
                        "      padding: 30px 50px;\n" +
                        "      width: 500px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mini-block {\n" +
                        "      background-color: #ffffff;\n" +
                        "      width: 498px;\n" +
                        "      border: 1px solid #e1e1e1;\n" +
                        "      border-radius: 5px;\n" +
                        "      padding: 25px 70px 60px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .block-rounded {\n" +
                        "      width: 260px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .info-img {\n" +
                        "      width: 258px;\n" +
                        "      border-radius: 5px 5px 0 0;\n" +
                        "    }\n" +
                        "\n" +
                        "    .force-width-img {\n" +
                        "      width: 480px;\n" +
                        "      height: 1px !important;\n" +
                        "    }\n" +
                        "\n" +
                        "    .force-width-full {\n" +
                        "      width: 600px;\n" +
                        "      height: 1px !important;\n" +
                        "    }\n" +
                        "\n" +
                        "    .user-img img {\n" +
                        "      width: 82px;\n" +
                        "      border-radius: 5px;\n" +
                        "      border: 1px solid #cccccc;\n" +
                        "    }\n" +
                        "\n" +
                        "    .user-img {\n" +
                        "      width: 92px;\n" +
                        "      text-align: left;\n" +
                        "    }\n" +
                        "\n" +
                        "    .user-msg {\n" +
                        "      width: 236px;\n" +
                        "      font-size: 14px;\n" +
                        "      text-align: left;\n" +
                        "      font-style: italic;\n" +
                        "    }\n" +
                        "\n" +
                        "    .code-block {\n" +
                        "      padding: 10px 0;\n" +
                        "      border: 1px solid #cccccc;\n" +
                        "      width: 20px;\n" +
                        "      color: #4d4d4d;\n" +
                        "      font-weight: bold;\n" +
                        "      font-size: 18px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .center-txt {\n" +
                        "      padding-left: 30px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .force-width-gmail {\n" +
                        "      min-width:600px;\n" +
                        "      height: 0px !important;\n" +
                        "      line-height: 1px !important;\n" +
                        "      font-size: 1px !important;\n" +
                        "    }\n" +
                        "\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"screen\">\n" +
                        "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"screen\">\n" +
                        "    @media screen {\n" +
                        "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                        "      * {\n" +
                        "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                        "      }\n" +
                        "    }\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                        "    /* Mobile styles */\n" +
                        "    @media only screen and (max-width: 480px) {\n" +
                        "\n" +
                        "      table[class*=\"container-for-gmail-android\"] {\n" +
                        "        min-width: 290px !important;\n" +
                        "        width: 100% !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      table[class=\"w320\"] {\n" +
                        "        width: 320px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      img[class=\"force-width-gmail\"] {\n" +
                        "        display: none !important;\n" +
                        "        width: 0 !important;\n" +
                        "        height: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"mobile-header-padding-left\"] {\n" +
                        "        width: 160px !important;\n" +
                        "        padding-left: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"mobile-header-padding-right\"] {\n" +
                        "        width: 160px !important;\n" +
                        "        padding-right: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"header-lg\"] {\n" +
                        "        font-size: 24px !important;\n" +
                        "        padding: 10px 50px 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"header-md\"] {\n" +
                        "        font-size: 18px !important;\n" +
                        "        padding-bottom: 5px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"content-padding\"] {\n" +
                        "        padding: 5px 0 30px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "       td[class=\"button\"] {\n" +
                        "        padding: 5px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"free-text\"] {\n" +
                        "        padding: 10px 18px 30px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      img[class=\"force-width-img\"],\n" +
                        "      img[class=\"force-width-full\"] {\n" +
                        "        display: none !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"info-block\"] {\n" +
                        "        display: block !important;\n" +
                        "        width: 280px !important;\n" +
                        "        padding-bottom: 40px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"info-img\"],\n" +
                        "      img[class=\"info-img\"] {\n" +
                        "        width: 278px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"mini-block-container\"] {\n" +
                        "        padding: 8px 20px !important;\n" +
                        "        width: 280px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"mini-block\"] {\n" +
                        "        padding: 20px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"user-img\"] {\n" +
                        "        display: block !important;\n" +
                        "        text-align: center !important;\n" +
                        "        width: 100% !important;\n" +
                        "        padding-bottom: 10px;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"user-msg\"] {\n" +
                        "        display: block !important;\n" +
                        "        padding-bottom: 20px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"center-txt\"] {\n" +
                        "        padding-left: 3px !important;\n" +
                        "      }\n" +
                        "    }\n" +
                        "  </style>\n" +
                        "</head>\n" +
                        "\n" +
                        "<body bgcolor=\"#f7f7f7\">\n" +
                        "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                        "  <tr>\n" +
                        "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                        "      <center>\n" +
                        "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                        "          <tr>\n" +
                        "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                        "            <!--[if gte mso 9]>\n" +
                        "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                        "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                        "              <v:textbox inset=\"0,0,0,0\">\n" +
                        "            <![endif]-->\n" +
                        "              <center>\n" +
                        "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                        "                  <tr>\n" +
                        "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                        "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                        "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
                        "                  </tr>\n" +
                        "                </table>\n" +
                        "              </center>\n" +
                        "              <!--[if gte mso 9]>\n" +
                        "              </v:textbox>\n" +
                        "            </v:rect>\n" +
                        "            <![endif]-->\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                        "      <center>\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                        "          <tr>\n" +
                        "            <td class=\"header-lg\">\n" +
                        "              Notification!\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"free-text\">\n" +
                        "\n" +
                        "\n" +
                        "\n" + "\n" +
                        "         </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "<tr>    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                        "      \n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                        "          <tr>\n" +
                        "            <td class=\"header-lg\">\n" +
                        "              \n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"free-text\">\n" +
                        "REQUEST TO INTERVIEW DOMESTIC STAFF <br /> <br />" +
                        "\n" +
                        "\n" + "\n" +
                        "Dear "+agentName+",\n <br />" +
                        "The user "+ employerName+" has requested to interview the following worker:\n <br />" +
                        "Ref: MIN0"+workerId+"\n <br />" +
                        "Occupation: "+occupation+"\n <br />" +
                        "Expected Salary: "+salary+"\n <br />" +
                        "Agency Fee: "+agencyFee+"\n <br />" +
                        "\n" +
                        "Please contact "+employerName+" on "+mobile+" to discuss arrangements for the interview.\n <br />" +

                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"mini-block-container\">\n" +
                        "Note: Make contact if the requested maid is available.. \n\n <br />" +
                        "Please do not hesitate to contact us if you have any questions.\n <br /><br />" +
                        "\n" +
                        "Thank you,\n<br />" +
                        "MaidInNigeria\n " +
                        "            </td>\n" +

                        "          </tr>\n" +
                        "        </table>\n" +
                        "      \n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
                        "      <center>\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                        "          <tr>\n" +
                        "            <td style=\"padding: 25px 0 25px\">\n" +
                        "              <strong>Maid In Nigeria</strong><br />\n" +
                        "              Lagos, Nigeria <br /> contact@maidinnigeria.com.ng\n" +
                        "               <br /><br />\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "</table>\n" +
                        "";

                Calendar date = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String requestDate = sdf.format(date.getTime());
                System.out.println(requestDate);

                Class.forName(DbConstants.DRIVER);
                connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
                statement = connection.createStatement();
                //String sql = "UPDATE users SET requested_maid = '"+workerName+"' where users_username = '"+username+"'";

                //statement.executeUpdate(sql);

                RequestedMaids requestedMaids = new RequestedMaids();

                //worker foreign key
                Workers workers = new Workers();
                workers.setWorkers_id(Integer.parseInt(workerId));
                Workers_dao workersDao = new Workers_dao(workers);
                workersDao.refreshWorker();

                //user foreign key
                Users users = new Users();
                users.setUsers_id(Integer.parseInt(userId));
                Users_dao usersDao = new Users_dao(users);
                usersDao.refreshUsers();

                //create requested maids
                requestedMaids.setWorkers_id(workers);
                requestedMaids.setUsers_id(users);
                requestedMaids.setRequestDate(requestDate);

                RequestedMaidDao requestedMaidDao = new RequestedMaidDao(requestedMaids);
                requestedMaidDao.createRequestedMaid();

                resultSet = statement.executeQuery("SELECT agent_email from agents WHERE agent_Company_Name = '" + agentName + "'");
                SendGridMailer mailer1 = new SendGridMailer();
                while (resultSet.next()) {
//                    MailHelper mailHelper = new MailHelper();
//                    mailHelper.sendMessage(resultSet.getString("agent_email"),Agentmessage,"MaidInNigeria REQUEST FOR INTERVIEW");
//                    mailHelper.sendMessage(MailConstants.REQUEST_MAIL,ADM_REQ_MSG,"MaidInNigeria REQUEST FOR INTERVIEW");
//                    mailHelper.sendMessage(empMail,USER_REQ_MSG,"MaidInNigeria REQUEST FOR INTERVIEW");

                    SimpleMailSender mailer = new SimpleMailSender();
                    mailer.sendMessage(resultSet.getString("agent_email"),"MaidInNigeria REQUEST FOR INTERVIEW",Agentmessage);
                    mailer.sendMessage(MailConstants.REQUEST_MAIL,"MaidInNigeria REQUEST FOR INTERRVIEW",ADM_REQ_MSG);
                    mailer.sendMessage(empMail,"MaidInNigeria REQUEST FOR INTERVIEW", USER_REQ_MSG);

                }

                response.sendRedirect("/min/confirmation.jsp");

            } catch (Exception ex) {
                ex.printStackTrace();
                response.sendRedirect("/min/SignInFirst.jsp");
            } finally {
                ConnectionDriver.close(resultSet);
                ConnectionDriver.close(statement);
                ConnectionDriver.close(connection);
            }

            writer.println(jsonObject.toString());
    }
}