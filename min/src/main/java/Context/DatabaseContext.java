package Context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import Dao.DbConstants;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;

import Models.*;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by user on 9/28/2015.
 */

public class DatabaseContext implements ServletContextListener {

    JdbcPooledConnectionSource con;



    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {

            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = loader.getResourceAsStream("app.properties");
            Properties prop = new Properties();
            prop.load(inputStream);
            String condition = prop.getProperty("CREATE_TABLES");

                if (condition == "true") {
                    con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
                    con.setUsername(DbConstants.DB_USER);
                    con.setPassword(DbConstants.DB_PWD);


                    TableUtils.createTableIfNotExists(con, Agents.class);
                    TableUtils.createTableIfNotExists(con, Guarantor.class);
                    TableUtils.createTableIfNotExists(con, Users.class);
                    TableUtils.createTableIfNotExists(con, Workers.class);
                    TableUtils.createTableIfNotExists(con, UnassignWorkers.class);
                    TableUtils.createTableIfNotExists(con, Admin.class);
                    TableUtils.createTableIfNotExists(con, Insurance.class);
                    TableUtils.createTableIfNotExists(con, BackgroundCheck.class);
                    TableUtils.createTableIfNotExists(con, Reference.class);
                    TableUtils.createTableIfNotExists(con, RequestedMaids.class);
                    TableUtils.createTableIfNotExists(con, HiredMaid.class);
                    TableUtils.createTableIfNotExists(con, DisengagedMaid.class);
                    TableUtils.createTableIfNotExists(con, PendingHire.class);
                    TableUtils.createTableIfNotExists(con, QueryLog.class);
                }
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            con.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
