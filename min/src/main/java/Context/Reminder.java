package Context;

import Dao.DbConstants;
import Mailer.MailHelper;
import Context.ConnectionDriver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.*;

/**
 * Created by user on 6/27/2016.
 */
public class Reminder extends HttpServlet{

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("");

            while(resultSet.next()){
                Date current = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String currentDate = sdf.format(current);

                if(currentDate.equals(current)){
                    MailHelper mailHelper = new MailHelper();
                    mailHelper.sendMessage(resultSet.getString(""),"","");
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
