package Dao;

import Models.RequestedMaids;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 6/2/2016.
 */
public class RequestedMaidDao {
    RequestedMaids requestedMaid;
    JdbcPooledConnectionSource connectionSource;

    public RequestedMaidDao(RequestedMaids requestedMaid) throws Exception {
        this.requestedMaid = requestedMaid;
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public RequestedMaidDao() throws Exception {
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public void createRequestedMaid() throws Exception {
        try{
            Dao<RequestedMaids,String> requestedMaidDao = DaoManager.createDao(connectionSource,RequestedMaids.class);
            requestedMaidDao.create(requestedMaid);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public List<RequestedMaids> getRequestedMaids() throws Exception {
        Dao<RequestedMaids,String> requestedMaidDao = DaoManager.createDao(connectionSource,RequestedMaids.class);
        return requestedMaidDao.queryForAll();
    }
}
