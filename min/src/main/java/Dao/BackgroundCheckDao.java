package Dao;

import Models.BackgroundCheck;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 3/3/2016.
 */
public class BackgroundCheckDao {
    JdbcPooledConnectionSource con;
    BackgroundCheck backgroundCheck;

    public BackgroundCheckDao(BackgroundCheck backgroundCheck) throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);

        this.backgroundCheck = backgroundCheck;
    }

    public void createBackgroundCheck() throws Exception{
        Dao<BackgroundCheck,String> checkDao = DaoManager.createDao(con,BackgroundCheck.class);
        checkDao.create(backgroundCheck);
    }
}
