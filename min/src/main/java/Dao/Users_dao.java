package Dao;

import Models.Users;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 9/28/2015.
 */
public class Users_dao {
    Users users;
    JdbcPooledConnectionSource con;

    public Users_dao(Users users) throws Exception{
        this.users = users;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);

    }

    public Users_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createUsers() throws Exception{
        try {
            Dao<Users, String> usersDao = DaoManager.createDao(con, Users.class);
            usersDao.create(users);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public boolean authenticateUsers(String username, String password) throws Exception{
      try {
          Dao<Users, String> usersDao = DaoManager.createDao(con, Users.class);
          List<Users> list = usersDao.queryForAll();
          for (int i = 0; i < list.size(); i++) {
              users = list.get(i);
              if (users.getUsers_username().equals(username) && users.getUsers_password().equals(password)) {
                  return true;
              }
          }
      }catch(Exception exp ){exp.printStackTrace();}
        return false;
    }

    public List<Users> getUsers()throws Exception {
        Dao<Users,String> Dao = DaoManager.createDao(con,Users.class);
        return Dao.queryForAll();

    }

    public void refreshUsers() throws Exception {
        Dao<Users,String> workerDao = DaoManager.createDao(con,Users.class);
        workerDao.refresh(users);
    }
}
