package Dao;

import Models.PendingHire;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 6/20/2016.
 */
public class PendingHireDao {
    PendingHire pendingHire;
    JdbcPooledConnectionSource con;

    public PendingHireDao(PendingHire pendingHire) throws Exception {
        this.pendingHire = pendingHire;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public PendingHireDao() throws Exception {
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createPending() throws Exception {
        try{
            Dao<PendingHire,String> pendingDao = DaoManager.createDao(con,PendingHire.class);
            pendingDao.create(pendingHire);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public List<PendingHire> allPendings() throws Exception {
        Dao<PendingHire,String> pendingDao = DaoManager.createDao(con,PendingHire.class);
        return pendingDao.queryForAll();
    }
}
