package Dao;

import Models.Guarantor;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 9/28/2015.
 */
public class Guarantor_dao {
    JdbcPooledConnectionSource con;
    Guarantor guarantor;

    public Guarantor_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public Guarantor_dao(Guarantor guarantor) throws Exception{
        this.guarantor = guarantor;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createGuarantor() throws Exception{
        Dao<Guarantor,String> guaranDao = DaoManager.createDao(con,Guarantor.class);
        guaranDao.create(guarantor);
    }

    public void getGuarantor(String workersName) throws Exception{

    }
}
