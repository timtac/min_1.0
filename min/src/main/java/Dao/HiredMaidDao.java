package Dao;

import Models.HiredMaid;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 6/2/2016.
 */
public class HiredMaidDao {
    JdbcPooledConnectionSource connectionSource;
    HiredMaid hiredMaid;

    public HiredMaidDao(HiredMaid hiredMaid) throws Exception {
        this.hiredMaid = hiredMaid;
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public HiredMaidDao() throws Exception {
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER, DbConstants.DB_PWD);
    }

    public void createHiredMaid() throws Exception {
        try {
            Dao<HiredMaid, Integer> hiredMaidsDao = DaoManager.createDao(connectionSource, HiredMaid.class);
            hiredMaidsDao.create(hiredMaid);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public List<HiredMaid> queryAllHiredMaid() throws Exception {
        Dao<HiredMaid,Integer> hiredMaidsDao = DaoManager.createDao(connectionSource, HiredMaid.class);
        return hiredMaidsDao.queryForAll();
    }
}
