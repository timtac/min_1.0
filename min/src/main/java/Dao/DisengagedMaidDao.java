package Dao;

import Models.DisengagedMaid;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 6/6/2016.
 */
public class DisengagedMaidDao {
    DisengagedMaid disengagedMaid;
    JdbcPooledConnectionSource con;

    public DisengagedMaidDao(DisengagedMaid disengagedMaid) throws Exception {
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
        this.disengagedMaid = disengagedMaid;
    }

    public DisengagedMaidDao() throws Exception {
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public void createDisengaged() throws Exception {
        try{
            Dao<DisengagedMaid,String> disengagedMaidsDao = DaoManager.createDao(con,DisengagedMaid.class);
            disengagedMaidsDao.create(disengagedMaid);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
