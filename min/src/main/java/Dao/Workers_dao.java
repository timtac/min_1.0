package Dao;

import Models.BackgroundCheck;
import Models.Guarantor;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import Models.Workers;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;
/**
 * Created by user on 9/28/2015.
 */
public class Workers_dao {
    Workers workers;
    JdbcPooledConnectionSource con;

    public Workers_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public Workers_dao(Workers workers) throws Exception{
        this.workers = workers;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createWorker() throws Exception{
        Dao<Workers,String> workersdao = DaoManager.createDao(con,Workers.class);
        workersdao.create(workers);
    }

    public int updateWorker() throws Exception{
        Dao<Workers,String> workersdao = DaoManager.createDao(con,Workers.class);
        UpdateBuilder<Workers,String> workersStringUpdateBuilder = workersdao.updateBuilder();
        return 1;
    }

    public void deleteWorker() throws Exception{

    }

    public List<Workers> getWorkerByAgent(String agentComname) throws  Exception{
        Dao<Workers,String> workersdao = DaoManager.createDao(con,Workers.class);
        QueryBuilder<Workers,String> qb = workersdao.queryBuilder();
        Where where = qb.where();
        //where.eq(workers.getAgent_company(),agentComname);
        PreparedQuery<Workers> preparedQuery = qb.prepare();
        return workersdao.query(preparedQuery);
    }

    public List<Workers> workersJoinGuarantor(int id) throws Exception {
        Dao<Workers,Integer> workersDao = DaoManager.createDao(con,Workers.class);
        QueryBuilder<Workers,Integer> workerQb = workersDao.queryBuilder();

        //create Dao for guarantor
        Dao<Guarantor,Integer> guarantorDao = DaoManager.createDao(con,Guarantor.class);
        QueryBuilder<Guarantor,Integer> guarantorQb = guarantorDao.queryBuilder();
        guarantorQb.where().eq("workers_id",id);
        //creates Dao for background check
        Dao<BackgroundCheck,Integer> backgroundCheckDao = DaoManager.createDao(con,BackgroundCheck.class);
        QueryBuilder<BackgroundCheck,Integer> backgroundQb = backgroundCheckDao.queryBuilder();
        backgroundQb.where().eq("workers_id",id);

        List<Workers> result = workerQb.join(guarantorQb).join(backgroundQb).query();
        return result;
    }


    public List<Workers> getAllWorkers()throws Exception {
        Dao<Workers,String> workerDao = DaoManager.createDao(con,Workers.class);
        return workerDao.queryForAll();

    }

    public void refreshWorker() throws Exception {
        Dao<Workers,String> workerDao = DaoManager.createDao(con,Workers.class);
        workerDao.refresh(workers);
    }
}
