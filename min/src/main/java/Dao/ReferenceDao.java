package Dao;

import Models.Reference;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 6/2/2016.
 */
public class ReferenceDao {
    JdbcPooledConnectionSource connectionSource;
    Reference reference;

    public ReferenceDao(Reference reference) throws Exception {
        this.reference = reference;
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public ReferenceDao() throws Exception {
        connectionSource = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public void createReference() throws Exception {
        try{
            Dao<Reference,String> referenceDao = DaoManager.createDao(connectionSource,Reference.class);
            referenceDao.create(reference);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
