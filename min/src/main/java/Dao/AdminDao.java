package Dao;

import Models.Admin;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 2/10/2016.
 */
public class AdminDao {
    JdbcPooledConnectionSource con;
    Admin admin;

    public AdminDao(Admin admin) throws Exception{
        this.admin = admin;

        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public AdminDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createAdmin() throws Exception{
        try {
            Dao<Admin, String> adminDao = DaoManager.createDao(con, Admin.class);
            adminDao.create(admin);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public boolean authenticateAdmin(String username, String password) throws Exception{
        try {
            Dao<Admin, String> agentdao = DaoManager.createDao(con, Admin.class);
            List<Admin> list = agentdao.queryForAll();
            for (int i = 0; i < list.size(); i++) {
                admin = list.get(i);
                if (admin.getUsername().equals(username) && admin.getPassword().equals(password)) {

                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
