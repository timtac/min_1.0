package Dao;

import Models.Agents;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by user on 9/28/2015.
 */
public class Agents_dao {

    JdbcPooledConnectionSource con;
    Agents agents;
    String[] mail2;


    public Agents_dao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setPassword(DbConstants.DB_PWD);
        con.setUsername(DbConstants.DB_USER);
    }

    public Agents_dao(Agents agents) throws Exception{
        this.agents = agents;

        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setPassword(DbConstants.DB_PWD);
    }

    public void createAgent() throws Exception{
        Dao<Agents,String> agentdao = DaoManager.createDao(con,Agents.class);
        agentdao.create(agents);
    }

    public void updateAgent(Agents agents,String username) throws Exception{
        Dao<Agents,String> agentdao = DaoManager.createDao(con,Agents.class);
        agentdao.updateId(agents,username);
    }

    public boolean authenticateAgents(String username, String password) throws Exception{
        try {
            Dao<Agents, String> agentdao = DaoManager.createDao(con, Agents.class);
            List<Agents> list = agentdao.queryForAll();
            for (int i = 0; i < list.size(); i++) {
                agents = list.get(i);
                if (agents.getAgent_username().equals(username) && agents.getAgent_password().equals(password)) {
                    return true;
                }
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    public List<Agents> getCompName(String username) throws Exception{
        Dao<Agents,String> agentDao = DaoManager.createDao(con,Agents.class);
        QueryBuilder<Agents,String> queryBuilder = agentDao.queryBuilder();
        Where<Agents,String> where = queryBuilder.where();
        where.eq(agents.getAgent_username(),username);
        PreparedQuery<Agents> preparedQuery = queryBuilder.prepare();

        return agentDao.query(preparedQuery);
    }

    public String[] getAgentsMail()throws Exception{
        Dao<Agents,String> agentDao = DaoManager.createDao(con,Agents.class);
        GenericRawResults<String[]> rawResults = agentDao.queryRaw("select e_mail from agents");
        List<String[]> list = rawResults.getResults();
            for(int i = 0;i < list.size(); i++) {
                 mail2 = list.get(i);
            }
        return mail2;
    }

    public void refresh() throws Exception {
        Dao<Agents,String> agentDao = DaoManager.createDao(con,Agents.class);
        agentDao.refresh(agents);
    }

    public List<Agents> allAgents() throws Exception{
        Dao<Agents,String> agentDao = DaoManager.createDao(con,Agents.class);
        return agentDao.queryForAll();
    }

}
