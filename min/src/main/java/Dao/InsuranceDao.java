package Dao;

import Models.Insurance;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 2/25/2016.
 */
public class InsuranceDao {
    JdbcPooledConnectionSource con;
    Insurance insurance;

    public InsuranceDao(Insurance insurance) throws Exception{
        this.insurance = insurance;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL);
        con.setUsername(DbConstants.DB_USER);
        con.setUsername(DbConstants.DB_PWD);
    }

    public void createInsurance() throws Exception{
        Dao<Insurance,String> insuranceDao = DaoManager.createDao(con,Insurance.class);
        insuranceDao.create(insurance);
    }
}
