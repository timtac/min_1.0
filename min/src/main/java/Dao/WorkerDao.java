package Dao;

import Models.UnassignWorkers;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.util.List;

/**
 * Created by user on 4/7/2016.
 */
public class WorkerDao {
    JdbcPooledConnectionSource con;
    UnassignWorkers unassignWorkers;

    public WorkerDao(UnassignWorkers unassignWorkers) throws Exception{
        this.unassignWorkers = unassignWorkers;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public WorkerDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public void createWorker() throws Exception {
        Dao<UnassignWorkers,String> workerDao = DaoManager.createDao(con, UnassignWorkers.class);
        workerDao.create(unassignWorkers);
    }

    public List<UnassignWorkers> getAllWorker() throws Exception{
        Dao<UnassignWorkers,String> unassignWorkerDao = DaoManager.createDao(con,UnassignWorkers.class);
        return unassignWorkerDao.queryForAll();
    }
}