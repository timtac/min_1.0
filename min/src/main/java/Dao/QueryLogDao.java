package Dao;

import Models.QueryLog;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 7/11/2016.
 */
public class QueryLogDao {
    JdbcPooledConnectionSource con;
    QueryLog queryLog;

    public QueryLogDao() throws Exception {
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public QueryLogDao(QueryLog queryLog) throws Exception {
        this.queryLog = queryLog;
        con = new JdbcPooledConnectionSource(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
    }

    public void createLog() throws Exception {
        try{
            Dao<QueryLog,Integer> queryLogsDao = DaoManager.createDao(con,QueryLog.class);
            queryLogsDao.create(queryLog);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
