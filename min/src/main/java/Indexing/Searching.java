package Indexing;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Dao.QueryLogDao;
import Dao.Users_dao;
import Models.QueryLog;
import Models.Users;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 2/23/2016.
 */
@WebServlet("/search")
public class Searching extends HttpServlet {

    private static final String SAVE_DIR = "index";
    Connection connection;
    Statement statement;
    ResultSet resultSet;


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("search");
        String userid = request.getParameter("userId");
        JsonArray jsonArray = new JsonArray();
        PrintWriter writer = response.getWriter();

/*
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(DbConstants.DB_URL);
        dataSource.setUser(DbConstants.DB_USER);
        dataSource.setPassword(DbConstants.DB_PWD);*/

            Indexing indexing = new Indexing();
        try {

            indexing.doPost(request, response);

            System.out.println("got here in searching 1");
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            /*JdbcDirectory jdbcDirectory = new JdbcDirectory(dataSource, new MySQLDialect(),"indexTable");
            jdbcDirectory.openInput("indexTable");*/
            File file = new File("index");
            File filedir = new File(getServletContext().getRealPath("")+ File.separator + SAVE_DIR);
            IndexReader reader = IndexReader.open(FSDirectory.open(file), true);
            IndexSearcher searcher = new IndexSearcher(reader);
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
            //String[] searchFields = {"age","occupation","sex","salary","location"};
            //MultiFieldQueryParser multiFieldQueryParser = new MultiFieldQueryParser(Version.LUCENE_29,searchFields,analyzer);
            QueryParser parser = new QueryParser(Version.LUCENE_30,"content",analyzer);
            parser.setDefaultOperator(QueryParser.Operator.AND);
            //multiFieldQueryParser.setDefaultOperator(QueryParser.Operator.AND);
            System.out.println("got here in searching 2");
            Query query1 = parser.parse(query);
            TopDocs hits = searcher.search(query1,10);
            System.out.println("query " + query1);
            System.out.println("hits " + hits.totalHits);
            System.out.println();

//                System.out.println("got into else");
                String sql = "select * from workers where workers_id = ?";
                PreparedStatement psmt = connection.prepareStatement(sql);
                for (int i = 0; hits.totalHits > i; i++) {

                    Document doc = searcher.doc(hits.scoreDocs[i].doc);
                    String id = doc.get("id");
                    System.out.println(id);

                    psmt.setString(1, id);

                    ResultSet rs = psmt.executeQuery();
                    while(rs.next()) {
                        JsonObject object = new JsonObject();
                        object.addProperty("id", rs.getString("workers_id"));
                        object.addProperty("first", rs.getString("workers_fullname"));
                        object.addProperty("age", rs.getString("worker_age"));
                        object.addProperty("sex", rs.getString("worker_sex"));
                        object.addProperty("agent", rs.getString("agent_Company_Name"));
                        object.addProperty("work", rs.getString("worker_occupation"));
                        object.addProperty("location", rs.getString("worker_location"));
                        object.addProperty("medical", rs.getString("worker_medical_history"));
                        object.addProperty("expectedSal", rs.getString("expected_salary"));
                        object.addProperty("agencyFee", rs.getString("agency_fee"));
                        object.addProperty("type",rs.getString("agency_fee_type"));
                        System.out.println("done!!!");
                        jsonArray.add(object);
                    }
                }
//response.sendRedirect("/min/search.jsp");
            try{
                Calendar date = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String searchDate = sdf.format(date.getTime());

                if(userid == null){
                    userid = "0";
                }

                Users users = new Users();
                users.setUsers_id(Integer.parseInt(userid));
                Users_dao usersDao = new Users_dao(users);
                usersDao.refreshUsers();

                QueryLog queryLog = new QueryLog();
                queryLog.setUsers_id(users);
                queryLog.setQueryData(query);
                queryLog.setQueryDate(searchDate);
                QueryLogDao queryLogDao = new QueryLogDao(queryLog);
                queryLogDao.createLog();
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(resultSet);
        }
       // writer.println(object.toString());
        writer.println(jsonArray.toString());
        writer.close();

    }



    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
