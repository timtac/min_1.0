package Indexing;


import Context.ConnectionDriver;
import Dao.DbConstants;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.*;
/**
 * Created by user on 2/22/2016.
 */
public class Indexing extends HttpServlet{

    Connection connection;
    Statement statement;
    private static final String SAVE_DIR = "index";
    final File file = new File("index");
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }
        try {
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            Directory directory = new SimpleFSDirectory(file);

            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            IndexWriter writer = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED);

            indexDocs(writer, connection);

            writer.optimize();
            writer.commit();
            writer.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }

    public void indexDocs(IndexWriter writer, Connection connection) throws Exception{
        try {
            this.connection = connection;
            String sql = "SELECT workers_id,worker_age,worker_sex,worker_location,worker_occupation,expected_salary from workers where employed='Unemployed'";
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Document d = new Document();
                d.add(new Field("id", resultSet.getString("workers_id"), Field.Store.YES, Field.Index.ANALYZED));
                d.add(new Field("content", resultSet.getString("worker_age"), Field.Store.YES, Field.Index.ANALYZED));
                d.add(new Field("content", resultSet.getString("worker_occupation"), Field.Store.YES, Field.Index.ANALYZED));
                d.add(new Field("content", resultSet.getString("worker_sex"), Field.Store.YES, Field.Index.ANALYZED));
                d.add(new Field("content", resultSet.getString("worker_location"), Field.Store.YES, Field.Index.ANALYZED));
                d.add(new Field("content", resultSet.getString("expected_salary"), Field.Store.YES, Field.Index.ANALYZED));

                writer.addDocument(d);
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
