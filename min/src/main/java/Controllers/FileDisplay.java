package Controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by user on 6/29/2016.
 */
public class FileDisplay extends HttpServlet {

    private static final String SAVE_DIR = "medicalReports";

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        try {
            File file = new File(getServletContext().getRealPath("") + File.separator + SAVE_DIR + File.separator + fileName);
            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server." + fileName);
            }

            FileInputStream fileInputStream = new FileInputStream(file);
            org.apache.commons.io.IOUtils.copy(fileInputStream,response.getOutputStream());
            response.setContentType("application/pdf");
            response.setHeader("Content-Dispoition","attachment; filename=" + file);
            response.flushBuffer();
        } catch (Exception ex) {

        }

    }
}
