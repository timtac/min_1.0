package Controllers;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Dao.Users_dao;
import EncrytHelper.EncrytoClass;
import Mailer.MessageLogics.MailConstants;
import Mailer.SendGridMailer;
import Mailer.SimpleMailSender;
import com.google.gson.JsonObject;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 9/29/2015.
 */
@WebServlet("/registernewuser")
public class UsersServlet extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String location = request.getParameter("location");
        String address = request.getParameter("address");
        String e_mail = request.getParameter("e_mail");
        String phone_number = request.getParameter("phoneno");
        String gender = request.getParameter("gender");

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        JsonObject jsonObject = new JsonObject();

        try {

            Calendar date = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String SignupDate = sdf.format(date.getTime());
            System.out.println(SignupDate);

            EncrytoClass encrytoClass = new EncrytoClass();
            String key = username;
            String encPassword = encrytoClass.encrypt(key, password);
            Models.Users users = new Models.Users();
            users.setUser_title(title);
            users.setUsers_fullname(fullname);
            users.setUsers_username(username);
            users.setUsers_password(encPassword);
            users.setUsers_location(location);
            users.setUsers_gender(gender);
            users.setUsers_address(address);
            users.setUser_email(e_mail);
            users.setUser_mobile(phone_number);
            users.setSignupDate(SignupDate);

            new Users_dao(users).createUsers();

            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * FROM users where users_username = '"+username+"'");

            while (resultSet.next()){
                session.setAttribute("User_username", username);
                session.setAttribute("Userfullname", fullname);
                session.setAttribute("userId",resultSet.getString("users_id"));
                session.setAttribute("mobile", phone_number);
                session.setAttribute("location", location);
                session.setAttribute("mail", e_mail);
            }


            session.setAttribute("login", "true");

            jsonObject.addProperty("mark",true);
            try {

                String USR_WEL_MSG = "\n" +
                        "\n" +
                        "\n" +
                        "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                        "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
                        "  <title></title>\n" +
                        "\n" +
                        "  <style type=\"text/css\">\n" +
                        "    /* Take care of image borders and formatting, client hacks */\n" +
                        "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
                        "    a img { border: none; }\n" +
                        "    table { border-collapse: collapse !important;}\n" +
                        "    #outlook a { padding:0; }\n" +
                        "    .ReadMsgBody { width: 100%; }\n" +
                        "    .ExternalClass { width: 100%; }\n" +
                        "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
                        "    table td { border-collapse: collapse; }\n" +
                        "    .ExternalClass * { line-height: 115%; }\n" +
                        "    .container-for-gmail-android { min-width: 600px; }\n" +
                        "\n" +
                        "\n" +
                        "    /* General styling */\n" +
                        "    * {\n" +
                        "      font-family: Helvetica, Arial, sans-serif;\n" +
                        "    }\n" +
                        "\n" +
                        "    body {\n" +
                        "      -webkit-font-smoothing: antialiased;\n" +
                        "      -webkit-text-size-adjust: none;\n" +
                        "      width: 100% !important;\n" +
                        "      margin: 0 !important;\n" +
                        "      height: 100%;\n" +
                        "      color: #676767;\n" +
                        "    }\n" +
                        "\n" +
                        "    td {\n" +
                        "      font-family: Helvetica, Arial, sans-serif;\n" +
                        "      font-size: 14px;\n" +
                        "      color: #777777;\n" +
                        "      text-align: center;\n" +
                        "      line-height: 21px;\n" +
                        "    }\n" +
                        "\n" +
                        "    a {\n" +
                        "      color: #676767;\n" +
                        "      text-decoration: none !important;\n" +
                        "    }\n" +
                        "\n" +
                        "    .pull-left {\n" +
                        "      text-align: left;\n" +
                        "    }\n" +
                        "\n" +
                        "    .pull-right {\n" +
                        "      text-align: right;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-lg,\n" +
                        "    .header-md,\n" +
                        "    .header-sm {\n" +
                        "      font-size: 32px;\n" +
                        "      font-weight: 700;\n" +
                        "      line-height: normal;\n" +
                        "      padding: 35px 0 0;\n" +
                        "      color: #4d4d4d;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-md {\n" +
                        "      font-size: 24px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .header-sm {\n" +
                        "      padding: 5px 0;\n" +
                        "      font-size: 18px;\n" +
                        "      line-height: 1.3;\n" +
                        "    }\n" +
                        "\n" +
                        "    .content-padding {\n" +
                        "      padding: 20px 0 30px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mobile-header-padding-right {\n" +
                        "      width: 290px;\n" +
                        "      text-align: right;\n" +
                        "      padding-left: 10px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .mobile-header-padding-left {\n" +
                        "      width: 290px;\n" +
                        "      text-align: left;\n" +
                        "      padding-left: 10px;\n" +
                        "      padding-bottom: 8px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .free-text {\n" +
                        "      width: 100% !important;\n" +
                        "      padding: 10px 60px 0px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .block-rounded {\n" +
                        "      border-radius: 5px;\n" +
                        "      border: 1px solid #e5e5e5;\n" +
                        "      vertical-align: top;\n" +
                        "    }\n" +
                        "\n" +
                        "    .button {\n" +
                        "      padding: 30px 0;\n" +
                        "    }\n" +
                        "\n" +
                        "    .info-block {\n" +
                        "      padding: 0 20px;\n" +
                        "      width: 260px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .block-rounded {\n" +
                        "      width: 260px;\n" +
                        "    }\n" +
                        "\n" +
                        "    .info-img {\n" +
                        "      width: 258px;\n" +
                        "      border-radius: 5px 5px 0 0;\n" +
                        "    }\n" +
                        "\n" +
                        "    .force-width-gmail {\n" +
                        "      min-width:600px;\n" +
                        "      height: 0px !important;\n" +
                        "      line-height: 1px !important;\n" +
                        "      font-size: 1px !important;\n" +
                        "    }\n" +
                        "\n" +
                        "    .button-width {\n" +
                        "      width: 228px;\n" +
                        "    }\n" +
                        "\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"screen\">\n" +
                        "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"screen\">\n" +
                        "    @media screen {\n" +
                        "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
                        "      * {\n" +
                        "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
                        "      }\n" +
                        "    }\n" +
                        "  </style>\n" +
                        "\n" +
                        "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
                        "    /* Mobile styles */\n" +
                        "    @media only screen and (max-width: 480px) {\n" +
                        "\n" +
                        "      table[class*=\"container-for-gmail-android\"] {\n" +
                        "        min-width: 290px !important;\n" +
                        "        width: 100% !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      table[class=\"w320\"] {\n" +
                        "        width: 320px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      img[class=\"force-width-gmail\"] {\n" +
                        "        display: none !important;\n" +
                        "        width: 0 !important;\n" +
                        "        height: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      a[class=\"button-width\"],\n" +
                        "      a[class=\"button-mobile\"] {\n" +
                        "        width: 248px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"mobile-header-padding-left\"] {\n" +
                        "        width: 160px !important;\n" +
                        "        padding-left: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"mobile-header-padding-right\"] {\n" +
                        "        width: 160px !important;\n" +
                        "        padding-right: 0 !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"header-lg\"] {\n" +
                        "        font-size: 24px !important;\n" +
                        "        padding-bottom: 5px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"header-md\"] {\n" +
                        "        font-size: 18px !important;\n" +
                        "        padding-bottom: 5px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"content-padding\"] {\n" +
                        "        padding: 5px 0 30px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "       td[class=\"button\"] {\n" +
                        "        padding: 5px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class*=\"free-text\"] {\n" +
                        "        padding: 10px 18px 30px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"info-block\"] {\n" +
                        "        display: block !important;\n" +
                        "        width: 280px !important;\n" +
                        "        padding-bottom: 40px !important;\n" +
                        "      }\n" +
                        "\n" +
                        "      td[class=\"info-img\"],\n" +
                        "      img[class=\"info-img\"] {\n" +
                        "        width: 278px !important;\n" +
                        "      }\n" +
                        "    }\n" +

                        "\n" +
                        "\n" +
                        " bgcolor=\"#f7f7f7\"\n" +
                        "  </style>\n" +
                        "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
                        "  <tr>\n" +
                        "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
                        "      <center>\n" +
                        "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
                        "          <tr>\n" +
                        "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
                        "            <!--[if gte mso 9]>\n" +
                        "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
                        "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
                        "              <v:textbox inset=\"0,0,0,0\">\n" +
                        "            <![endif]-->\n" +
                        "              <center>\n" +
                        "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
                        "                  <tr>\n" +
                        "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
                        "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
                        "                    </td>\n" +

                        "                  </tr>\n" +
                        "                </table>\n" +
                        "              </center>\n" +
                        "              <!--[if gte mso 9]>\n" +
                        "              </v:textbox>\n" +
                        "            </v:rect>\n" +
                        "            <![endif]-->\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
                        "      <center>\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                        "          <tr>\n" +
                        "            <td class=\"header-lg\">\n" +
                        "              Greetings!\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"free-text\">\n" +
                        "              Thank you for signing up on maidinnigeria.com.ng. We are excited to assist you in your quest for peace of mind in sourcing for domestic staff and healthcare workers.\n" +
                        "<br /> Your login details are as follows: <br /> <br /> Username: "+username+"\n <br />" +
                        "Password: "+password+"\n <br /> <br /> To get the most of our portal, please browse through our services, identify your search and place your request.\n <br />" +
                        "If you require support at any stage, please contact us through the various channels on the Contact Us section of our site. \n <br />" +
                        "Thanks again for choosing us - we are here to serve you!\n" +

                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"free-text\">\n" +
                        "      maidinnigeria.com.ng \n <br />" +
                        "Reliable. Trustworthy. Proven\n <br />" +
                        "Please call: 234 702 614 2888"+
                        "            </td>\n" +
                        "          </tr>\n" +
                        "          <tr>\n" +
                        "            <td class=\"button\">\n" +
                        "              <div><!--[if mso]>\n" +
                        "                <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://www.maidinnigeria.com.ng\" style=\"height:45px;v-text-anchor:middle;width:155px;\" arcsize=\"15%\" strokecolor=\"#ffffff\" fillcolor=\"#4a89dc\">\n" +
                        "                  <w:anchorlock/>\n" +
                        "                  <center style=\"color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;\">My Account</center>\n" +
                        "                </v:roundrect>\n" +
                        "              <![endif]--><a class=\"button-mobile\" href=\"http://\"\n" +
                        "              style=\"background-color:#4a89dc;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;\">My Account</a></div>\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;\">\n" +
                        "      <center>\n" +

                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7; height: 100px;\">\n" +
                        "      <center>\n" +
                        "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
                        "          <tr>\n" +
                        "            <td style=\"padding: 25px 0 25px\">\n" +
                        "              <strong>MaidInNigeria </strong><br />\n" +
                        "               Lagos, Nigeria <br />\n" +
                        "               <br /><br />\n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "      </center>\n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "</table>\n" +
                        "\n" +
                        "\n";
//                MailHelper mailHelper = new MailHelper();
//                mailHelper.sendMessage(e_mail, USR_WEL_MSG, "MaidInNigeria");
//                mailHelper.sendMessage(MailConstants.SUPPORT_MAIL,"Name: " +fullname+",<br /> Location: " + location + " <br /> Email: " + e_mail,);

                SimpleMailSender mailer = new SimpleMailSender();
                mailer.sendMessage(e_mail,"Maid In Nigeria", "Greetings! \n" +
                        "\n" +
                        "\n" +
                        "Thank you for signing up on maidinnigeria.com.ng. We are excited to assist you in your quest for peace of mind in sourcing for domestic staff and health care workers.\n" +
                        "         Your login details are as follows: \n" +
                        "\t\tUsername: "+username+"\n" +
                        "\t\tPassword: "+password+"\n" +
                        "\n" +
                        "To get the most of our portal, please browse through our services, identify your search and place your request.\n" +
                        "If you require support at any stage, please contact us through the various channels on the Contact Us section of our site. \n" +
                        "Thanks again for choosing us - we are here to serve you!\n" +
                        "\n\n" +
                        "NB: You can change your password after a first successful login.\n" +
                        "\n\n" +
                        "\n" +
                        "maidinnigeria.com.ng\n" +
                        "Reliable. Trustworthy. Proven\n" +
                        "Please call: 234 702 614 2888\n" +
                        "\n" +
                        "\n\n" +
                        "MaidInNigeria\n" +
                        "Lagos, Nigeria.");
                mailer.sendMessage(MailConstants.SUPPORT_MAIL,"REGISTRATION ALERT","Name: " +fullname+",\nLocation: " + location + " \n Email: " + e_mail);



            }catch (Exception ex){ex.printStackTrace();}

        } catch (Exception ex) {
            ex.printStackTrace();
            jsonObject.addProperty("mark",false);

        }
        writer.println(jsonObject.toString());

    }
}
