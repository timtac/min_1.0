package Controllers;

import Context.ConnectionDriver;
import Dao.DbConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 5/31/2016.
 */
@WebServlet("/agentProfile")
public class AgentProfile extends HttpServlet {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        String query = "SELECT * FROM agents WHERE agent_Company_Name = '" + name + "'";

        HttpSession session = request.getSession();
        try {
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL, DbConstants.DB_USER, DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                session.setAttribute("title", resultSet.getString("title"));
                session.setAttribute("first", resultSet.getString("agent_fullName"));
                session.setAttribute("gender", resultSet.getString("gender"));
                session.setAttribute("agent", resultSet.getString("agent_Company_Name"));
                session.setAttribute("location", resultSet.getString("agents_location"));
                session.setAttribute("address", resultSet.getString("company_address"));
                session.setAttribute("mobile", resultSet.getString("agent_mobile"));
                session.setAttribute("e_mail",resultSet.getString("agent_email"));
                session.setAttribute("image", resultSet.getString("agent_image"));
            }

            response.sendRedirect("/min/AgentsProfile.jsp");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }
}
