package Controllers;

import Dao.InsuranceDao;
import Models.Insurance;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 2/26/2016.
 */
@WebServlet("/insurance")
public class InsuranceServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");

        PrintWriter writer = response.getWriter();

        JsonObject jsonObject = new JsonObject();
        try{
            Insurance insurance = new Insurance();
            insurance.setName(name);
            insurance.setAddress(address);
            insurance.setEmail(email);
            insurance.setMobile(mobile);

            InsuranceDao insuranceDao = new InsuranceDao(insurance);
            insuranceDao.createInsurance();

            jsonObject.addProperty("mark",true);
        }catch (Exception ex){
            jsonObject.addProperty("mark",false);
        }
        writer.println(jsonObject.toString());
        writer.close();
    }
}
