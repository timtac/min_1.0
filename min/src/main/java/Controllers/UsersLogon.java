package Controllers;

import Context.ConnectionDriver;
import Dao.DbConstants;
import Dao.Users_dao;
import EncrytHelper.EncrytoClass;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 1/9/2016.
 */
@WebServlet("/usersignIn")
public class UsersLogon extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        JsonObject jsonObject = new JsonObject();
        PrintWriter out = response.getWriter();

        try{
          
            EncrytoClass encrytoClass = new EncrytoClass();
            String key = username;
            String encPassword = encrytoClass.encrypt(key,password);

            Models.Users users = new Models.Users();
            Users_dao users_dao = new Users_dao(users);


            if( users_dao.authenticateUsers(username,encPassword)){

                jsonObject.addProperty("mark",true);
                Class.forName(DbConstants.DRIVER);
                connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select * from users where users_username = '"+username+"'");

                while(resultSet.next()){
                    session.setAttribute("userId",resultSet.getString("users_id"));
                    session.setAttribute("Userfullname",resultSet.getString("users_fullname"));
                    session.setAttribute("location",resultSet.getString("users_location"));
                    session.setAttribute("userImage",resultSet.getString("user_image"));
                    session.setAttribute("mail",resultSet.getString("user_email"));
                    session.setAttribute("mobile",resultSet.getString("user_mobile"));
                }

                session.setAttribute("User_username",username);
                session.setAttribute("login","true");

                Calendar date = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String lastSeen = sdf.format(date.getTime());
                System.out.println(lastSeen);

                statement.executeUpdate("update users set lastSeen ='"+lastSeen+"' where users_username = '"+username+"'");

        //        response.sendRedirect("/UserHomePage.jsp");
  //  Indexing Indexing = new Indexing();
//                Indexing.doPost(request,response);
            } else{
                //dError("Wrong Username/Password Combination");
                jsonObject.addProperty("mark",false);
                System.out.println("Error occured here");
            }

        }
        catch (Exception ex){
            ex.printStackTrace();
            jsonObject.addProperty("mark", Boolean.FALSE);
        }
        finally{
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
        out.println(jsonObject.toString());
        out.close();
    }
}
