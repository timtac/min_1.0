package Controllers;

import Dao.Guarantor_dao;
import Dao.Workers_dao;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 10/12/2015.
 */
@WebServlet("/Guarantor")
public class GuarantorServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{

        String worker_id = request.getParameter("worker_id");
        String title = request.getParameter("title");
        String guarantorfirstname = request.getParameter("guaran_firstname");
        String guarantorlastname = request.getParameter("guaran_lastname");
        String worker_name = request.getParameter("worker_name");
        String guarantorhomeaddress = request.getParameter("guaran_homeaddress");
        String guarantoroccupation = request.getParameter("guaran_occupation");
        String guarantorworkaddress = request.getParameter("guaran_workaddress");
        String guarantorrelationship = request.getParameter("guaran_relationship");
        String guarantorphonenumber = request.getParameter("guaran_mobile");
        String guarantor_mail = request.getParameter("guaran_mail");

        Integer workers_id = new Integer(worker_id);

        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        JsonObject jsonObject = new JsonObject();
        try{
            Models.Workers workers = new Models.Workers();
            workers.setWorkers_id(workers_id);
            Workers_dao workers_dao = new Workers_dao(workers);
            workers_dao.refreshWorker();

            Models.Guarantor guarantor = new Models.Guarantor();

            guarantor.setGuarantor_title(title);
            guarantor.setWorkers_id(workers);
            guarantor.setGuarantor_firstname(guarantorfirstname);
            guarantor.setGuarantor_lastname(guarantorlastname);
            guarantor.setGuarantor_home_address(guarantorhomeaddress);
            guarantor.setGuarantor_occupation(guarantoroccupation);
            guarantor.setGuarantor_work_address(guarantorworkaddress);
            guarantor.setRelationship(guarantorrelationship);
            guarantor.setGuarantor_phone_number(guarantorphonenumber);
            guarantor.setE_mail(guarantor_mail);

            Guarantor_dao guarantorDao = new Guarantor_dao(guarantor);
            guarantorDao.createGuarantor();



            jsonObject.addProperty("success",true);
            session.setAttribute("title",title);
            session.setAttribute("first",guarantorfirstname);
            session.setAttribute("last",guarantorlastname);
            session.setAttribute("worker",worker_name);
        }
        catch(Exception ex){
            jsonObject.addProperty("success",false);
        }

        writer.println(jsonObject.toString());
    }
}
