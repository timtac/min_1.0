package Controllers;

import Context.ConnectionDriver;
import Dao.Agents_dao;
import Dao.DbConstants;
import EncrytHelper.EncrytoClass;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 1/9/2016.
 */
@WebServlet("/agentSignIn")
public class AgentLogon extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        PrintWriter writer = response.getWriter();

        JsonObject object = new JsonObject();


            try {

                EncrytoClass encryptClass = new EncrytoClass();
                String key = username;
                String encPassword = encryptClass.encrypt(key, password);

                Models.Agents agents = new Models.Agents();
                Agents_dao agentsDao = new Agents_dao(agents);


                boolean result = agentsDao.authenticateAgents(username, encPassword);


                if (result == true) {

                    Class.forName(DbConstants.DRIVER);
                    connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
                    statement = connection.createStatement();

                    resultSet = statement.executeQuery("select * from agents where agent_username = '"+username+"'");

                    while (resultSet.next()){
                        session.setAttribute("company",resultSet.getString("agent_Company_Name"));
                        session.setAttribute("location",resultSet.getString("agents_location"));
                        session.setAttribute("mail",resultSet.getString("agent_email"));
                        session.setAttribute("agentImage",resultSet.getString("agent_image"));

                    }
                    session.setAttribute("Agent_username", username);
                    session.setAttribute("login", "true");


                    object.addProperty("mark",true);

/*                    response.sendRedirect("/min/index.jsp");*/
                    Calendar date = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                    String lastSeen = sdf.format(date.getTime());
                    System.out.println(lastSeen);

                    statement.executeUpdate("update agents SET lastSeen = '"+lastSeen+"' WHERE agent_username = '"+username+"'");
                } else {
                    object.addProperty("mark",false);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                object.addProperty("mark", Boolean.FALSE);
            }

            finally{
                ConnectionDriver.close(resultSet);
                ConnectionDriver.close(statement);
                ConnectionDriver.close(connection);
            }

        writer.println(object.toString());
    }
}
