package Controllers;

import Dao.AdminDao;
import EncrytHelper.EncrytoClass;
import Models.Admin;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by user on 3/7/2016.
 */
@WebServlet("/adminreg")
public class AdminServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        try{
            EncrytoClass encrypt = new EncrytoClass();
            String encPassword = encrypt.encrypt(username, password);
            
            Admin admin = new Admin();
            admin.setUsername(username);
            admin.setPassword(encPassword);
            
            AdminDao dao = new AdminDao(admin);
            dao.createAdmin();


            session.setAttribute("login","true");
            session.setAttribute("username",username);


        }catch (Exception ex){
            ex.printStackTrace();
        }
        response.sendRedirect("/min/adminPage.jsp");
    }
}
