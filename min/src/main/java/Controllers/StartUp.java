package Controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by user on 6/13/2016.
 */
@WebServlet("/startup")
public class StartUp extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie cookie = null;
        Cookie[] cookies = null;

        cookies = request.getCookies();

        HttpSession session = request.getSession();

        if(cookies != null){
            for(int i=0; i < cookies.length; i++){
                cookie = cookies[i];
                if(cookie.getName().equals("User_username")){
                    session.setAttribute("User_username",cookie.getValue());
                }else if(cookie.getName().equals("Agent_username")){
                    session.setAttribute("Agent_username",cookie.getValue());
                }
            }
        }
        response.sendRedirect("/min/index.jsp");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
