package Controllers;

import Dao.Workers_dao;
import Models.Workers;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 10/7/2015.
 */
@WebServlet("/pullWorker")
public class AllWorkers extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter writer  = response.getWriter();
        try {
            Workers_dao workersDao = new Workers_dao();
            List<Workers> list = workersDao.getAllWorkers();
            JsonArray jsonArray = new JsonArray();
            for (Workers workers : list) {
                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("id", workers.getWorkers_id());
                jsonObject.addProperty("first",workers.getWorkers_fullname());
                jsonObject.addProperty("work", workers.getWorker_occupation());
                jsonObject.addProperty("location", workers.getWorker_location());
                jsonObject.addProperty("image", workers.getWorker_image());
                jsonObject.addProperty("agent", workers.getAgent_Company_Name().getAgent_Company_Name());
                jsonObject.addProperty("medical",workers.getWorker_medical_history());
                jsonObject.addProperty("age",workers.getWorker_age());
                jsonObject.addProperty("sex",workers.getWorker_sex());
                jsonObject.addProperty("address",workers.getWorker_address());
                jsonObject.addProperty("phoneNumber",workers.getWorker_mobile());
                jsonObject.addProperty("education",workers.getEducational_level());
                jsonObject.addProperty("agencyFee",workers.getAgency_fee());
                jsonObject.addProperty("expectedSal",workers.getExpected_salary());

                jsonArray.add(jsonObject);
            }

            writer.println(jsonArray.toString());

        }
        catch(Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        doGet(request,response);
    }
}
