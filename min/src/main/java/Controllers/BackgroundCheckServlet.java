package Controllers;

import Dao.BackgroundCheckDao;
import Dao.Workers_dao;
import Models.BackgroundCheck;
import Models.Workers;
import com.google.gson.JsonObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 3/3/2016.
 */
@WebServlet("/backgroundCheck")
public class BackgroundCheckServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException,IOException{
        doPost(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        String workerId = request.getParameter("workerId");
        String birth = request.getParameter("birth");
        String town = request.getParameter("town");
        String localGov = request.getParameter("localGov");
        String state = request.getParameter("state");
        String nationality = request.getParameter("nationality");
        String idCard = request.getParameter("idcard");
        String id_number = request.getParameter("id_number");
        String mother_name = request.getParameter("mother_name");

        Integer worker_id = new Integer(workerId);
        PrintWriter writer = response.getWriter();
        JsonObject jsonObject = new JsonObject();
        try{
            Workers workers = new Workers();
            workers.setWorkers_id(worker_id);
            Workers_dao workers_dao = new Workers_dao(workers);
            workers_dao.refreshWorker();

            BackgroundCheck backgroundCheck = new BackgroundCheck();
            backgroundCheck.setWorkers_id(workers);
            backgroundCheck.setBirth(birth);
            backgroundCheck.setTown(town);
            backgroundCheck.setLocalGov(localGov);
            backgroundCheck.setState(state);
            backgroundCheck.setNationality(nationality);
            backgroundCheck.setIdCard(idCard);
            backgroundCheck.setId_number(id_number);
            backgroundCheck.setMother_name(mother_name);

            BackgroundCheckDao dao = new BackgroundCheckDao(backgroundCheck);
            dao.createBackgroundCheck();

            jsonObject.addProperty("status",true);
        }catch(Exception ex){
            jsonObject.addProperty("status",false);
        }
        writer.println(jsonObject.toString());
    }
}
