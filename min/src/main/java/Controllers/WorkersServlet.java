package Controllers;

import Dao.Agents_dao;
import Dao.Workers_dao;
import com.google.gson.JsonObject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;

/**
 * Created by user on 9/29/2015.
 */
@WebServlet("/adminaddnewmaids")
public class WorkersServlet extends HttpServlet {


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();

        String agentCompany = request.getParameter("agentCompname");
        String firstname = request.getParameter("firstname");
        String fee_type = request.getParameter("fee_type");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String relationship = request.getParameter("relationship");
        String occupation = request.getParameter("occupation");
        String location = request.getParameter("location");
        String address = request.getParameter("address");
        String physical_challenge = request.getParameter("physical_challenge");
        String education_level = request.getParameter("educationalLevel");
        String phone_number = request.getParameter("phoneno");
        String agency_fee = request.getParameter("agencyFee");
        String total = request.getParameter("total");
        String expected_salary = request.getParameter("expectedSalary");
        String religion = request.getParameter("religion");
        String language = request.getParameter("language");
        String preference = request.getParameter("preference");

        JsonObject object = new JsonObject();

        try{

            Calendar date = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String registerDate = sdf.format(date.getTime());
            System.out.println(registerDate);

            Models.Agents agents = new Models.Agents();
            agents.setAgent_Company_Name(agentCompany);
            Agents_dao dao = new Agents_dao(agents);
            dao.refresh();

            Models.Workers workers = new Models.Workers();
            workers.setAgent_Company_Name(agents);
            workers.setAgency_fee(agency_fee);
            workers.setAgency_fee_type(fee_type);
            workers.setExpected_salary(expected_salary);
            workers.setWorkers_fullname(firstname);
            workers.setWorker_age(age);
            workers.setWorker_sex(sex);
            workers.setWorker_relationship(relationship);
            workers.setWorker_occupation(occupation);
            workers.setWorker_location(location);
            workers.setWorker_address(address);
            workers.setLanguage(language);
            workers.setWorker_medical_history(physical_challenge);
            workers.setEducational_level(education_level);
            workers.setWorker_mobile(phone_number);
            workers.setPolice_report(religion);
            workers.setPreference(preference);
            workers.setTotal(total);
            workers.setRegisteredDate(registerDate);
//            workers.setWorker_image(fileName);
            Workers_dao workersdao = new Workers_dao(workers);

            workersdao.createWorker();

            object.addProperty("mark",true);
            //dispatcher.forward(request,response);

        }
        catch(Exception ex){
            object.addProperty("mark",false);
            ex.printStackTrace();
        }
        writer.println(object.toString());
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request,response);
    }
}
