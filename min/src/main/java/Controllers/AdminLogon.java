package Controllers;

import Dao.AdminDao;
import EncrytHelper.EncrytoClass;
import Models.Admin;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 2/10/2016.
 */
@WebServlet("/adminLogon")
public class AdminLogon extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        PrintWriter writer = response.getWriter();
        JsonObject object = new JsonObject();
        HttpSession session = request.getSession();

        try{
            EncrytoClass encrytoClass = new EncrytoClass();
            String encPassword = encrytoClass.encrypt(username,password);
            Admin admin = new Admin();
            AdminDao dao = new AdminDao(admin);
            boolean authenticate = dao.authenticateAdmin(username,encPassword);
            if(authenticate){

                object.addProperty("success",true);
                session.setAttribute("username",username);
                session.setAttribute("login","true");
            }else{
                object.addProperty("success",false);
            }
        }catch(Exception ex){

        }
        writer.println(object.toString());
    }
}
