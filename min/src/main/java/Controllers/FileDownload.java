
package Controllers;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by user on 1/10/2016.
 */

@WebServlet("/adminimagedownload")
public class FileDownload extends HttpServlet {

    private static final String SAVE_DIR = "images";

    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response ) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        try {
            File file = new File(getServletContext().getRealPath("")+ File.separator + SAVE_DIR+File.separator+ fileName);
            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server." + fileName);
            }
            //System.out.println("File location on server::" + file.getAbsolutePath());
            ServletContext ctx = getServletContext();
            InputStream fis = new FileInputStream(file);
            String mimeType = ctx.getMimeType(file.getAbsolutePath());
            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + "\"");
            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read = 0;
            while ((read = fis.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            fis.close();
            // System.out.println("File downloaded at client successfully");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        doGet(request,response);
    }

}