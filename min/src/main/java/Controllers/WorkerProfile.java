package Controllers;

import Context.ConnectionDriver;
import Dao.DbConstants;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 3/15/2016.
 */
@WebServlet("/workerProfile")
public class WorkerProfile extends HttpServlet {

    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        String query = "select * from workers w left join guarantor g on w.workers_id = g.workers_id left join backgroundCheck b on w.workers_id = b.workers_id where w.workers_id = '"+name+"'";

        HttpSession session = request.getSession();
        try{
            Class.forName(DbConstants.DRIVER);
            connection = ConnectionDriver.getConnection(DbConstants.DB_URL,DbConstants.DB_USER,DbConstants.DB_PWD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()){

                session.setAttribute("worker_id",resultSet.getString("workers_id"));
                session.setAttribute("first",resultSet.getString("workers_fullname"));
                session.setAttribute("age",resultSet.getString("worker_age"));
                session.setAttribute("sex",resultSet.getString("worker_sex"));
                session.setAttribute("agent",resultSet.getString("agent_Company_Name"));
                session.setAttribute("medical",resultSet.getString("worker_medical_history"));
                session.setAttribute("occupation",resultSet.getString("worker_occupation"));
                session.setAttribute("address",resultSet.getString("worker_address"));
                session.setAttribute("total",resultSet.getString("total"));
                session.setAttribute("agency_fee",resultSet.getString("agency_fee"));
                session.setAttribute("salary",resultSet.getString("expected_salary"));
                session.setAttribute("religion",resultSet.getString("police_report"));
                session.setAttribute("relationship",resultSet.getString("worker_relationship"));
                session.setAttribute("fee_type",resultSet.getString("agency_fee_type"));
                session.setAttribute("guanId",resultSet.getString("guarantor_id"));
                session.setAttribute("backId",resultSet.getString("id"));
                session.setAttribute("preference",resultSet.getString("preference"));
            }

            response.sendRedirect("/min/WorkersProfile.jsp");
        }catch (Exception ex){
            ex.printStackTrace();
            response.sendRedirect("/min/Error404.jsp");
        }finally {
            ConnectionDriver.close(resultSet);
            ConnectionDriver.close(statement);
            ConnectionDriver.close(connection);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
