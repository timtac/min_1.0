package Mailer;

import Mailer.MessageLogics.MailConstants;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 * Created by user on 1/17/2017.
 */
public class SimpleMailSender {

    public static void sendMessage(String recipient,String subject,String message) throws EmailException {
        Email email = new SimpleEmail();
        email.setHostName(MailConstants.SMTP_HOST);
        email.setSmtpPort(25);
        email.setAuthenticator(new DefaultAuthenticator(MailConstants.SMTP_USER, MailConstants.SMTP_PSD));
        email.setSSLOnConnect(true);
        email.setFrom(MailConstants.SMTP_MAIL);
        email.setSubject(subject);
        email.setContent(message, "text/html; charset=utf-8");
        email.setMsg(message);
        email.addTo(recipient);
        email.send();
    }
}
