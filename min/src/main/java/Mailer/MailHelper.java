package Mailer;

import Mailer.MessageLogics.MailConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;


/**
 * Created by user on 9/28/2015.
 */
public class MailHelper {
    public void sendMessage(String recipient,String message,String subject) {

        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("api",
                MailConstants.API_KEY));
        WebResource webResource =
                client.resource(MailConstants.WEB_RESOURCE);
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add("from", MailConstants.FROM_MAIL);
        formData.add("to", recipient);
        formData.add("subject", subject);
        formData.add("html", message);
        ClientResponse mres= webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
                post(ClientResponse.class, formData); //

        if(mres.getStatus()==200){
            // return ok("Message Sent");
            System.out.print("Message Sent ");
        }
    }
}
