package Mailer.MessageLogics;

/**
 * Created by user on 1/20/2016.
 */
public class MessageLogics {
    public static final String AGT_WEL_MSG ="\n" +
            "\n" +
            "\n" +
            "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
            "  <title></title>\n" +
            "\n" +
            "  <style type=\"text/css\">\n" +
            "    /* Take care of image borders and formatting, client hacks */\n" +
            "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
            "    a img { border: none; }\n" +
            "    table { border-collapse: collapse !important;}\n" +
            "    #outlook a { padding:0; }\n" +
            "    .ReadMsgBody { width: 100%; }\n" +
            "    .ExternalClass { width: 100%; }\n" +
            "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
            "    table td { border-collapse: collapse; }\n" +
            "    .ExternalClass * { line-height: 115%; }\n" +
            "    .container-for-gmail-android { min-width: 600px; }\n" +
            "\n" +
            "\n" +
            "    /* General styling */\n" +
            "    * {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "    }\n" +
            "\n" +
            "    body {\n" +
            "      -webkit-font-smoothing: antialiased;\n" +
            "      -webkit-text-size-adjust: none;\n" +
            "      width: 100% !important;\n" +
            "      margin: 0 !important;\n" +
            "      height: 100%;\n" +
            "      color: #676767;\n" +
            "    }\n" +
            "\n" +
            "    td {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "      font-size: 14px;\n" +
            "      color: #777777;\n" +
            "      text-align: center;\n" +
            "      line-height: 21px;\n" +
            "    }\n" +
            "\n" +
            "    a {\n" +
            "      color: #676767;\n" +
            "      text-decoration: none !important;\n" +
            "    }\n" +
            "\n" +
            "    .pull-left {\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .pull-right {\n" +
            "      text-align: right;\n" +
            "    }\n" +
            "\n" +
            "    .header-lg,\n" +
            "    .header-md,\n" +
            "    .header-sm {\n" +
            "      font-size: 32px;\n" +
            "      font-weight: 700;\n" +
            "      line-height: normal;\n" +
            "      padding: 35px 0 0;\n" +
            "      color: #4d4d4d;\n" +
            "    }\n" +
            "\n" +
            "    .header-md {\n" +
            "      font-size: 24px;\n" +
            "    }\n" +
            "\n" +
            "    .header-sm {\n" +
            "      padding: 5px 0;\n" +
            "      font-size: 18px;\n" +
            "      line-height: 1.3;\n" +
            "    }\n" +
            "\n" +
            "    .content-padding {\n" +
            "      padding: 20px 0 30px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-right {\n" +
            "      width: 290px;\n" +
            "      text-align: right;\n" +
            "      padding-left: 10px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-left {\n" +
            "      width: 290px;\n" +
            "      text-align: left;\n" +
            "      padding-left: 10px;\n" +
            "      padding-bottom: 8px;\n" +
            "    }\n" +
            "\n" +
            "    .free-text {\n" +
            "      width: 100% !important;\n" +
            "      padding: 10px 60px 0px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #e5e5e5;\n" +
            "      vertical-align: top;\n" +
            "    }\n" +
            "\n" +
            "    .button {\n" +
            "      padding: 30px 0;\n" +
            "    }\n" +
            "\n" +
            "    .info-block {\n" +
            "      padding: 0 20px;\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .info-img {\n" +
            "      width: 258px;\n" +
            "      border-radius: 5px 5px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-gmail {\n" +
            "      min-width:600px;\n" +
            "      height: 0px !important;\n" +
            "      line-height: 1px !important;\n" +
            "      font-size: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .button-width {\n" +
            "      width: 228px;\n" +
            "    }\n" +
            "\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @media screen {\n" +
            "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
            "      * {\n" +
            "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
            "    /* Mobile styles */\n" +
            "    @media only screen and (max-width: 480px) {\n" +
            "\n" +
            "      table[class*=\"container-for-gmail-android\"] {\n" +
            "        min-width: 290px !important;\n" +
            "        width: 100% !important;\n" +
            "      }\n" +
            "\n" +
            "      table[class=\"w320\"] {\n" +
            "        width: 320px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-gmail\"] {\n" +
            "        display: none !important;\n" +
            "        width: 0 !important;\n" +
            "        height: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      a[class=\"button-width\"],\n" +
            "      a[class=\"button-mobile\"] {\n" +
            "        width: 248px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-left\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-left: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-right\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-right: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-lg\"] {\n" +
            "        font-size: 24px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-md\"] {\n" +
            "        font-size: 18px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"content-padding\"] {\n" +
            "        padding: 5px 0 30px !important;\n" +
            "      }\n" +
            "\n" +
            "       td[class=\"button\"] {\n" +
            "        padding: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"free-text\"] {\n" +
            "        padding: 10px 18px 30px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-block\"] {\n" +
            "        display: block !important;\n" +
            "        width: 280px !important;\n" +
            "        padding-bottom: 40px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-img\"],\n" +
            "      img[class=\"info-img\"] {\n" +
            "        width: 278px !important;\n" +
            "      }\n" +
            "    }\n" +

            "\n" +
            "\n" +
            " bgcolor=\"#f7f7f7\"\n" +
            "  </style>\n" +
            "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
            "  <tr>\n" +
            "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
            "      <center>\n" +
            "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
            "          <tr>\n" +
            "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
            "            <!--[if gte mso 9]>\n" +
            "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
            "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
            "              <v:textbox inset=\"0,0,0,0\">\n" +
            "            <![endif]-->\n" +
            "              <center>\n" +
            "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
            "                  <tr>\n" +
            "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
            "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
            "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
            "                  </tr>\n" +
            "                </table>\n" +
            "              </center>\n" +
            "              <!--[if gte mso 9]>\n" +
            "              </v:textbox>\n" +
            "            </v:rect>\n" +
            "            <![endif]-->\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td class=\"header-lg\">\n" +
            "              Welcome to MaidInNigeria.com.ng!\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"free-text\">\n" +
            "              Thank you for signing up with MaidInNigeria! We hope you enjoy your time with us.\n" +
            "<br /> Login to add wards to your profile to start getting hire request fast from serious employers."+
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"button\">\n" +
            "              <div><!--[if mso]>\n" +
            "                <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://www.maidinnigeria.com.ng\" style=\"height:45px;v-text-anchor:middle;width:155px;\" arcsize=\"15%\" strokecolor=\"#ffffff\" fillcolor=\"#4a89dc\">\n" +
            "                  <w:anchorlock/>\n" +
            "                  <center style=\"color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;\">My Account</center>\n" +
            "                </v:roundrect>\n" +
            "              <![endif]--><a class=\"button-mobile\" href=\"http://\"\n" +
            "              style=\"background-color:#4a89dc;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;\">My Account</a></div>\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;\">\n" +
            "      <center>\n" +

            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7; height: 100px;\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td style=\"padding: 25px 0 25px\">\n" +
            "              <strong>Maid In Nigeria </strong><br />\n" +
            "               Lagos, Nigeria <br />\n" +
            "               <br /><br />\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "</table>\n" +
            "\n" +
            "\n";


    public static final String USR_WEL_MSG = "\n" +
            "\n" +
            "\n" +
            "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
            "  <title></title>\n" +
            "\n" +
            "  <style type=\"text/css\">\n" +
            "    /* Take care of image borders and formatting, client hacks */\n" +
            "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
            "    a img { border: none; }\n" +
            "    table { border-collapse: collapse !important;}\n" +
            "    #outlook a { padding:0; }\n" +
            "    .ReadMsgBody { width: 100%; }\n" +
            "    .ExternalClass { width: 100%; }\n" +
            "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
            "    table td { border-collapse: collapse; }\n" +
            "    .ExternalClass * { line-height: 115%; }\n" +
            "    .container-for-gmail-android { min-width: 600px; }\n" +
            "\n" +
            "\n" +
            "    /* General styling */\n" +
            "    * {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "    }\n" +
            "\n" +
            "    body {\n" +
            "      -webkit-font-smoothing: antialiased;\n" +
            "      -webkit-text-size-adjust: none;\n" +
            "      width: 100% !important;\n" +
            "      margin: 0 !important;\n" +
            "      height: 100%;\n" +
            "      color: #676767;\n" +
            "    }\n" +
            "\n" +
            "    td {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "      font-size: 14px;\n" +
            "      color: #777777;\n" +
            "      text-align: center;\n" +
            "      line-height: 21px;\n" +
            "    }\n" +
            "\n" +
            "    a {\n" +
            "      color: #676767;\n" +
            "      text-decoration: none !important;\n" +
            "    }\n" +
            "\n" +
            "    .pull-left {\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .pull-right {\n" +
            "      text-align: right;\n" +
            "    }\n" +
            "\n" +
            "    .header-lg,\n" +
            "    .header-md,\n" +
            "    .header-sm {\n" +
            "      font-size: 32px;\n" +
            "      font-weight: 700;\n" +
            "      line-height: normal;\n" +
            "      padding: 35px 0 0;\n" +
            "      color: #4d4d4d;\n" +
            "    }\n" +
            "\n" +
            "    .header-md {\n" +
            "      font-size: 24px;\n" +
            "    }\n" +
            "\n" +
            "    .header-sm {\n" +
            "      padding: 5px 0;\n" +
            "      font-size: 18px;\n" +
            "      line-height: 1.3;\n" +
            "    }\n" +
            "\n" +
            "    .content-padding {\n" +
            "      padding: 20px 0 30px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-right {\n" +
            "      width: 290px;\n" +
            "      text-align: right;\n" +
            "      padding-left: 10px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-left {\n" +
            "      width: 290px;\n" +
            "      text-align: left;\n" +
            "      padding-left: 10px;\n" +
            "      padding-bottom: 8px;\n" +
            "    }\n" +
            "\n" +
            "    .free-text {\n" +
            "      width: 100% !important;\n" +
            "      padding: 10px 60px 0px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #e5e5e5;\n" +
            "      vertical-align: top;\n" +
            "    }\n" +
            "\n" +
            "    .button {\n" +
            "      padding: 30px 0;\n" +
            "    }\n" +
            "\n" +
            "    .info-block {\n" +
            "      padding: 0 20px;\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .info-img {\n" +
            "      width: 258px;\n" +
            "      border-radius: 5px 5px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-gmail {\n" +
            "      min-width:600px;\n" +
            "      height: 0px !important;\n" +
            "      line-height: 1px !important;\n" +
            "      font-size: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .button-width {\n" +
            "      width: 228px;\n" +
            "    }\n" +
            "\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @media screen {\n" +
            "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
            "      * {\n" +
            "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
            "    /* Mobile styles */\n" +
            "    @media only screen and (max-width: 480px) {\n" +
            "\n" +
            "      table[class*=\"container-for-gmail-android\"] {\n" +
            "        min-width: 290px !important;\n" +
            "        width: 100% !important;\n" +
            "      }\n" +
            "\n" +
            "      table[class=\"w320\"] {\n" +
            "        width: 320px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-gmail\"] {\n" +
            "        display: none !important;\n" +
            "        width: 0 !important;\n" +
            "        height: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      a[class=\"button-width\"],\n" +
            "      a[class=\"button-mobile\"] {\n" +
            "        width: 248px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-left\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-left: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-right\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-right: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-lg\"] {\n" +
            "        font-size: 24px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-md\"] {\n" +
            "        font-size: 18px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"content-padding\"] {\n" +
            "        padding: 5px 0 30px !important;\n" +
            "      }\n" +
            "\n" +
            "       td[class=\"button\"] {\n" +
            "        padding: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"free-text\"] {\n" +
            "        padding: 10px 18px 30px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-block\"] {\n" +
            "        display: block !important;\n" +
            "        width: 280px !important;\n" +
            "        padding-bottom: 40px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-img\"],\n" +
            "      img[class=\"info-img\"] {\n" +
            "        width: 278px !important;\n" +
            "      }\n" +
            "    }\n" +

            "\n" +
            "\n" +
            " bgcolor=\"#f7f7f7\"\n" +
            "  </style>\n" +
            "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
            "  <tr>\n" +
            "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
            "      <center>\n" +
            "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
            "          <tr>\n" +
            "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
            "            <!--[if gte mso 9]>\n" +
            "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
            "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
            "              <v:textbox inset=\"0,0,0,0\">\n" +
            "            <![endif]-->\n" +
            "              <center>\n" +
            "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
            "                  <tr>\n" +
            "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
            "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
            "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
            "                  </tr>\n" +
            "                </table>\n" +
            "              </center>\n" +
            "              <!--[if gte mso 9]>\n" +
            "              </v:textbox>\n" +
            "            </v:rect>\n" +
            "            <![endif]-->\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td class=\"header-lg\">\n" +
            "              Greetings!\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"free-text\">\n" +
            "              Thank you for signing up on maidinnigeria.com.ng. We are excited to assist you in your quest for peace of mind in sourcing for domestic staff and healthcare workers.\n" +
            "Your login details are as follows:\n\n Username: <username>\n" +
            "Password: <password>\n To get the most of our portal, please browse through our services, identify your search and place your request.\n" +
            "If you require support at any stage, please contact us through the various channels on the Contact Us section of our site. \n" +
            "Thanks again for choosing us - we are here to serve you!\n" +

            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"free-text\">\n" +
            "      MAIDINNIGERIA.COM.NG \n" +
            "Reliable. Trustworthy. Proven\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"button\">\n" +
            "              <div><!--[if mso]>\n" +
            "                <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://www.maidinnigeria.com.ng\" style=\"height:45px;v-text-anchor:middle;width:155px;\" arcsize=\"15%\" strokecolor=\"#ffffff\" fillcolor=\"#4a89dc\">\n" +
            "                  <w:anchorlock/>\n" +
            "                  <center style=\"color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;\">My Account</center>\n" +
            "                </v:roundrect>\n" +
            "              <![endif]--><a class=\"button-mobile\" href=\"http://\"\n" +
            "              style=\"background-color:#4a89dc;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;\">My Account</a></div>\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;\">\n" +
            "      <center>\n" +

            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7; height: 100px;\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td style=\"padding: 25px 0 25px\">\n" +
            "              <strong>MaidInNigeria </strong><br />\n" +
            "               Lagos, Nigeria <br />\n" +
            "               <br /><br />\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "</table>\n" +
            "\n" +
            "\n";


    public static final String USR_REQ_MSG = "<style type=\"text/css\">\n" +
            "    /* Take care of image borders and formatting, client hacks */\n" +
            "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
            "    a img { border: none; }\n" +
            "    table { border-collapse: collapse !important;}\n" +
            "    #outlook a { padding:0; }\n" +
            "    .ReadMsgBody { width: 100%; }\n" +
            "    .ExternalClass { width: 100%; }\n" +
            "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
            "    table td { border-collapse: collapse; }\n" +
            "    .ExternalClass * { line-height: 115%; }\n" +
            "    .container-for-gmail-android { min-width: 600px; }\n" +
            "\n" +
            "\n" +
            "    /* General styling */\n" +
            "    * {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "    }\n" +
            "\n" +
            "    body {\n" +
            "      -webkit-font-smoothing: antialiased;\n" +
            "      -webkit-text-size-adjust: none;\n" +
            "      width: 100% !important;\n" +
            "      margin: 0 !important;\n" +
            "      height: 100%;\n" +
            "      color: #676767;\n" +
            "    }\n" +
            "\n" +
            "    td {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "      font-size: 14px;\n" +
            "      color: #777777;\n" +
            "      text-align: center;\n" +
            "      line-height: 21px;\n" +
            "    }\n" +
            "\n" +
            "    a {\n" +
            "      color: #676767;\n" +
            "      text-decoration: none !important;\n" +
            "    }\n" +
            "\n" +
            "    .pull-left {\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .pull-right {\n" +
            "      text-align: right;\n" +
            "    }\n" +
            "\n" +
            "    .header-lg,\n" +
            "    .header-md,\n" +
            "    .header-sm {\n" +
            "      font-size: 32px;\n" +
            "      font-weight: 700;\n" +
            "      line-height: normal;\n" +
            "      padding: 35px 0 0;\n" +
            "      color: #4d4d4d;\n" +
            "    }\n" +
            "\n" +
            "    .header-md {\n" +
            "      font-size: 24px;\n" +
            "    }\n" +
            "\n" +
            "    .header-sm {\n" +
            "      padding: 5px 0;\n" +
            "      font-size: 18px;\n" +
            "      line-height: 1.3;\n" +
            "      padding-bottom: 30px;\n" +
            "      font-weight: 400;\n" +
            "    }\n" +
            "\n" +
            "    .content-padding {\n" +
            "      padding: 20px 0 30px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-right {\n" +
            "      width: 290px;\n" +
            "      text-align: right;\n" +
            "      padding-left: 10px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-left {\n" +
            "      width: 290px;\n" +
            "      text-align: left;\n" +
            "      padding-left: 10px;\n" +
            "      padding-bottom: 8px;\n" +
            "    }\n" +
            "\n" +
            "    .free-text {\n" +
            "      width: 100% !important;\n" +
            "      padding: 10px 60px 0px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #e5e5e5;\n" +
            "      vertical-align: top;\n" +
            "    }\n" +
            "\n" +
            "    .button {\n" +
            "      padding: 55px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .info-block {\n" +
            "      padding: 0 20px;\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .mini-block-container {\n" +
            "      padding: 30px 50px;\n" +
            "      width: 500px;\n" +
            "    }\n" +
            "\n" +
            "    .mini-block {\n" +
            "      background-color: #ffffff;\n" +
            "      width: 498px;\n" +
            "      border: 1px solid #e1e1e1;\n" +
            "      border-radius: 5px;\n" +
            "      padding: 25px 70px 60px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .info-img {\n" +
            "      width: 258px;\n" +
            "      border-radius: 5px 5px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-img {\n" +
            "      width: 480px;\n" +
            "      height: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-full {\n" +
            "      width: 600px;\n" +
            "      height: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .user-img img {\n" +
            "      width: 82px;\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #cccccc;\n" +
            "    }\n" +
            "\n" +
            "    .user-img {\n" +
            "      width: 92px;\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .user-msg {\n" +
            "      width: 236px;\n" +
            "      font-size: 14px;\n" +
            "      text-align: left;\n" +
            "      font-style: italic;\n" +
            "    }\n" +
            "\n" +
            "    .code-block {\n" +
            "      padding: 10px 0;\n" +
            "      border: 1px solid #cccccc;\n" +
            "      width: 20px;\n" +
            "      color: #4d4d4d;\n" +
            "      font-weight: bold;\n" +
            "      font-size: 18px;\n" +
            "    }\n" +
            "\n" +
            "    .center-txt {\n" +
            "      padding-left: 30px;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-gmail {\n" +
            "      min-width:600px;\n" +
            "      height: 0px !important;\n" +
            "      line-height: 1px !important;\n" +
            "      font-size: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @media screen {\n" +
            "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
            "      * {\n" +
            "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
            "    /* Mobile styles */\n" +
            "    @media only screen and (max-width: 480px) {\n" +
            "\n" +
            "      table[class*=\"container-for-gmail-android\"] {\n" +
            "        min-width: 290px !important;\n" +
            "        width: 100% !important;\n" +
            "      }\n" +
            "\n" +
            "      table[class=\"w320\"] {\n" +
            "        width: 320px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-gmail\"] {\n" +
            "        display: none !important;\n" +
            "        width: 0 !important;\n" +
            "        height: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-left\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-left: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-right\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-right: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-lg\"] {\n" +
            "        font-size: 24px !important;\n" +
            "        padding: 10px 50px 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-md\"] {\n" +
            "        font-size: 18px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"content-padding\"] {\n" +
            "        padding: 5px 0 30px !important;\n" +
            "      }\n" +
            "\n" +
            "       td[class=\"button\"] {\n" +
            "        padding: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"free-text\"] {\n" +
            "        padding: 10px 18px 30px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-img\"],\n" +
            "      img[class=\"force-width-full\"] {\n" +
            "        display: none !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-block\"] {\n" +
            "        display: block !important;\n" +
            "        width: 280px !important;\n" +
            "        padding-bottom: 40px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-img\"],\n" +
            "      img[class=\"info-img\"] {\n" +
            "        width: 278px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"mini-block-container\"] {\n" +
            "        padding: 8px 20px !important;\n" +
            "        width: 280px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"mini-block\"] {\n" +
            "        padding: 20px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"user-img\"] {\n" +
            "        display: block !important;\n" +
            "        text-align: center !important;\n" +
            "        width: 100% !important;\n" +
            "        padding-bottom: 10px;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"user-msg\"] {\n" +
            "        display: block !important;\n" +
            "        padding-bottom: 20px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"center-txt\"] {\n" +
            "        padding-left: 3px !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "</head>\n" +
            "\n" +
            "<body bgcolor=\"#f7f7f7\">\n" +
            "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
            "  <tr>\n" +
            "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
            "      <center>\n" +
            "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
            "          <tr>\n" +
            "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
            "            <!--[if gte mso 9]>\n" +
            "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
            "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
            "              <v:textbox inset=\"0,0,0,0\">\n" +
            "            <![endif]-->\n" +
            "              <center>\n" +
            "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
            "                  <tr>\n" +
            "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
            "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
            "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
            "                  </tr>\n" +
            "                </table>\n" +
            "              </center>\n" +
            "              <!--[if gte mso 9]>\n" +
            "              </v:textbox>\n" +
            "            </v:rect>\n" +
            "            <![endif]-->\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td class=\"header-lg\">\n" +
            "              Notification!\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"free-text\">\n" +
            "Dear <Employer Name>,\n" +
            "You have requested to interview the following worker:\n" +
            "Ref: <MIN Ref>\n" +
            "Occupation: <Occupation>\n" +
            "Expected Salary: <Expected Salary>\n" +
            "Agency Fee: <Agency Fee>\n" +
            "\n" +
            "The agent will be in contact with you shortly to discuss arrangements for the interview.\n\n\n" +
            "Please do not hesitate to contact us if you have any questions.\n" +
            "\n\n" +
            "Thank you,\n" +
            "MaidInNigeria\n " +

            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"mini-block-container\">\n" +
            "Note:You are advised to complete the hire process from the portal " +
            " MaidInNigeria will not be responsible for any negligency..." +
            "            </td>\n" +

            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td style=\"padding: 25px 0 25px\">\n" +
            "              <strong>Maid In Nigeria</strong><br />\n" +
            "              Lagos, Nigeria <br />\n" +
            "               <br /><br />\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "</table>\n"
            ;
    public static final String AGT_REQ_MSG = "";

    public static final String USER_REQ_MSG = "<style type=\"text/css\">\n" +
            "\n" +           "                /* Take care of image borders and formatting, client hacks */\n\" +\n" +
            "                img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n\" +\n" +
            "                a img { border: none; }\n\" +\n" +
            "                table { border-collapse: collapse !important;}\n \n" +
            "                #outlook a { padding:0; }\n\n" +
            "                .ReadMsgBody { width: 100%; }\n\n" +
            "                .ExternalClass { width: 100%; }\n\n" +
            "                .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n\n" +
            "                table td { border-collapse: collapse; }\n\n" +
            "                .ExternalClass * { line-height: 115%; }\n\n" +
            "                .container-for-gmail-android { min-width: 600px; }\n\n" +
            "            \n\n" +
            "            \n\n" +
            "                /* General styling */\n\n" +
            "                * {\n\n" +
            "                  font-family: Helvetica, Arial, sans-serif;\n\n" +
            "                }\n\n" +
            "            \n \n" +
            "                body {\n\n" +
            "                  -webkit-font-smoothing: antialiased;\n\n" +
            "                  -webkit-text-size-adjust: none;\n" +
            "                  width: 100% !important;\n\n" +
            "                  margin: 0 !important;\n\n" +
            "                  height: 100%;\n\n" +
            "                  color: #676767;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                td {\n\n" +
            "                  font-family: Helvetica, Arial, sans-serif;\n\n" +
            "                  font-size: 14px;\n\n" +
            "                  color: #777777;\n\n" +
            "                  text-align: center;\n\n" +
            "                  line-height: 21px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                a {\n\n" +
            "                  color: #676767;\n\n" +
            "                  text-decoration: none !important;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .pull-left {\n\n" +
            "                  text-align: left;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .pull-right {\n\n" +
            "                  text-align: right;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .header-lg,\n\n" +
            "                .header-md,\n\n" +
            "                .header-sm {\n\n" +
            "                  font-size: 32px;\n\n" +
            "                  font-weight: 700;\n\n" +
            "                  line-height: normal;\n\n" +
            "                  padding: 35px 0 0;\n\n" +
            "                  color: #4d4d4d;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .header-md {\n\n" +
            "                  font-size: 24px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .header-sm {\n\n" +
            "                  padding: 5px 0;\n\n" +
            "                  font-size: 18px;\n\n" +
            "                  line-height: 1.3;\n\n" +
            "                  padding-bottom: 30px;\n\n" +
            "                  font-weight: 400;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .content-padding {\n\n" +
            "                  padding: 20px 0 30px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .mobile-header-padding-right {\n\n" +
            "                  width: 290px;\n\n" +
            "                  text-align: right;\n\n" +
            "                  padding-left: 10px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .mobile-header-padding-left {\n\n" +
            "                  width: 290px;\n\n" +
            "                  text-align: left;\n\n" +
            "                  padding-left: 10px;\n\n" +
            "                  padding-bottom: 8px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .free-text {\n\n" +
            "                  width: 100% !important;\n\n" +
            "                  padding: 10px 60px 0px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .block-rounded {\n\n" +
            "                  border-radius: 5px;\n\n" +
            "                  border: 1px solid #e5e5e5;\n\n" +
            "                  vertical-align: top;\n\n" +
            "                }\n\n" +
            "            \n \n" +
            "                .button {\n\n" +
            "                  padding: 55px 0 0;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .info-block {\n\n" +
            "                  padding: 0 20px;\n\n" +
            "                  width: 260px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .mini-block-container {\n\n" +
            "                  padding: 30px 50px;\n\n" +
            "                  width: 500px;\n\n" +
            "                }\n\n" +
            "            \n\n" +
            "                .mini-block {\\n\" +\n" +
            "                  background-color: #ffffff;\\n\" +\n" +
            "                  width: 498px;\\n\" +\n" +
            "                  border: 1px solid #e1e1e1;\\n\" +\n" +
            "                  border-radius: 5px;\\n\" +\n" +
            "                  padding: 25px 70px 60px;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .block-rounded {\\n\" +\n" +
            "                  width: 260px;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .info-img {\\n\" +\n" +
            "                  width: 258px;\\n\" +\n" +
            "                  border-radius: 5px 5px 0 0;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .force-width-img {\\n\" +\n" +
            "                  width: 480px;\\n\" +\n" +
            "                  height: 1px !important;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .force-width-full {\\n\" +\n" +
            "                  width: 600px;\\n\" +\n" +
            "                  height: 1px !important;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .user-img img {\\n\" +\n" +
            "                  width: 82px;\\n\" +\n" +
            "                  border-radius: 5px;\\n\" +\n" +
            "                  border: 1px solid #cccccc;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .user-img {\\n\" +\n" +
            "                  width: 92px;\\n\" +\n" +
            "                  text-align: left;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .user-msg {\\n\" +\n" +
            "                  width: 236px;\\n\" +\n" +
            "                  font-size: 14px;\\n\" +\n" +
            "                  text-align: left;\\n\" +\n" +
            "                  font-style: italic;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .code-block {\\n\" +\n" +
            "                  padding: 10px 0;\\n\" +\n" +
            "                  border: 1px solid #cccccc;\\n\" +\n" +
            "                  width: 20px;\\n\" +\n" +
            "                  color: #4d4d4d;\\n\" +\n" +
            "                  font-weight: bold;\\n\" +\n" +
            "                  font-size: 18px;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .center-txt {\\n\" +\n" +
            "                  padding-left: 30px;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "                .force-width-gmail {\\n\" +\n" +
            "                  min-width:600px;\\n\" +\n" +
            "                  height: 0px !important;\\n\" +\n" +
            "                  line-height: 1px !important;\\n\" +\n" +
            "                  font-size: 1px !important;\\n\" +\n" +
            "                }\\n\" +\n" +
            "            \\n\" +\n" +
            "              </style>\\n\" +\n" +
            "            \\n\" +\n" +
            "              <style type=\\\"text/css\\\" media=\\\"screen\\\">\\n\" +\n" +
            "                @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\\n\" +\n" +
            "              </style>\\n\" +\n" +
            "            \\n\" +\n" +
            "              <style type=\\\"text/css\\\" media=\\\"screen\\\">\\n\" +\n" +
            "                @media screen {\\n\" +\n" +
            "                  /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\\n\" +\n" +
            "                  * {\\n\" +\n" +
            "                    font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "                }\\n\" +\n" +
            "              </style>\\n\" +\n" +
            "            \\n\" +\n" +
            "              <style type=\\\"text/css\\\" media=\\\"only screen and (max-width: 480px)\\\">\\n\" +\n" +
            "                /* Mobile styles */\\n\" +\n" +
            "                @media only screen and (max-width: 480px) {\\n\" +\n" +
            "            \\n\" +\n" +
            "                  table[class*=\\\"container-for-gmail-android\\\"] {\\n\" +\n" +
            "                    min-width: 290px !important;\\n\" +\n" +
            "                    width: 100% !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  table[class=\\\"w320\\\"] {\\n\" +\n" +
            "                    width: 320px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  img[class=\\\"force-width-gmail\\\"] {\\n\" +\n" +
            "                    display: none !important;\\n\" +\n" +
            "                    width: 0 !important;\\n\" +\n" +
            "                    height: 0 !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class*=\\\"mobile-header-padding-left\\\"] {\\n\" +\n" +
            "                    width: 160px !important;\\n\" +\n" +
            "                    padding-left: 0 !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class*=\\\"mobile-header-padding-right\\\"] {\\n\" +\n" +
            "                    width: 160px !important;\\n\" +\n" +
            "                    padding-right: 0 !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"header-lg\\\"] {\\n\" +\n" +
            "                    font-size: 24px !important;\\n\" +\n" +
            "                    padding: 10px 50px 0 !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"header-md\\\"] {\\n\" +\n" +
            "                    font-size: 18px !important;\\n\" +\n" +
            "                    padding-bottom: 5px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"content-padding\\\"] {\\n\" +\n" +
            "                    padding: 5px 0 30px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                   td[class=\\\"button\\\"] {\\n\" +\n" +
            "                    padding: 5px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class*=\\\"free-text\\\"] {\\n\" +\n" +
            "                    padding: 10px 18px 30px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  img[class=\\\"force-width-img\\\"],\\n\" +\n" +
            "                  img[class=\\\"force-width-full\\\"] {\\n\" +\n" +
            "                    display: none !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"info-block\\\"] {\\n\" +\n" +
            "                    display: block !important;\\n\" +\n" +
            "                    width: 280px !important;\\n\" +\n" +
            "                    padding-bottom: 40px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"info-img\\\"],\\n\" +\n" +
            "                  img[class=\\\"info-img\\\"] {\\n\" +\n" +
            "                    width: 278px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"mini-block-container\\\"] {\\n\" +\n" +
            "                    padding: 8px 20px !important;\\n\" +\n" +
            "                    width: 280px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"mini-block\\\"] {\\n\" +\n" +
            "                    padding: 20px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"user-img\\\"] {\\n\" +\n" +
            "                    display: block !important;\\n\" +\n" +
            "                    text-align: center !important;\\n\" +\n" +
            "                    width: 100% !important;\\n\" +\n" +
            "                    padding-bottom: 10px;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"user-msg\\\"] {\\n\" +\n" +
            "                    display: block !important;\\n\" +\n" +
            "                    padding-bottom: 20px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "            \\n\" +\n" +
            "                  td[class=\\\"center-txt\\\"] {\\n\" +\n" +
            "                    padding-left: 3px !important;\\n\" +\n" +
            "                  }\\n\" +\n" +
            "                }\\n\" +\n" +
            "              </style>\\n\" +\n" +
            "            </head>\\n\" +\n" +
            "            \n\" +\n" +
            "            <body bgcolor=\\\"#f7f7f7\\\">\\n\" +\n" +
            "            <table align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"container-for-gmail-android\\\" width=\\\"100%\\\">\\n\" +\n" +
            "              <tr>\\n\" +\n" +
            "               <td align=\\\"left\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\\\">\\n\" +\n" +
            "                  <center>\\n\" +\n" +
            "                  <img src=\\\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\\\" class=\\\"force-width-gmail\\\">\\n\" +\n" +
            "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"100%\\\" bgcolor=\\\"#ffffff\\\" background=\\\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\\\" style=\\\"background-color:transparent\\\">\\n\" +\n" +
            "                      <tr>\\n\" +\n" +
            "                        <td width=\\\"100%\\\" height=\\\"80\\\" valign=\\\"top\\\" style=\\\"text-align: center; vertical-align:middle;\\\">\\n\" +\n" +
            "                        <!--[if gte mso 9]>\\n\" +\n" +
            "                        <v:rect xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" fill=\\\"true\\\" stroke=\\\"false\\\" style=\\\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\\\">\\n\" +\n" +
            "                          <v:fill type=\\\"tile\\\" src=\\\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\\\" color=\\\"#ffffff\\\" />\\n\" +\n" +
            "                          <v:textbox inset=\\\"0,0,0,0\\\">\\n\" +\n" +
            "                        <![endif]-->\\n\" +\n" +
            "                          <center>\\n\" +\n" +
            "                            <table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\">\\n\" +\n" +
            "                              <tr>\\n\" +\n" +
            "                                <td class=\\\"pull-left mobile-header-padding-left\\\" style=\\\"vertical-align: middle;\\\">\\n\" +\n" +
            "                                  <a href=\\\"\\\"><h3>Maid In Nigeria</h3></a>\\n\" +\n" +
            "                                </td>\\n\" +\n" +
            "                                <td class=\\\"pull-right mobile-header-padding-right\\\" style=\\\"color: #4d4d4d;\\\">\\n\" +\n" +
            "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-twitter.png\\\" alt=\\\"twitter\\\" /></a>\\n\" +\n" +
            "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-fb.png\\\" alt=\\\"facebook\\\" /></a>\\n\" +\n" +
            "                                  <a href=\\\"\\\"><img width=\\\"40\\\" height=\\\"47\\\" src=\\\"img/social-feed.png\\\" alt=\\\"rss\\\" /></a>\\n\" +\n" +
            "                                </td>\\n\" +*/\n" +
            "                              </tr>\\n\" +\n" +
            "                            </table>\\n\" +\n" +
            "                          </center>\\n\" +\n" +
            "                          <!--[if gte mso 9]>\\n\" +\n" +
            "                          </v:textbox>\\n\" +\n" +
            "                        </v:rect>\\n\" +\n" +
            "                        <![endif]-->\\n\" +\n" +
            "                        </td>\\n\" +\n" +
            "                      </tr>\\n\" +\n" +
            "                    </table>\\n\" +\n" +
            "                  </center>\\n\" +\n" +
            "                </td>\\n\" +\n" +
            "              </tr>\\n\" +\n" +
            "              <tr>\\n\" +\n" +
            "                <td align=\\\"center\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background-color: #f7f7f7;\\\" class=\\\"content-padding\\\">\\n\" +\n" +
            "                  <center>\\n\" +\n" +
            "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\">\\n\" +\n" +
            "                      <tr>\\n\" +\n" +
            "                        <td class=\\\"header-lg\\\">\\n\" +\n" +
            "                          Notification!\\n\" +\n" +
            "                        </td>\\n\" +\n" +
            "                      </tr>\\n\" +\n" +
            "                      <tr>\\n\" +\n" +
            "                        <td class=\\\"free-text\\\">\\n\" +\n" +
            "            Dear <Employer Name>,\\n\" +\n" +
            "            You have requested to interview the following worker:\n\" +\n" +
            "            Ref: <MIN Ref>\n\n" +
            "            Occupation: <Occupation>\n\n" +
            "            Expected Salary: <Expected Salary>\n\n" +
            "            Agency Fee: <Agency Fee>\n\n" +
            "            \n\n" +
            "            The agent will be in contact with you shortly to discuss arrangements for the interview.\n\n\n\n" +
            "            Please do not hesitate to contact us if you have any questions.\n\" +\n" +
            "            \n\n\n" +
            "            Thank you,\n\n" +
            "            MaidInNigeria\n \n" +
            "\n" +
            "                        </td>\n\n" +
            "                      </tr>\n\n" +
            "                      <tr>\n\n" +
            "                        <td class=\\\"mini-block-container\\\">\n\n" +
            "            Note:You are advised to complete the hire process from the portal \n" +
            "             MaidInNigeria will not be responsible for any negligency...\n" +
            "                        </td>\n\n" +
            "\n" +           "                      </tr>\n\n" +
            "                   </table>\n\n" +
            "                  </center>\n\n" +
            "                </td>\n\n" +
            "              </tr>\n\n" +
            "              <tr>\n\n" +
            "                <td align=\\\"center\\\" valign=\\\"top\\\" width=\\\"100%\\\" style=\\\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\\\">\\n\" +\n" +
            "                  <center>\n\n" +
            "                    <table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" width=\\\"600\\\" class=\\\"w320\\\">\\n\" +\n" +
            "                      <tr>\\n\" +\n" +
            "                        <td style=\\\"padding: 25px 0 25px\\\">\\n\" +\n" +
            "                          <strong>Maid In Nigeria</strong><br />\\n\" +\n" +
            "                          Lagos, Nigeria <br />\\n\" +\n" +
            "                           <br /><br />\\\n" +
            "                        </td>\n" +
            "                      </tr>\n" +
            "                    </table>\n" +
            "                  </center>\n" +
            "                </td>\n" +
            "              </tr>\n" +
            "            </table>\n";

    public static final String MAIL_USN="maidinnigeria";
    public static final String MAIL_PSD="HDSgroup2015";
    public static final String MAIL_ADD="maidinnigeria@gmail.com";

    String InsurMessage = "<style type=\"text/css\">\n" +
            "    /* Take care of image borders and formatting, client hacks */\n" +
            "    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\n" +
            "    a img { border: none; }\n" +
            "    table { border-collapse: collapse !important;}\n" +
            "    #outlook a { padding:0; }\n" +
            "    .ReadMsgBody { width: 100%; }\n" +
            "    .ExternalClass { width: 100%; }\n" +
            "    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\n" +
            "    table td { border-collapse: collapse; }\n" +
            "    .ExternalClass * { line-height: 115%; }\n" +
            "    .container-for-gmail-android { min-width: 600px; }\n" +
            "\n" +
            "\n" +
            "    /* General styling */\n" +
            "    * {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "    }\n" +
            "\n" +
            "    body {\n" +
            "      -webkit-font-smoothing: antialiased;\n" +
            "      -webkit-text-size-adjust: none;\n" +
            "      width: 100% !important;\n" +
            "      margin: 0 !important;\n" +
            "      height: 100%;\n" +
            "      color: #676767;\n" +
            "    }\n" +
            "\n" +
            "    td {\n" +
            "      font-family: Helvetica, Arial, sans-serif;\n" +
            "      font-size: 14px;\n" +
            "      color: #777777;\n" +
            "      text-align: center;\n" +
            "      line-height: 21px;\n" +
            "    }\n" +
            "\n" +
            "    a {\n" +
            "      color: #676767;\n" +
            "      text-decoration: none !important;\n" +
            "    }\n" +
            "\n" +
            "    .pull-left {\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .pull-right {\n" +
            "      text-align: right;\n" +
            "    }\n" +
            "\n" +
            "    .header-lg,\n" +
            "    .header-md,\n" +
            "    .header-sm {\n" +
            "      font-size: 32px;\n" +
            "      font-weight: 700;\n" +
            "      line-height: normal;\n" +
            "      padding: 35px 0 0;\n" +
            "      color: #4d4d4d;\n" +
            "    }\n" +
            "\n" +
            "    .header-md {\n" +
            "      font-size: 24px;\n" +
            "    }\n" +
            "\n" +
            "    .header-sm {\n" +
            "      padding: 5px 0;\n" +
            "      font-size: 18px;\n" +
            "      line-height: 1.3;\n" +
            "      padding-bottom: 30px;\n" +
            "      font-weight: 400;\n" +
            "    }\n" +
            "\n" +
            "    .content-padding {\n" +
            "      padding: 20px 0 30px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-right {\n" +
            "      width: 290px;\n" +
            "      text-align: right;\n" +
            "      padding-left: 10px;\n" +
            "    }\n" +
            "\n" +
            "    .mobile-header-padding-left {\n" +
            "      width: 290px;\n" +
            "      text-align: left;\n" +
            "      padding-left: 10px;\n" +
            "      padding-bottom: 8px;\n" +
            "    }\n" +
            "\n" +
            "    .free-text {\n" +
            "      width: 100% !important;\n" +
            "      padding: 10px 60px 0px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #e5e5e5;\n" +
            "      vertical-align: top;\n" +
            "    }\n" +
            "\n" +
            "    .button {\n" +
            "      padding: 55px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .info-block {\n" +
            "      padding: 0 20px;\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .mini-block-container {\n" +
            "      padding: 30px 50px;\n" +
            "      width: 500px;\n" +
            "    }\n" +
            "\n" +
            "    .mini-block {\n" +
            "      background-color: #ffffff;\n" +
            "      width: 498px;\n" +
            "      border: 1px solid #e1e1e1;\n" +
            "      border-radius: 5px;\n" +
            "      padding: 25px 70px 60px;\n" +
            "    }\n" +
            "\n" +
            "    .block-rounded {\n" +
            "      width: 260px;\n" +
            "    }\n" +
            "\n" +
            "    .info-img {\n" +
            "      width: 258px;\n" +
            "      border-radius: 5px 5px 0 0;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-img {\n" +
            "      width: 480px;\n" +
            "      height: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-full {\n" +
            "      width: 600px;\n" +
            "      height: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "    .user-img img {\n" +
            "      width: 82px;\n" +
            "      border-radius: 5px;\n" +
            "      border: 1px solid #cccccc;\n" +
            "    }\n" +
            "\n" +
            "    .user-img {\n" +
            "      width: 92px;\n" +
            "      text-align: left;\n" +
            "    }\n" +
            "\n" +
            "    .user-msg {\n" +
            "      width: 236px;\n" +
            "      font-size: 14px;\n" +
            "      text-align: left;\n" +
            "      font-style: italic;\n" +
            "    }\n" +
            "\n" +
            "    .code-block {\n" +
            "      padding: 10px 0;\n" +
            "      border: 1px solid #cccccc;\n" +
            "      width: 20px;\n" +
            "      color: #4d4d4d;\n" +
            "      font-weight: bold;\n" +
            "      font-size: 18px;\n" +
            "    }\n" +
            "\n" +
            "    .center-txt {\n" +
            "      padding-left: 30px;\n" +
            "    }\n" +
            "\n" +
            "    .force-width-gmail {\n" +
            "      min-width:600px;\n" +
            "      height: 0px !important;\n" +
            "      line-height: 1px !important;\n" +
            "      font-size: 1px !important;\n" +
            "    }\n" +
            "\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"screen\">\n" +
            "    @media screen {\n" +
            "      /* Thanks Outlook 2013! http://goo.gl/XLxpyl */\n" +
            "      * {\n" +
            "        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "\n" +
            "  <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\n" +
            "    /* Mobile styles */\n" +
            "    @media only screen and (max-width: 480px) {\n" +
            "\n" +
            "      table[class*=\"container-for-gmail-android\"] {\n" +
            "        min-width: 290px !important;\n" +
            "        width: 100% !important;\n" +
            "      }\n" +
            "\n" +
            "      table[class=\"w320\"] {\n" +
            "        width: 320px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-gmail\"] {\n" +
            "        display: none !important;\n" +
            "        width: 0 !important;\n" +
            "        height: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-left\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-left: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"mobile-header-padding-right\"] {\n" +
            "        width: 160px !important;\n" +
            "        padding-right: 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-lg\"] {\n" +
            "        font-size: 24px !important;\n" +
            "        padding: 10px 50px 0 !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"header-md\"] {\n" +
            "        font-size: 18px !important;\n" +
            "        padding-bottom: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"content-padding\"] {\n" +
            "        padding: 5px 0 30px !important;\n" +
            "      }\n" +
            "\n" +
            "       td[class=\"button\"] {\n" +
            "        padding: 5px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class*=\"free-text\"] {\n" +
            "        padding: 10px 18px 30px !important;\n" +
            "      }\n" +
            "\n" +
            "      img[class=\"force-width-img\"],\n" +
            "      img[class=\"force-width-full\"] {\n" +
            "        display: none !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-block\"] {\n" +
            "        display: block !important;\n" +
            "        width: 280px !important;\n" +
            "        padding-bottom: 40px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"info-img\"],\n" +
            "      img[class=\"info-img\"] {\n" +
            "        width: 278px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"mini-block-container\"] {\n" +
            "        padding: 8px 20px !important;\n" +
            "        width: 280px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"mini-block\"] {\n" +
            "        padding: 20px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"user-img\"] {\n" +
            "        display: block !important;\n" +
            "        text-align: center !important;\n" +
            "        width: 100% !important;\n" +
            "        padding-bottom: 10px;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"user-msg\"] {\n" +
            "        display: block !important;\n" +
            "        padding-bottom: 20px !important;\n" +
            "      }\n" +
            "\n" +
            "      td[class=\"center-txt\"] {\n" +
            "        padding-left: 3px !important;\n" +
            "      }\n" +
            "    }\n" +
            "  </style>\n" +
            "</head>\n" +
            "\n" +
            "<body bgcolor=\"#f7f7f7\">\n" +
            "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container-for-gmail-android\" width=\"100%\">\n" +
            "  <tr>\n" +
            "    <td align=\"left\" valign=\"top\" width=\"100%\" style=\"background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;\">\n" +
            "      <center>\n" +
            "      <img src=\"http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png\" class=\"force-width-gmail\">\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" bgcolor=\"#ffffff\" background=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" style=\"background-color:transparent\">\n" +
            "          <tr>\n" +
            "            <td width=\"100%\" height=\"80\" valign=\"top\" style=\"text-align: center; vertical-align:middle;\">\n" +
            "            <!--[if gte mso 9]>\n" +
            "            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:80px; v-text-anchor:middle;\">\n" +
            "              <v:fill type=\"tile\" src=\"http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg\" color=\"#ffffff\" />\n" +
            "              <v:textbox inset=\"0,0,0,0\">\n" +
            "            <![endif]-->\n" +
            "              <center>\n" +
            "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"w320\">\n" +
            "                  <tr>\n" +
            "                    <td class=\"pull-left mobile-header-padding-left\" style=\"vertical-align: middle;\">\n" +
            "                      <a href=\"\"><h3>Maid In Nigeria</h3></a>\n" +
            "                    </td>\n" +
            /*"                    <td class=\"pull-right mobile-header-padding-right\" style=\"color: #4d4d4d;\">\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-twitter.png\" alt=\"twitter\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-fb.png\" alt=\"facebook\" /></a>\n" +
            "                      <a href=\"\"><img width=\"40\" height=\"47\" src=\"img/social-feed.png\" alt=\"rss\" /></a>\n" +
            "                    </td>\n" +*/
            "                  </tr>\n" +
            "                </table>\n" +
            "              </center>\n" +
            "              <!--[if gte mso 9]>\n" +
            "              </v:textbox>\n" +
            "            </v:rect>\n" +
            "            <![endif]-->\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #f7f7f7;\" class=\"content-padding\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td class=\"header-lg\">\n" +
            "              Notification!\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"free-text\">\n" +
            "NOTIFICATION ON PREPARING INDEMNITY INSURANCE \n\n\n" +
            "This is to inform you to prepare indemnity insurance for a newly hired maid \n" +
            "Name:  \n" +
            "Agency Name: \n" +
            "            </td>\n" +
            "          </tr>\n" +
            "          <tr>\n" +
            "            <td class=\"mini-block-container\">\n" +
            "Note: Agent will make contact to get the Indemnity Insurance Policy.. " +
            " " +
            "            </td>\n" +

            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "  <tr>\n" +
            "    <td align=\"center\" valign=\"top\" width=\"100%\" style=\"background-color: #ffffff; border-top:1px solid #cccccc; height: 100px;\">\n" +
            "      <center>\n" +
            "        <table cellspacing=\"0\" cellpadding=\"0\" width=\"600\" class=\"w320\">\n" +
            "          <tr>\n" +
            "            <td style=\"padding: 25px 0 25px\">\n" +
            "              <strong>Maid In Nigeria</strong><br />\n" +
            "              Lagos, Nigeria <br />\n" +
            "               <br /><br />\n" +
            "            </td>\n" +
            "          </tr>\n" +
            "        </table>\n" +
            "      </center>\n" +
            "    </td>\n" +
            "  </tr>\n" +
            "</table>\n" +
            "";
}
