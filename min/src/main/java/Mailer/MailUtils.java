package Mailer;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Created by timtac on 3/16/2017.
 */
public class MailUtils {
    public static void sendMail(String[] recipient, String subject, String body) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource("https://api.sparkpost.com/api/v1/transmissions");

            JsonObject content = new JsonObject();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("from","postmaster@novatiang.com");
            jsonObject.addProperty("subject", subject);
            jsonObject.addProperty("text", body);

            JsonArray jsonArray = new JsonArray();

            for(String addr : recipient){
                JsonObject address = new JsonObject();
                address.addProperty("address",addr);
                jsonArray.add(address);
            }
    System.out.println("got her, create address");
            JsonObject payload = new JsonObject();
            payload.add("content", jsonObject);
            payload.add("recipient", jsonArray);

            ClientResponse response = webResource.header("Authorization", "d02dd38dd90591985f89a872de983e4d3dfd2450").type("application/json")
                    .post(ClientResponse.class,  payload.toString());

                if (response.getStatus() != 200){
                    throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
                }

                String output = response.getEntity(String.class);
                System.out.println(output);

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}