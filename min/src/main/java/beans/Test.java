package beans;

import Dao.Workers_dao;
import Models.Workers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by user on 7/5/2016.
 */
@WebServlet("/test")
public class Test extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Workers_dao workersDao = new Workers_dao();
            List<Workers> list = workersDao.workersJoinGuarantor(1);
            for(int i = 0;list.size() >i; i++){
                Workers workers = list.get(i);

                System.out.println(workers.getAgency_fee_type());
                System.out.println(workers.getWorkers_fullname());
            }
            System.out.println("found nothing");
        }catch (Exception ex){
ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
