package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/9/2016.
 */
@DatabaseTable(tableName = "medicalLabs")
public class MedicalLabs {
    @DatabaseField(generatedId = true)
    private int medicalLab_id;
    @DatabaseField
    private String medicalLab_name;
    @DatabaseField
    private String medicalLab_mobile;
    @DatabaseField
    private String medicalLab_location;
    @DatabaseField
    private String medicalLab_address;
    @DatabaseField
    private String medicalLab_email;

    public MedicalLabs(){

    }

    public int getMedicalLab_id() {
        return medicalLab_id;
    }

    public void setMedicalLab_id(int medicalLab_id) {
        this.medicalLab_id = medicalLab_id;
    }

    public String getMedicalLab_name() {
        return medicalLab_name;
    }

    public void setMedicalLab_name(String medicalLab_name) {
        this.medicalLab_name = medicalLab_name;
    }

    public String getMedicalLab_mobile() {
        return medicalLab_mobile;
    }

    public void setMedicalLab_mobile(String medicalLab_mobile) {
        this.medicalLab_mobile = medicalLab_mobile;
    }

    public String getMedicalLab_location() {
        return medicalLab_location;
    }

    public void setMedicalLab_location(String medicalLab_location) {
        this.medicalLab_location = medicalLab_location;
    }

    public String getMedicalLab_address() {
        return medicalLab_address;
    }

    public void setMedicalLab_address(String medicalLab_address) {
        this.medicalLab_address = medicalLab_address;
    }

    public String getMedicalLab_email() {
        return medicalLab_email;
    }

    public void setMedicalLab_email(String medicalLab_email) {
        this.medicalLab_email = medicalLab_email;
    }
}
