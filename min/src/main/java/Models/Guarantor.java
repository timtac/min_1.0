package Models;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by user on 9/28/2015.
 */
@DatabaseTable(tableName = "guarantor")
public class Guarantor {
    @DatabaseField(generatedId = true)
    private int guarantor_id;
    @DatabaseField(foreign = true,columnName = "workers_id")
    private Workers workers_id;
    @DatabaseField
    private String guarantor_title;
    @DatabaseField
    private String guarantor_firstname;
    @DatabaseField
    private String guarantor_lastname;
    @DatabaseField
    private String guarantor_home_address;
    @DatabaseField
    private String guarantor_occupation;
    @DatabaseField
    private String guarantor_work_address;
    @DatabaseField
    private String relationship;
    @DatabaseField
    private String guarantor_phone_number;
    @DatabaseField
    private String e_mail;
    @DatabaseField
    private String image;


    public Guarantor(){

    }

    public String getGuarantor_title() {
        return guarantor_title;
    }

    public int getGuarantor_id() {
        return guarantor_id;
    }

    public void setGuarantor_id(int guarantor_id) {
        this.guarantor_id = guarantor_id;
    }

    public void setGuarantor_title(String guarantor_title) {
        this.guarantor_title = guarantor_title;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public String getGuarantor_firstname() {
        return guarantor_firstname;
    }

    public void setGuarantor_firstname(String guarantor_firstname) {
        this.guarantor_firstname = guarantor_firstname;
    }

    public String getGuarantor_lastname() {
        return guarantor_lastname;
    }

    public void setGuarantor_lastname(String guarantor_lastname) {
        this.guarantor_lastname = guarantor_lastname;
    }

    public String getGuarantor_home_address() {
        return guarantor_home_address;
    }

    public void setGuarantor_home_address(String guarantor_home_address) {
        this.guarantor_home_address = guarantor_home_address;
    }

    public String getGuarantor_occupation() {
        return guarantor_occupation;
    }

    public void setGuarantor_occupation(String guarantor_occupation) {
        this.guarantor_occupation = guarantor_occupation;
    }

    public String getGuarantor_work_address() {
        return guarantor_work_address;
    }

    public void setGuarantor_work_address(String guarantor_work_address) {
        this.guarantor_work_address = guarantor_work_address;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getGuarantor_phone_number() {
        return guarantor_phone_number;
    }

    public void setGuarantor_phone_number(String guarantor_phone_number) {
        this.guarantor_phone_number = guarantor_phone_number;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
