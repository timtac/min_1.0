package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 9/28/2015.
 */
@DatabaseTable(tableName="users")
public class Users {
    @DatabaseField(generatedId = true)
    private int users_id;
    @DatabaseField
    private String user_title;
    @DatabaseField
    private String users_fullname;
    @DatabaseField
    private String users_username;
    @DatabaseField
    private String users_password;
    @DatabaseField
    private String users_location;
    @DatabaseField
    private String users_gender;
    @DatabaseField
    private String users_address;
    @DatabaseField(unique = true)
    private String user_email;
    @DatabaseField
    private String user_mobile;
    @DatabaseField
    private String user_image;
    @DatabaseField
    private String signupDate;
    @DatabaseField
    private String lastSeen;


    public Users(){

    }

    public int getUsers_id() {
        return users_id;
    }

    public void setUsers_id(int users_id) {
        this.users_id = users_id;
    }

    public String getUsers_fullname() {
        return users_fullname;
    }

    public void setUsers_fullname(String users_fullname) {
        this.users_fullname = users_fullname;
    }

    public String getUsers_username() {
        return users_username;
    }

    public void setUsers_username(String users_username) {
        this.users_username = users_username;
    }

    public String getUsers_password() {
        return users_password;
    }

    public void setUsers_password(String users_password) {
        this.users_password = users_password;
    }

    public String getUsers_location() {
        return users_location;
    }

    public void setUsers_location(String users_location) {
        this.users_location = users_location;
    }

    public String getUsers_address() {
        return users_address;
    }

    public void setUsers_address(String users_address) {
        this.users_address = users_address;
    }

    public String getUser_title() {
        return user_title;
    }

    public void setUser_title(String user_title) {
        this.user_title = user_title;
    }

    public String getUsers_gender() {
        return users_gender;
    }

    public void setUsers_gender(String users_gender) {
        this.users_gender = users_gender;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getSignupDate() {
        return signupDate;
    }

    public void setSignupDate(String signupDate) {
        this.signupDate = signupDate;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }
}
