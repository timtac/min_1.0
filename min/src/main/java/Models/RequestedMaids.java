package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/1/2016.
 */
@DatabaseTable(tableName = "requestedMaids")
public class RequestedMaids {
    @DatabaseField(generatedId = true)
    public int request_id;
    @DatabaseField(foreign = true)
    public Workers workers_id;
    @DatabaseField(foreign = true)
    public Users users_id;
    @DatabaseField
    public String requestDate;

    public RequestedMaids(){

    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }
}
