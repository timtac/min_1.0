package Models;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
/**
 * Created by user on 9/28/2015.
 */
@DatabaseTable(tableName="workers")
public class Workers {
    @DatabaseField(generatedId = true)
    private int workers_id;
    @DatabaseField
    private String workers_fullname;
    @DatabaseField(foreign = true,columnName = "agent_Company_Name")
    private Agents agent_Company_Name;
    @DatabaseField
    private String worker_age;
    @DatabaseField
    private String worker_sex;
    @DatabaseField
    private String worker_relationship;
    @DatabaseField
    private String worker_occupation;
    @DatabaseField
    private String worker_location;
    @DatabaseField
    private String worker_address;
    @DatabaseField
    private String worker_medical_history;
    @DatabaseField
    private String police_report;
    @DatabaseField
    private String educational_level;
    @DatabaseField
    private String language;
    @DatabaseField(unique = true)
    private String worker_mobile;
    @DatabaseField(defaultValue = "Unemployed")
    private String employed;
    @DatabaseField
    private String agency_fee;
    @DatabaseField
    private String agency_fee_type;
    @DatabaseField
    private String expected_salary;
    @DatabaseField
    private String preference;
    @DatabaseField
    private String total;
    @DatabaseField
    private String worker_image;
    @DatabaseField
    private String registeredDate;
    @DatabaseField
    private String constrain;


    public Workers(){

    }

    public int getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(int workers_id) {
        this.workers_id = workers_id;
    }

    public String getWorkers_fullname() {
        return workers_fullname;
    }

    public void setWorkers_fullname(String workers_fullname) {
        this.workers_fullname = workers_fullname;
    }

    public Agents getAgent_Company_Name() {
        return agent_Company_Name;
    }

    public void setAgent_Company_Name(Agents agent_Company_Name) {
        this.agent_Company_Name = agent_Company_Name;
    }

    public String getPolice_report() {
        return police_report;
    }

    public void setPolice_report(String police_report) {
        this.police_report = police_report;
    }

    public String getEducational_level() {
        return educational_level;
    }

    public void setEducational_level(String educational_level) {
        this.educational_level = educational_level;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAgency_fee() {
        return agency_fee;
    }

    public void setAgency_fee(String agency_fee) {
        this.agency_fee = agency_fee;
    }

    public String getAgency_fee_type() {
        return agency_fee_type;
    }

    public void setAgency_fee_type(String agency_fee_type) {
        this.agency_fee_type = agency_fee_type;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getTotal() {
        return total;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getEmployed() {
        return employed;
    }

    public void setEmployed(String employed) {
        this.employed = employed;
    }

    public String getWorker_image() {
        return worker_image;
    }

    public void setWorker_image(String worker_image) {
        this.worker_image = worker_image;
    }

    public String getWorker_age() {
        return worker_age;
    }

    public void setWorker_age(String worker_age) {
        this.worker_age = worker_age;
    }

    public String getWorker_sex() {
        return worker_sex;
    }

    public void setWorker_sex(String worker_sex) {
        this.worker_sex = worker_sex;
    }

    public String getWorker_relationship() {
        return worker_relationship;
    }

    public void setWorker_relationship(String worker_relationship) {
        this.worker_relationship = worker_relationship;
    }

    public String getWorker_occupation() {
        return worker_occupation;
    }

    public void setWorker_occupation(String worker_occupation) {
        this.worker_occupation = worker_occupation;
    }

    public String getWorker_location() {
        return worker_location;
    }

    public void setWorker_location(String worker_location) {
        this.worker_location = worker_location;
    }

    public String getWorker_address() {
        return worker_address;
    }

    public void setWorker_address(String worker_address) {
        this.worker_address = worker_address;
    }

    public String getWorker_medical_history() {
        return worker_medical_history;
    }

    public void setWorker_medical_history(String worker_medical_history) {
        this.worker_medical_history = worker_medical_history;
    }

    public String getWorker_mobile() {
        return worker_mobile;
    }

    public void setWorker_mobile(String worker_mobile) {
        this.worker_mobile = worker_mobile;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(String registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getConstrain() {
        return constrain;
    }

    public void setConstrain(String constrain) {
        this.constrain = constrain;
    }
}
