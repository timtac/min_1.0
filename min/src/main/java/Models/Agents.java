package Models;


import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by user on 9/28/2015.
 */
@DatabaseTable(tableName = "agents")
public class Agents {
    @DatabaseField
    private String title;
    @DatabaseField
    private String agent_fullName;
    @DatabaseField
    private String gender;
    @DatabaseField(id = true)
    private String agent_Company_Name;
    @DatabaseField
    private String company_Reg_No;
    @DatabaseField
    private String agents_location;
    @DatabaseField
    private String company_address;
    @DatabaseField
    private String agent_email;
    @DatabaseField
    private String agent_mobile;
    @DatabaseField
    private String agent_username;
    @DatabaseField
    private String agent_password;
    @DatabaseField
    private String agent_image;
    @DatabaseField
    private String signupDate;
    @DatabaseField
    private String lastSeen;
    @DatabaseField
    private String constrain;

    public Agents(){

    }

    public String getAgent_fullName() {
        return agent_fullName;
    }

    public void setAgent_fullName(String agent_fullName) {
        this.agent_fullName = agent_fullName;
    }

    public String getAgent_Company_Name() {
        return agent_Company_Name;
    }

    public void setAgent_Company_Name(String agent_Company_Name) {
        this.agent_Company_Name = agent_Company_Name;
    }

    public String getCompany_Reg_No() {
        return company_Reg_No;
    }

    public void setCompany_Reg_No(String company_Reg_No) {
        this.company_Reg_No = company_Reg_No;
    }

    public String getAgents_location() {
        return agents_location;
    }

    public void setAgents_location(String agents_location) {
        this.agents_location = agents_location;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getAgent_email() {
        return agent_email;
    }

    public void setAgent_email(String agent_email) {
        this.agent_email = agent_email;
    }

    public String getAgent_mobile() {
        return agent_mobile;
    }

    public void setAgent_mobile(String agent_mobile) {
        this.agent_mobile = agent_mobile;
    }

    public String getAgent_username() {
        return agent_username;
    }

    public void setAgent_username(String agent_username) {
        this.agent_username = agent_username;
    }

    public String getAgent_password() {
        return agent_password;
    }

    public void setAgent_password(String agent_password) {
        this.agent_password = agent_password;
    }

    public String getAgent_image() {
        return agent_image;
    }

    public void setAgent_image(String agent_image) {
        this.agent_image = agent_image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSignupDate() {
        return signupDate;
    }

    public void setSignupDate(String signupDate) {
        this.signupDate = signupDate;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getConstrain() {
        return constrain;
    }

    public void setConstrain(String constrain) {
        this.constrain = constrain;
    }
}
