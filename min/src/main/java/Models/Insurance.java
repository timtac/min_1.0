package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 2/25/2016.
 */
@DatabaseTable(tableName = "insurance")
public class Insurance {
    @DatabaseField(generatedId  = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String address;
    @DatabaseField
    private String email;
    @DatabaseField
    private String mobile;

    public Insurance(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
