package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/6/2016.
 */
@DatabaseTable
public class DisengagedMaid {
    @DatabaseField(generatedId = true)
    private int disengaged_id;
    @DatabaseField(foreign = true)
    private Workers worker_id;
    @DatabaseField(foreign = true)
    private Users users_id;
    @DatabaseField
    private String date_disengaged;

    public DisengagedMaid(){

    }

    public int getDisengaged_id() {
        return disengaged_id;
    }

    public void setDisengaged_id(int disengaged_id) {
        this.disengaged_id = disengaged_id;
    }

    public Workers getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(Workers worker_id) {
        this.worker_id = worker_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    public String getDate_disengaged() {
        return date_disengaged;
    }

    public void setDate_disengaged(String date_disengaged) {
        this.date_disengaged = date_disengaged;
    }
}
