package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 3/3/2016.
 */
@DatabaseTable(tableName = "backgroundCheck")
public class BackgroundCheck {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(foreign = true,columnName = "workers_id")
    private Workers workers_id;
    @DatabaseField
    private String birth;
    @DatabaseField
    private String town;
    @DatabaseField
    private String localGov;
    @DatabaseField
    private String state;
    @DatabaseField
    private String nationality;
    @DatabaseField
    private String idCard;
    @DatabaseField
    private String id_number;
    @DatabaseField
    private String id_image;
    @DatabaseField
    private String mother_name;

    public BackgroundCheck(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getLocalGov() {
        return localGov;
    }

    public void setLocalGov(String localGov) {
        this.localGov = localGov;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getId_image() {
        return id_image;
    }

    public void setId_image(String id_image) {
        this.id_image = id_image;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

}
