package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/1/2016.
 */
@DatabaseTable(tableName = "reference")
public class Reference {
    @DatabaseField(generatedId = true)
    public int reference_id;
    @DatabaseField(foreign = true)
    public Workers workers_id;
    @DatabaseField(foreign = true)
    public Users users_id;
    @DatabaseField
    public String comments;
    @DatabaseField
    public String reference;

    public Reference(){

    }

    public int getReference_id() {
        return reference_id;
    }

    public void setReference_id(int reference_id) {
        this.reference_id = reference_id;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
