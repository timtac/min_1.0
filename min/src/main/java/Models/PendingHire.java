package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/19/2016.
 */
@DatabaseTable(tableName = "pendingHire")
public class PendingHire {
    @DatabaseField(generatedId = true)
    private int pending_id;
    @DatabaseField(foreign = true,columnName = "workers_id")
    private Workers workers_id;
    @DatabaseField(foreign = true,columnName = "users_id")
    private Users users_id;

    public PendingHire(){

    }

    public int getPending_id() {
        return pending_id;
    }

    public void setPending_id(int pending_id) {
        this.pending_id = pending_id;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }
}
