package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 6/1/2016.
 */
@DatabaseTable(tableName = "hiredMaids")
public class HiredMaid {
    @DatabaseField(generatedId = true)
    public int hiredmaid_id;
    @DatabaseField(foreign = true)
    public Workers workers_id;
    @DatabaseField(foreign = true)
    public Users users_id;
    @DatabaseField
    public String replaceMaid;
    @DatabaseField
    public String hired_date;
    @DatabaseField
    public String due_date;

    public HiredMaid() {

    }

    public int getHiredmaid_id() {
        return hiredmaid_id;
    }

    public void setHiredmaid_id(int hiredmaid_id) {
        this.hiredmaid_id = hiredmaid_id;
    }

    public Workers getWorkers_id() {
        return workers_id;
    }

    public void setWorkers_id(Workers workers_id) {
        this.workers_id = workers_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    public String getReplaceMaid() {
        return replaceMaid;
    }

    public void setReplaceMaid(String replaceMaid) {
        this.replaceMaid = replaceMaid;
    }

    public String getHired_date() {
        return hired_date;
    }

    public void setHired_date(String hired_date) {
        this.hired_date = hired_date;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }
}
