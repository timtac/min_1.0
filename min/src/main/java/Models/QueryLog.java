package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 7/11/2016.
 */
@DatabaseTable(tableName = "queryLog")
public class QueryLog {
    @DatabaseField(generatedId = true)
    private int queryData_id;
    @DatabaseField(foreign = true)
    private Users users_id;
    @DatabaseField
    private String queryData;
    @DatabaseField
    private String queryDate;

    public QueryLog(){

    }

    public int getQueryData_id() {
        return queryData_id;
    }

    public void setQueryData_id(int queryData_id) {
        this.queryData_id = queryData_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    public String getQueryData() {
        return queryData;
    }

    public void setQueryData(String queryData) {
        this.queryData = queryData;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }
}
