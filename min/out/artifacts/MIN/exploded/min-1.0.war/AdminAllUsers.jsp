<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/18/2016
  Time: 3:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(session != null){
        String state =(String) session.getAttribute("login");
        if(state==null){
            response.sendRedirect("/min/AdminLogin.jsp");
        }
    }
    else{
        response.sendRedirect("/min/AdminLogin.jsp");
    }
    String name = (String) session.getAttribute("username");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_page.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table_jui.css" rel="stylesheet">
    <link href="asset/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables_themeroller.css" rel="stylesheet">
    <link href="asset/js/jquery-ui-1.11.4/jquery-ui.css" media="all" rel="stylesheet" type="text/css">
    <script src="asset/js/datatables/js/jquery.dataTables.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">

    <script src="asset/js/datatables/js/jquery.js"></script>
    <script src="asset/js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />
                <%--<div class="profile_info">


                </div>--%>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">

                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <%--<div class="wrapper">--%>
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/adminPage.jsp">Dashboard</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllUsers.jsp">Users</a>
                            </li>

                            <li >
                                <a href="/min/AdminAllAgents.jsp">Agents</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllWorkers.jsp">Domestic Staffs</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllHiredStaff.jsp">Hired Staffs</a>
                            </li>

                            <li>
                                <a href="/min/AdminUnassignedWorkers.jsp">Un-Assigned Staffs</a>
                            </li>
                            <li>
                                <a href="/min/PendingHire.jsp">Pending Hires</a>
                            </li>
                            <li>
                                <a href="/min/MailAllAgents.jsp">Mail All Agents</a>
                            </li>

                            <li><a href="/min/MailAllUsers.jsp"> Mail All Users</a>

                        </ul>
                    <%--</div>--%>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="asset/images/user.png" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="/min/AgentsReg.jsp">Register Agent</a>
                                </li>
                                <li>
                                    <a href="/min/Insurance.jsp">Register Insurance Company</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>All Workers Page</h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row">


                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>All Users Table</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" id="dt_example" >
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2><small></small></h2>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content"  >

                                        <table class="table table-striped responsive-utilities jambo_table" id="usertable">
                                            <thead>
                                            <tr class="headings">
                                                <th>Title</th>
                                                <th>Full Name</th>
                                                <th>Gender</th>
                                                <th>Username</th>
                                                <th>E-mail</th>
                                                <th>Mobile</th>
                                                <th>Location</th>

                                            </tr>
                                            </thead>

                                            <tbody>



                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">

                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->

                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2017 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<!-- bootstrap progress js -->
<!-- icheck -->

<script src="asset/js/custom.js"></script>

<!-- moris js-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script type="text/javascript">

    //    $(document).ready(function(){
    //    $('#mytable').dataTable({
    //        sPaginationType: 'full_numbers',
    //                bJQueryUI: true
    //    })
    //    });

    $(document).ready(function(){

        $.ajax({
            type:"GET",
            url:"/min/allUsers",
            success: function(response){
                var jsonResponse = JSON.parse(response);
                var i =0;
                while(jsonResponse.length > i){
                    var id = jsonResponse[i].id;
                    var title = jsonResponse[i].title;
                    var gender = jsonResponse[i].gender;
                    var first = jsonResponse[i].first;
                    var last = jsonResponse[i].last;
                    var username = jsonResponse[i].username;
                    // var password = jsonResponse[i].password;
                    var email = jsonResponse[i].email;
                    var phone = jsonResponse[i].phone;
                    var location = jsonResponse[i].location;

                    $('#usertable').append('<tr class="even pointer"><td>'+title+'</td><td>'+first+'</td><td>'+gender+'</td><td><a href="/min/userPage?username='+username+'\"">'+username+'</a></td><td>'+email+'</td><td>'+phone+'</td><td>'+location+'</td></tr>');

                    i++;

                }
            },
            error: function(){
                console.log("error")
            }
        })
    })

</script>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
<script>
    NProgress.done();
</script>
<!-- Datatables -->
<script src="asset/js/datatables/js/jquery.dataTables.js"></script>
<script src="asset/js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = [];
    $(document).ready(function () {
        var oTable = $('#usertable').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": ""
                //<?php echo base_url('asset/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
</body>

</html>

