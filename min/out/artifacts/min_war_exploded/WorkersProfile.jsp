<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10/10/2015
  Time: 11:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    String username = (String) session.getAttribute("User_username");
    //session.setAttribute("User_username",username);
    String userId = (String) session.getAttribute("userId");
    String relationship = (String) session.getAttribute("relationship");
    String fee_type = (String) session.getAttribute("fee_type");
    String agencyFee = (String) session.getAttribute("agency_fee");
    String salary = (String) session.getAttribute("salary");
    String religion = (String) session.getAttribute("religion");
    String total = (String) session.getAttribute("total");
    String workerId = (String) session.getAttribute("worker_id");
    String firstname = (String) session.getAttribute("first");
    String age = (String) session.getAttribute("age");
    String sex = (String) session.getAttribute("sex");
    String agent = (String) session.getAttribute("agent");
    String medical = (String) session.getAttribute("medical");
    String occupation = (String) session.getAttribute("occupation");
    String address = (String) session.getAttribute("address");
    String mobile = (String) session.getAttribute("mobile");
    String imageName = (String) session.getAttribute("userImage");
    String guanId = (String) session.getAttribute("guanId");
    String backId = (String) session.getAttribute("backId");
    String name = (String) session.getAttribute("employerName");
    String mail = (String) session.getAttribute("mail");
    String preference = (String) session.getAttribute("preference");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }
    //String medicalUrl = "/min/medicalReport?fileName="+medical;

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Maid in lagos, Driver, House Help, Nanny, Cleaner, Cook, Nigeria, Ogun, Abuja, Port Harcourt, Security, Gardener" />
    <meta name="description" content="Hire professional and vetted domestic staffs in Nigeria. Drivers in Nigeria, Nanny in Nigeria, House Help in Nigeria. Domestic staffs in Lagos and other part of Nigeria" />
    <title>Maid In Nigeria | Profile :: Nigeria </title>

    <!-- Bootstrap core CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <script src="js/custom.js"></script>

    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->
                <%--<div class="profile">
                    <div class="profile_pic">
                        <img src="asset/images/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2></h2>
                    </div>
                </div>--%>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <%--<h3>General</h3>--%>
                        <ul class="nav side-menu">

                            </li>

                            </li>

                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>



                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="admin/images/user.png" alt=""><%=username%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3> Profile</h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row" style="height: 700px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <div class="col-md-4 col-sm-4 col-xs-12 profile_left">

                                    <div class="profile_img">

                                        <!-- end of image cropping -->
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="">
                                                <img src="<%=imageUrl%>" alt="" title="<%=workerId%>">
                                            </div>

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                        <!-- end of image cropping -->

                                    </div>

                                </div>

                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <h3>Min0<%=workerId%> </h3>

                                    <ul class="list-unstyled user_data">
                                        <li ><i class="fa fa-map-marker user-profile-icon"></i> <%=address%>
                                        </li>

                                        <li >
                                            <i class="fa fa-briefcase user-profile-icon"></i> <%=occupation%>
                                        </li>

                                        <li >
                                            Age: <%=age%>
                                        </li>

                                        <li >
                                            Gender: <%=sex%>
                                        </li>

                                        <li >
                                            Relationship Status: <%=relationship%>
                                        </li>

                                        <li >
                                            Religion: <span> <%=religion%></span>
                                        </li>
                                        <li >
                                            Agency Name: <span><b><%=agent%></b></span>
                                        </li>

                                        <%
                                            if(fee_type.equals("Monthly")){
                                        %>
                                        <li >
                                            Expected Salary: <span> &#8358; <%=salary%></span><span class="badge badge-primary" style="color: #FFBF00;" title="Payable to agent monthly">Monthly</span>
                                        </li>
                                        <li >
                                            Agency Fee: <span> <%=agencyFee%> &percnt; of Expected Salary </span> <span class="badge badge-primary" style="color: #FFBF00;" title="Payable to agent monthly"><%=fee_type%></span>
                                        </li>
                                        <li >
                                            Total to Agent: <span> &#8358; <%=total%> </span> <span class="badge badge-primary" style="color: #FFBF00;" title="Payable to agent monthly"><%=fee_type%></span>
                                        </li>
                                        <%
                                            }else if(fee_type.equals("Annual")) {
                                        %>
                                        <li >
                                            Expected Salary: <span>&#8358; <%=salary%></span><span class="badge badge-primary" style="color: #FFBF00;" title="Payable to staff monthly">Monthly</span>
                                        </li>
                                        <li >
                                            Agency Fee: &#8358; <span id="feeSpan"> <%=agencyFee%></span> <span class="badge badge-primary" style="color: #FFBF00;" title="Payable to agent per annum"><%=fee_type%></span>
                                        </li>
                                        <%
                                            }
                                            /*if(medical.equals("none")){*/
                                        %>

                                        <li class="m-top-xs" >
                                        <input type="checkbox" name="medical" id="medicalCheck" onchange="addMedicals()" title="Ask for Medical Report"/>    Medical Report <div class="dropdown"><button class="fa fa-info-circle dropbtn" style="color: #FFBF00" ></button><div class="dropdown-content" style="color: #FFBF00">Accredited Medical Centers <a href="">Health Affairs</a><a href="">Beulah Health Service</a><a href="">Ojuelegba Scan Center</a><a href="">Immack Diagnostic Center</a></div> </div><span class="badge" title="Payed for by prospective employer">Optional</span>
                                        </li>
                                        <%--<%
                                        }else{
                                        %>--%>
                                        <%--<li class="m-top-xs">
                                            Medical Report<i class="fa fa-check" style="color: green" title="Available"> </i>
                                        </li>--%>
                                        <%
                                            /*}*/
                                            if(guanId != null){
                                        %>
                                        <li class="m-top-xs" >
                                            Guarantor <i class="fa fa-check" style="color: green" title="Available"></i>
                                        </li>
                                        <%
                                        }else{

                                        %>
                                        <li class="m-top-xs" >
                                            Guarantor <i class="fa fa-close" style="color: #ff0000" title="Not Available"></i>
                                        </li>
                                        <%
                                            }
                                            if(backId != null){
                                        %>
                                        <li class="m-top-xs" >
                                            Background Check <i class="fa fa-check" style="color: green" title="Available"></i>
                                        </li>
                                        <%
                                        } else{
                                        %>
                                        <li class="m-top-xs" >
                                            Background Check <i class="fa fa-close" style="color: #ff0000" title="Not Available"></i>
                                        </li>
                                        <%
                                            }
                                        %>
                                        <li>
                                            Preference: <%=preference%>
                                        </li>
                                    </ul>

                                    <a href="/min/UserHomePage.jsp" class="btn btn-danger">Cancel</a><a class="btn btn-info btn-sm" href="/min/sendMailReqWorker?agentName=<%=agent%>&workerName=<%=firstname%>&occupation=<%=occupation%>&mobile=<%=mobile%>&employerName=<%=name%>&mail=<%=mail%>&workerid=<%=workerId%>&userId=<%=userId%>&salary=<%=salary%>&agencyFee=<%=agencyFee%>"><i class="fa fa-edit m-right-xs"></i>Request</a>
                                    <br />

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">

                </div>
                <div class="col-md-6 col-sm-6">
                    <fieldset>
                        <legend>Tip:</legend>
                        <i class="fa fa-check" style="color: green"></i> Available
                        <i class="fa fa-question" style="color: #FFBF00"></i> Available but require more Info
                        <i class="fa fa-close" style="color: #ff0000"></i> Not Available
                    </fieldset>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
<%--                        <li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="http://twitter.com/maidinnigeria"></a></li>
                        <%--<li><a class="fa fa-google-plus-square" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="fa fa-linkedin-square" data-original-title="linkedin" href="#"></a></li>
                        <li><a class="fa fa-youtube-square" data-original-title="youtube" href="#"></a></li>
                        <li><a class="fa fa-vimeo-square" data-original-title="vimeo" href="#"></a></li>
                        <li><a class="fa fa-skype" data-original-title="skype" href="#"></a></li>--%>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <%--<div class="col-md-6 col-sm-6 ">
                    <div class="pre-footer-subscribe-box pull-right">
                        <h2>Newsletter</h2>
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="youremail@mail.com" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </span>
                            </div>
                        </form>
                    </div>
                </div>--%>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2015 - 2017 © Novatia Ltd. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">
                                <!-- <li><img src="/frontend/assets/img/payments/western-union.jpg" alt="We accept Western Union" title="We accept Western Union"></li>-->
                                <%--<li><img src="asset/images/american-express.png" alt="We accept American Express" title="We accept American Express"></li>
                                <li><img src="asset/images/mastercard.png" alt="We accept MasterCard" title="We accept MasterCard"></li>
                                <li><img src="asset/images/paypal2.png" alt="We accept PayPal" title="We accept PayPal"></li>
                                <li><img src="asset/images/visa.png" alt="We accept Visa" title="We accept Visa"></li>--%>
                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
                <div class="foot">
                    <p class="pull-right">MaidInNigeria. |
                    </p>
                </div>
                <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="admin/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="admin/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="admin/js/icheck/icheck.min.js"></script>

<script src="admin/js/custom.js"></script>

<!-- image cropping -->
<script src="admin/js/cropping/cropper.min.js"></script>
<script src="admin/js/cropping/main.js"></script>


<!-- daterangepicker -->
<script type="text/javascript" src="admin/js/moment.min.js"></script>
<script type="text/javascript" src="admin/js/datepicker/daterangepicker.js"></script>
<!-- moris js -->
<script src="admin/js/moris/raphael-min.js"></script>
<script src="admin/js/moris/morris.js"></script>
<script>

</script>

<script>
    NProgress.done();
</script>
<!-- /datepicker -->
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>
