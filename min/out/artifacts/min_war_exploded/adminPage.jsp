<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2/9/2016
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null){
        String state =(String) session.getAttribute("login");
        if(state==null){
            response.sendRedirect("/min/AdminLogin.jsp");
        }
    }
    else{
        response.sendRedirect("/min/AdminLogin.jsp");
    }
    String name = (String) session.getAttribute("username");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="asset/css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <link href="asset/css/floatexamples.css" rel="stylesheet" />

    <script src="asset/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">


                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <%--<h3>General</h3>--%>
                        <ul class="nav side-menu">

                        </ul>
                    </div>
                    <div class="menu_section">
                        <%--<h3>Live On</h3>--%>
                        <ul class="nav side-menu">

                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <a href="/min/index.jsp">
                    <div class="navbar-left">
                        <img src="images/MINLogo2.PNG">
                    </div></a>

                    <div class="wrapper">
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/adminPage.jsp">Dashboard</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllUsers.jsp">Users</a>
                            </li>

                            <li >
                                <a href="/min/AdminAllAgents.jsp">Agents</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllWorkers.jsp">Domestic Staffs</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllHiredStaff.jsp">Hired Staffs</a>
                            </li>

                            <li>
                                <a href="/min/AdminUnassignedWorkers.jsp">Un-Assigned Staffs</a>
                            </li>
                            <li>
                                <a href="/min/PendingHire.jsp">Pending Hires</a>
                            </li>
                            <li>
                                <a href="/min/MailAllAgents.jsp">Mail All Agents</a>
                            </li>

                            <li><a href="/min/MailAllUsers.jsp"> Mail All Users</a>


                        </ul>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="asset/images/user.png" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="/min/AgentsReg.jsp">Register Agent</a>
                                </li>
                                <li>
                                    <a href="/min/Insurance.jsp">Register Insurance Company</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

            <br />
            <div class="">

                <div class="row top_tiles">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                            </div>
                            <div class="count" id="user"></div>

                            <h3>Users Registered Today</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                            </div>
                            <div class="count" id="agentToday"></div>

                            <h3> Agents Registered Today</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                            </div>
                            <div class="count" id="workerToday"></div>

                            <h3>Registered Domestic Staff</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-comment-o"></i>
                            </div>
                            <div class="count" id="pending"></div>

                            <h3>New Pending Hires</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="count" id="hired"></div>

                            <h3>Total Hires</h3>
                            <p></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="filter">

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="col-md-9 col-sm-12 col-xs-12">

                                    <div class="tiles">
                                        <div class="col-md-4 tile">
                                            <span>Total Users</span>
                                            <h2 id="users"></h2>
                                                <span class="sparkline11 graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <span>Total Agents</span>
                                            <h2 id="agent"></h2>
                                                <span class="sparkline22 graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <span>Total Domestic Staffs</span>
                                            <h2 id="workers"></h2>
                                                <span class="sparkline11 graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                        </div><br />


                                    </div>

                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div>
                                        <div class="x_title">
                                            <h2>Query Logs</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#">Settings 1</a>
                                                        </li>
                                                        <li><a href="#">Settings 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="#"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <ul class="list-unstyled top_profiles scroll-view" id="logs">

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>







                <div class="row">

                </div>
            </div>

            <!-- footer content -->
            <footer>
                <div class="">
                    <p class="pull-right"> MaidInNigeria. |
                        <span class="lead"></span>
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="asset/js/moment.min2.js"></script>
<script type="text/javascript" src="asset/js/datepicker/daterangepicker.js"></script>
<!-- sparkline -->
<script src="asset/js/sparkline/jquery.sparkline.min.js"></script>

<script src="asset/js/custom.js"></script>

<!-- flot js -->
<!--[if lte IE 8]><script type="text/javascript" src="asset/js/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="asset/js/flot/jquery.flot.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="asset/js/flot/date.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.spline.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.stack.js"></script>
<script type="text/javascript" src="asset/js/flot/curvedLines.js"></script>
<script type="text/javascript" src="asset/js/flot/jquery.flot.resize.js"></script>

<script>
    //Load New Users
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/userToday',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].number;
                    $('#user').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load New Agents
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/agentToday',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].number;
                    $('#agentToday').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load New Workers
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/workersToday',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#workerToday').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load Pending Requests
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/totalpending',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#pending').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load total Hired
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/totalHire',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#hired').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load total users
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/totalUsers',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#users').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //load total Agents
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/totalAgents',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#agent').html(count);
                    console.log(count);
                    initial++
                }
            },
            error: function(){

            }
        });
    });

    //load total workers
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/totalWorkers',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var count = jsonResponse[initial].count;
                    $('#workers').html(count);
                    console.log(count);
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

    //logs
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:'/min/logsToday',

            success: function(response){
                var jsonResponse = JSON.parse(response);
                var initial = 0;
                while(jsonResponse.length > initial){
                    var name = jsonResponse[initial].name;
                    var query = jsonResponse[initial].data;
                    $('#logs').append('<li class="media event">' +
                    '<a class="pull-left border-aero profile_thumb">'+
                    '<i class="fa fa-user aero"></i>' +
                    '</a>' +
                    '<div class="media-body">' +
                    '<a class="title" href="#">'+name+'</a>' +
                    '<p>queried for a <strong>'+query+' </strong> </p>' +
                    '<p> <small>Today</small>' +
                    '</p>' +
                    '</div>' +
                    '</li>');
                    console.log();
                    initial++;
                }
            },
            error: function(){

            }
        });
    });

</script>
<!-- flot -->
<%--<script type="text/javascript">
    //define chart colors ( you maybe add more colors if you want or flot will add it automatic )
    var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

    //generate random number for charts
    randNum = function () {
        return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    }

    $(function () {
        var d1 = [];
        var d2 = [];

        //here we generate data for chart
        for (var i = 0; i < 30; i++) {
            d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
                d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
        }

        var chartMinDate = d1[0][0]; //first day
        var chartMaxDate = d1[20][0]; //last day

        var tickSize = [1, "day"];
        var tformat = "%d/%m/%y";

        //graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 10,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 100
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 4.5,
                    symbol: "circle",
                    lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function (label, series) {
                    // just add some space to labes
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };
        var plot = $.plot($("#placeholder33x"), [{
            label: "Progress",
            data: d1,
            lines: {
                fillColor: "rgba(150, 202, 89, 0.12)"
            }, //#96CA59 rgba(150, 202, 89, 0.42)
            points: {
                fillColor: "#fff"
            }
        }], options);
    });
</script>--%>
<!-- /flot -->
<!--  -->
<script>
    $('document').ready(function () {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
            type: 'bar',
            height: '125',
            barWidth: 13,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
            type: 'bar',
            height: '40',
            barWidth: 8,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
            type: 'line',
            height: '40',
            width: '200',
            lineColor: '#26B99A',
            fillColor: '#ffffff',
            lineWidth: 3,
            spotColor: '#34495E',
            minSpotColor: '#34495E'
        });

        var doughnutData = [
            {
                value: 30,
                color: "#455C73"
            },
            {
                value: 30,
                color: "#9B59B6"
            },
            {
                value: 60,
                color: "#BDC3C7"
            },
            {
                value: 100,
                color: "#26B99A"
            },
            {
                value: 120,
                color: "#3498DB"
            }
        ];
        var myDoughnut = new Chart(document.getElementById("canvas1i").getContext("2d")).Doughnut(doughnutData);
        var myDoughnut = new Chart(document.getElementById("canvas1i2").getContext("2d")).Doughnut(doughnutData);
        var myDoughnut = new Chart(document.getElementById("canvas1i3").getContext("2d")).Doughnut(doughnutData);
    });
</script>

</body>

</html>
