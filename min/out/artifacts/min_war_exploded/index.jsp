<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String User_username = (String) session.getAttribute("User_username");
    String imageName = (String) session.getAttribute("userImage");
    String userId = (String) session.getAttribute("userId");

    String Agent_Username = (String) session.getAttribute("Agent_username");

    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+imageName;
    }else{
        imageUrl = "asset/images/user.png";
    }

%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Maid In Nigeria | Domestic staffs, Driver, HouseHelp, Nanny, Chef, Maids in Lagos, Nigeria.</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Maid, Househelp, HouseMaid, Driver's, Nanny, chef, gardener, security, housegirl, Lagos, Nigeria" />
    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic servant to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants(Maid, Househelp, Driver, Nanny, chef, gardener, security, housegirl, butler).">
    <meta name="keywords" content="Maid in lagos, Driver, House Help, Nanny, Cleaner, Cook, Nigeria, Ogun, Abuja, Port Harcourt, Security, Gardener" />
    <meta name="description" content="Hire professional and vetted domestic staffs in Nigeria. Drivers in Nigeria, Nanny in Nigeria, House Help in Nigeria. Domestic staffs in Lagos and other part of Nigeria" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    <!-- for-gallery-rotation -->
    <script src="js/modernizr.custom.97074.js"></script>
    <!-- //for-gallery-rotation -->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Peralta' rel='stylesheet' type='text/css'>
    <!-- Custom styling plus plugins -->
<script type="text/javascript" src="js/custom.js"></script>
    <script src="js/jquery.chocolat.js"></script>
    <!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- body -->
	<div class="body-content">
		<div class="container">
			<div class="body-content1">
			<!-- header -->
				<div class="logo-search">
					<div class="logo">
						<h1><a href="index.jsp"><img src="images/MINLogo.PNG"></a></h1>
					</div>

                        <ul class="nav navbar-nav navbar-right ">
                            <li class="">

                                <%
                                    if(User_username == null &&  Agent_Username == null){
                                %>

                                <a href="#" id="login-trigger" class=" user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                   <%-- <img src="<%=imageUrl%>" alt="" width="29px" height="29px">--%>

                                    SignIn<span class=""></span>
                                    <span class=" fa fa-angle-down"></span>
                                </a>


                                <div id="login-content">
                                    <form method="POST" action="" id="loginForm" >
                                        <fieldset class="account-info">
                                            <label>
                                                Username
                                                <input type="text" name="username" placeholder="" required>
                                            </label>

                                            <label>
                                                Password
                                                <input type="password" name="password" placeholder="******" required>
                                            </label>

                                        </fieldset>
                                        <fieldset class="account-action">
                                            <input class="btn" type="submit" id="logon" value="Login" name="submit">


                                            <label>
                                                <select class="" id="role" size="1" onchange="getRole()" required>
                                                    <option value="">Choose Role</option>
                                                    <option value="1">User</option>
                                                    <option value="2">Agent</option>
                                                </select>
                                            </label>

                                            <label>
                                                <a href="/min/reset.jsp">Forgot Password</a>
                                            </label>
                                        </fieldset>
                                    </form>
                                </div>

                                <a href="#" id="signin-trigger" class=" user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                    <%-- <img src="<%=imageUrl%>" alt="" width="29px" height="29px">--%>

                                    Register
                                    <span class=" fa fa-angle-down"></span>
                                </a>

                                <div class="" id="signin-content">
                                    <form method="POST" action="/min/registernewuser" id="singUpForm">
                                        <fieldset class="account-info-signup">
                                            <label>
                                                Name
                                                <input type="text" name="fullname" placeholder="FirstName         LastName" required>
                                            </label>
                                            <label>
                                                Email
                                                <input type="email" name="e_mail" placeholder="example@me.com" required>
                                            </label>
                                            <label>
                                                Mobile
                                                <input type="text" id="phoneno" onblur="phonenumber()" name="phoneno" required placeholder="2348012345678">
                                            </label>
                                            <label>
                                                Username
                                                <input type="text" name="username" placeholder="" required>
                                            </label>
                                            <label>
                                                Password
                                                <input type="password" name="password" placeholder="******" required>
                                            </label>

                                        </fieldset>
                                        <fieldset class="account-action">

                                            <input type="submit" value="Register" id="signUp" class="btn">
                                        </fieldset>
                                    </form>
                                </div>

                                <%
                                    }else if(User_username != null){
                                %>
                                <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                    <img src="<%=imageUrl%>" alt="" width="29px" height="29px"><span class="badge badge-primary"> <%=User_username%></span>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="/min/Requests.jsp">Requests</a>
                                    </li>
                                    <li>
                                    <a href="/min/UserHomePage.jsp">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="/min/Hired.jsp">Hires</a>
                                    </li>

                                    <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                                <%
                                    }else if(Agent_Username != null){
                                %>
                                <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                    <img src="<%=imageUrl%>" alt="" width="29px" height="29px"><span class="badge badge-primary"><%=Agent_Username%></span>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="/min/AgentHomePage.jsp">Profile</a>
                                    </li>
                                    <li><a href="/min/AddWorker.jsp">Add Staffs</a>
                                    </li>
                                    <li><a href="/min/AllWorkers.jsp">All Staffs</a>
                                    </li>
                                    <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                                <%
                                    }
                                %>
                            </li>
                        </ul>


					<div class="search">

                            <form action="/min/search" method="get" id="form1">
                                <input type="hidden" value="<%=userId%>">
							    <input type="text" value="Search Here...e.g Driver / House Help in lagos" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search Here...';}" name="search" id="search" required="">
							    <input type="submit" value=" " id="go">
						    </form>
					</div>
					<div class="clearfix"> </div>
				</div>
			<!-- //header -->
			<!-- nav -->
				<div class="navigation">
					<nav class="navbar navbar-default">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
							<nav class="stroke">
								<ul class="nav navbar-nav">
									<li class="active"><a href="index.jsp">HOME</a></li>
                                    <li><a href="/min/AgentsReg.jsp" class="hvr-rectangle-out">REGISTER AS AGENT</a></li>
                                    <li><a href="/min/about.jsp" class="hvr-rectangle-out">ABOUT</a></li>
                                    <li><a href="/min/mail.jsp" class="hvr-rectangle-out">MAIL US</a></li>
                                    <%--<li><a href="#" class="hvr-rectangle-out">HOW TO USE</a></li>--%>
                                    <li><a href="/min/WorkersReg.jsp" class="hvr-rectangle-out">Domestic Staff? <span>Register Here</span></a></li>
								</ul>
							</nav>
						</div>
						<!-- /.navbar-collapse -->
					</nav>
                    <div  id="searchResult" style="background: #ffffff;">


                    </div>
				</div>
			<!-- //nav -->

			<!-- banner -->
				<div class="banner">
					<div class="banner-grids">
						<div class="banner-left">
							<div class="banner-left1">
								<div class="banner-left1-grid">
									<img src="images/thehelp.jpeg" alt=" " class="img-responsive" />
									<div class="banner-info">
										<a class="" href=""><i></i></a>
										<h3>
											<a >
                                                Nanny
											</a>
										</h3>
										<div class="event-meta">
											<h4></h4>
											<p></p>
										</div>
									</div>
								</div>
								<div class="banner-left1-grid">
									<img src="images/AfricanAmericanDriver.jpg" alt=" " class="img-responsive" />
									<div class="banner-info">
										<a class="" href=""><i></i></a>
										<h3>
											<a >
												Driver
											</a>
										</h3>
										<div class="event-meta">
											<h4></h4>
											<p></p>
										</div>
									</div>
								</div>
							</div>
							<div class="banner-left2">
								<div class="banner-left1-grid">
									<img src="images/A-good-cook.jpg" alt=" " class="img-responsive" />
									<div class="banner-info">
										<a class="" ><i></i></a>
										<h3>
											<a >
												Cook
											</a>
										</h3>
										<div class="event-meta">
											<h4></h4>
											<p></p>
										</div>
									</div>
								</div>
								<div class="banner-left1-grid">
									<img src="images/dish-washing1.jpg" alt=" " class="img-responsive" />
									<div class="banner-info">
										<a class="" ><i></i></a>
										<h3>
											<a>
												House Maid
                                            </a>
										</h3>
										<div class="event-meta">
											<h4></h4>
											<p></p>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="banner-right">
							<section class="slider">
								<div class="flexslider">
									<ul class="slides">
										<li>
											<div class="services-grid-right-grid1">

											</div>
										</li>
										<li>
											<div class="services-grid-right-grid2">

											</div>
										</li>
										<li>
											<div class="services-grid-right-grid3">

											</div>
										</li>
                                        <li>
                                            <div class="services-grid-right-grid4">

                                            </div>
                                        </li>
									</ul>
								</div>
							</section>
									<!--FlexSlider-->
									<script defer src="js/jquery.flexslider.js"></script>
									<script type="text/javascript">
										$(window).load(function(){
										  $('.flexslider').flexslider({
											animation: "slide",
											start: function(slider){
											  $('body').removeClass('loading');
											}
										  });
										});
									</script>
									<!--End-slider-script-->
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<!-- //banner -->
                <!-- gallery -->
                <div class="gallery">
                    <h2>Gallery</h2>
                    <div class="gallery-grids">
                        <section>
                            <ul id="da-thumbs" class="da-thumbs">
                                <li>
                                    <a href="images/Gardener.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/Gardener.jpg" alt="" />
                                        <div>
                                            <h5>Gardener</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="images/laundry_basket.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/laundry_basket.jpg" alt="" />
                                        <div>
                                            <h5>Laundry</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="images/driver-rome1.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/driver-rome1.jpg" alt="" />
                                        <div>
                                            <h5>Driver</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="images/images.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/images.jpg" alt="" />
                                        <div>
                                            <h5>Cleaner</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="images/maid-cooking.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/maid-cooking1.jpg" alt="" />
                                        <div>
                                            <h5>Cook</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="images/floor-cleaning.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                                        <img src="images/floor-cleaning.jpg" alt="" />
                                        <div>
                                            <h5>HouseHelp</h5>
                                            <span></span>
                                        </div>
                                    </a>
                                </li>

                                <div class="clearfix"> </div>
                            </ul>
                        </section>
                    </div>
                    <!-- pop-up-script -->

                    <!--light-box-files -->
                    <script type="text/javascript" >
                        $(function() {
                            $('.gallery a').Chocolat();
                        });
                    </script>
                    <!-- //pop-up-script -->
                    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
                    <script type="text/javascript">
                        $(function() {

                            $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

                        });
                    </script>
                </div>
                <!-- //gallery -->
			<!-- banner-bottom -->
				<div class="banner-bottom">
					<div class="banner-bottom-grids">
						<div class="col-md-4 banner-bottom-grid">
							<h3>Maid <span>In Nigeria</span></h3>
							<h2><i>" Maid in Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace."</i></h2>
							<p>Due to the busy lifestyle of the average Nigerian, we are increasingly dependent on a little extra support
                                to ensure we can continue to grow our careers, while being able to support our families.
								<span>While we do not provide the domestic staff's ourselves,
                                    we provide the platform through which vetted domestic staff
                                    can be sourced.</span>
                                We highly recommend that anyone sourcing domestic staff through our portal ensures that
                                all the information provided for the said domestic staff are checked out.
                            </p>
							<div class="more">
								<a href="/min/about.jsp" class="hvr-rectangle-out">Read More...</a>
							</div>
							
						</div>
						<div class="col-md-4 banner-bottom-grid">
							<h3>Services <span></span></h3>
							<div class="" id="service">
								<script>
									$(document).ready(function () {
										size_li = $("#myList li").size();
										x=1;
										$('#myList li:lt('+x+')').show();
										$('#loadMore').click(function () {
											x= (x+1 <= size_li) ? x+1 : size_li;
											$('#myList li:lt('+x+')').show();
										});
										$('#showLess').click(function () {
											x=(x-1<0) ? 1 : x-1;
											$('#myList li').not(':lt('+x+')').hide();
										});
									});
								</script>
								<ul id="myList">
									<li>
										<div class="l_g">
											<div class="banner-bottom-grid1">
												<div class="col-xs-4 banner-bottom-grid-left">
													<img src="icons/reference.jpg" alt=" " class="img-responsive" />
												</div>
												<div class="col-xs-8 banner-bottom-grid-left">
													<h4><a >Bio-Data</a></h4>
												</div>
												<div class="clearfix"> </div>
											</div>
											<div class="banner-bottom-grid1">
												<div class="col-xs-4 banner-bottom-grid-left">
													<img src="icons/medical.jpg" alt=" " class="img-responsive" />
												</div>
												<div class="col-xs-8 banner-bottom-grid-left">
													<h4><a>Medical Report</a></h4>
												</div>
												<div class="clearfix"> </div>
											</div>
											<div class="banner-bottom-grid1">
												<div class="col-xs-4 banner-bottom-grid-left">
													<img src="icons/guarantor.jpg" alt=" " class="img-responsive" />
												</div>
												<div class="col-xs-8 banner-bottom-grid-left">
													<h4><a >Guarantor</a></h4>
												</div>
												<div class="clearfix"> </div>
											</div>
											<div class="banner-bottom-grid1">
												<div class="col-xs-4 banner-bottom-grid-left">
													<img src="icons/reference.jpg" alt=" " class="img-responsive" />
												</div>
												<div class="col-xs-8 banner-bottom-grid-left">
													<h4><a >Reference</a></h4>
												</div>
												<div class="clearfix"> </div>
											</div>
											
											
										</div>
									</li>
									
								</ul>
									
							</div>
						</div>
						<div class="col-md-4 banner-bottom-grid">
							<h3>What's <span>New</span></h3>
							<div class="banner-bottom-grid-fig">
								<div class="banner-bottom-grid-fig1">
									<img src="images/ad.png" alt=" " class="img-responsive" />
									<div class="banner-bottom-grid-fig1-pos">
										<a href="/min/mail.jsp">Advertise Here</a>
										<p><%--<i class="glyphicon glyphicon-time" aria-hidden="true"></i>--%> </p>
									</div>
								</div>
								<div class="banner-bottom-grid-fig-grid">
									<div class="banner-bottom-grid-fig-grid1">
										<h4><span>Testimony-1</span><a >Great, Hired my driver from this site.</a></h4>
										<p><i class="glyphicon glyphicon-time" aria-hidden="true"></i>March 25, 2016 By <a >Oyemade</a></p>
									</div>
								</div>
								<div class="banner-bottom-grid-fig-grid">
									<div class="banner-bottom-grid-fig-grid1">
										<h4><span>Testimony-2</span><a >This actually made it easy for me to get a well trained nanny for my 1yr old.</a></h4>
										<p><i class="glyphicon glyphicon-time" aria-hidden="true"></i>March 28, 2016 By <a >Kehinde Wunmi</a></p>
									</div>
								</div>
								<div class="banner-bottom-grid-fig-grid">
									<div class="banner-bottom-grid-fig-grid1">
										<h4><span>Testimony-3</span><a >I got two maids from this website.....Nice!</a></h4>
										<p><i class="glyphicon glyphicon-time" aria-hidden="true"></i>March 30, 2016 By <a >Mrs Uche</a></p>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>

			<!-- //banner-bottom -->
			</div>
		</div>
	</div>
<!-- //body -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			
			<div class="footer-grids">
				<div class="col-md-3 footer-grid">
					<p>Novatia LTD
						147A, Ogunlana Drive
						Surulere, Lagos.
					<a href="mailto:contact@maidinnigeria.com.ng">contact@maidinnigeria.com.ng</a>
					<p>Phone : +234 702-614-2888</p>
				</div>
				<div class="col-md-3 footer-grid">
					<ul>
						<li><a href="/min/mail.jsp">Contact</a></li>
						<li><a href="#service">Service</a></li>
						<li><a href="/min/index.jsp">Home</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid">
					<ul>
						<li><a href=""></a></li>
						<li><a href=""></a></li>
						<li><a href="index.jsp"></a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid">
					<div class="footer-grid-left">
						<a href=""><img src="images/floor-cleaning.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="footer-grid-left">
						<a href=""><img src="images/images.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="footer-grid-left">
						<a href=""><img src="images/maid-cooking.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="footer-grid-left">
						<a href=""><img src="images/rs.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="footer-grid-left">
						<a href=""><img src="images/floor-cleaning.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="footer-grid-left">
						<a href=""><img src="images/rs.jpg" alt=" " class="img-responsve" /></a>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-copy">
				<p>&copy 2017 MaidInNigeria. All rights reserved.</p>
				<ul>
					<li><a href="http://twitter.com/maidinnigeria" class="twitter"><span type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Follow Us On Twitter"></span></a></li>

					<li><a href="https://www.facebook.com/maidinnigeria" class="facebook" id="facebook"><span type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Follow Us On Facebook"></span></a></li>

					<div class="clearfix"> </div>
				</ul>

			</div>
		</div>
	</div>
<!-- //footer -->

<!-- moris js -->
<script>
    jQuery(document).ready(function(){
        //var hook = false;
        $('#loginForm').submit(function(e){
            e.preventDefault();
            return false;
        });

        $('#logon').click( function(){

            var loginform = $('#loginForm');
            formData = loginform.serialize();

            $('.error').remove();
           $('#logon').ajaxStart(function(){
               $(this).attr('disabled',true)
           })
                   .ajaxStop(function(){
                      $(this).attr('disabled',false)
                   });
            //formData = "?username=" + userName + "&password="+ passWord;
            $.ajax({

                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formData,
                //dataType:"json",

                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        //displaySuccessMessage("Welcome");
                        window.location = "/min/index.jsp";
                    }
                    else{
                        //displayErrorDialog("Wrong Username/Password Combination");
                        $('#loginForm').prepend('<span style="color: #ff0000; padding: 5px;" class="error" >wrong username/password combination..</span>');
                        $('#username').val('');
                        $('#password').val('');
                    }
                    //console.log(jsonResponse)
//                    $('#logon').attr('disabled',false)
                },
                error: function (jqXHR,textStatus, errorThrown) {
                    $('#loginForm').prepend('<span style="color: #ff0000; padding: 5px;" class="error" >wrong username/password combination..</span>');
                    console.log(jqXHR.statusText);
//                    $('#logon').attr('disabled',false)
                }

            });
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#singUpForm').submit(function(e){
            e.preventDefault();
        });

        $('#signUp').click(function(){
            var formSignup = $('#singUpForm');
            formData = formSignup.serialize();

            console.log(formSignup.attr('method'));
            console.log(formSignup.attr('action'));
            $.ajax({
                type:formSignup.attr('method'),
                url:formSignup.attr('action'),
                data:formData,

                success: function (response){
                    console.log("got into the success");
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        window.location = "/min/index.jsp";
                    }else {
                        $('#signUpForm').prepend('<span>Error Creating Profile</span>')
                    }
//                    $('#signUp').attr('disabled',false)
                },
                error: function (jqXHR,textStatus, errorThrown) {
//                    $('#signUp').attr('disabled',false)
                }
            });
        });
    });
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
									});
	</script>
<!-- //here ends scrolling icon -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<script>
    $(document).ready(function(){
        $('#login-trigger').click(function(){
            $('#signin-trigger').removeClass('active');
            $('#signin-content').slideUp();
            $(this).next('#login-content').slideToggle();
            $(this).toggleClass('active');

            /*if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
            else $(this).find('span').html('&#x25BC;')*/
        })
    });

    $(document).ready(function(){
        $('#signin-trigger').click(function(){
            $('#login-trigger').removeClass('active');
            $('#login-content').slideUp();
            $(this).next('#signin-content').slideToggle();
            $(this).toggleClass('active');

           /* if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
             else $(this).find('span').html('&#x25BC;')*/
        })
    });
</script>

<%--
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>
--%>
<!-- //for bootstrap working -->
<script>

    $(document).ready(function(){

        $('#form1').submit(function(e){
            e.preventDefault();
        });

        $('#go').click(function(){


            var initia = 0;
            var loginform = $('#form1');
            formData = loginform.serialize();
            $('#search').attr('disabled',true)
            $('#go').attr('disabled',true)

            $.ajax({
                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formData,

                success: function(response){

                    var jsonResponse = JSON.parse(response);

                    $('.banner').remove();
                    $('.banner-bottom').remove();
                    $('.gallery').remove();
                    $('#searchResult').empty();

                    while(jsonResponse.length > initia) {
                        var firstname = jsonResponse[initia].first;

                        var age = jsonResponse[initia].age;
                        var sex = jsonResponse[initia].sex;
                        var work = jsonResponse[initia].work;
                        //var image = jsonResponse[initia].image;
                        var idno = jsonResponse[initia].id;
                        var location = jsonResponse[initia].location;
                        var medical = jsonResponse[initia].medical;
                        var guarantor = 'Guarantor ' + '<i class="glyphicon glyphicon-stop" style="color: red" title=" Not Available"></i>';
                        var agencyFee = jsonResponse[initia].agencyFee;
                        var expectedSal = jsonResponse[initia].expectedSal;
                        var name = firstname ;
                        var type = jsonResponse[initia].type;
                        var request = '/min/workerProfile?name='+idno+'\"';

                        if(type == 'Monthly'){
                            agencyFee = agencyFee+ '&percnt; of staff salary'
                        }else if(type == 'Annual'){
                            agencyFee = '&#8358;'+ agencyFee
                        }

                        if(medical == 'none'){
                            medical = 'Medical Report ' + '<i class="glyphicon glyphicon-stop" style="color: #ff0000" title="Not Available"></i>'
                        }else{
                            medical = 'Medical Report ' + '<i class="glyphicon glyphicon-stop" style="color: green" title="Available"></i>'
                        }

                        $('#searchResult').append('<div class="col-md-7 col-sm-8 col-xs-10 animated fadeInDown" style="padding-top:5%;padding-bottom:5%;">' +
                        '<div class="well profile_view">' +
                        '<div class="col-sm-12">' +
                        '<h4 class="brief"><i>MaidInNigeria</i></h4>' +
                        '<div class="left col-xs-7 " >' +
                        '<form >' +
                        '<h5><span id="name"></span> MIN 0' +idno + '</h5>' +

                        '<strong></strong> <span >' + work + '</span>' +
                        '<ul class="list-unstyled ">' +
                        '<li><span>  ' + location + '</span></li>' +
                        '<li></i><span >  ' + sex + ',</span><span> '+age+' </span></li>' +
                        '<li></i><span >  ' +  guarantor + '</span></li>' +
                        '<li></i><span >  ' + medical + '</span></li>' +
                        '<li>Expected Salary: <span > &#8358; '+expectedSal +'</span></li>' +
                        '<li>Agency Fee: <span >  '+agencyFee +'</span></li>' +
                        '</ul>' +
                        '</form>' +
                        '</div>' +
                        '<div class="right col-xs-5 text-center">' +
                        '<img src="asset/images/user.png" alt="' + work + '\"" class="img-circle img-responsive" title="">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-12 bottom text-center">' +
                        '<div class="col-xs-12 col-sm-6 emphasis">' +
                        '<!--   <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">' +
                        '<!--</i> <i class="fa fa-comments-o"></i> </button>-->' +
                        '<a class="request"  href="' + request + '\"" >  <button type="button" data-toggle="tooltip" data-placement="bottom" title="Review Worker" class="btn btn-primary btn-xs" id="requestBtn"> <i class="fa fa-user">' +
                        '</i> Review </button></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');

                        initia++

                    }
                    if(jsonResponse.length == 0){
                        $('#searchResult').append("No result meet your search criteria.. Try phrase like 'Driver in lagos' \n Or 'House Help in abuja'");
                    }
                    $('#go').attr('disabled',false);
                    $('#search').attr('disabled',false);

                },
                error: function(){
                    $('#searchResult').append("Could not find result for your search..");
                    $('#go').attr('disabled',false);
                    $('#search').attr('disabled',false);
                }
            })
        })
    })

</script>
</body>
</html>