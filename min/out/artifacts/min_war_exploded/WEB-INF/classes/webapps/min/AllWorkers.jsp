<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10/12/2015
  Time: 1:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    //confirm authentication
    if(session!=null) {
        String state = (String) session.getAttribute("login");
        if (state == null) {
            response.sendRedirect("/min/AgentsLogin.jsp");
        }
    }
    else{ response.sendRedirect("/min/AgentsLogin.jsp");}

    String name = (String) session.getAttribute("Agent_username");
    String company = (String) session.getAttribute("company");
    String editUrl = "/min/editAgentAction?username="+ name ;
    String idUrl = "/min/getWorkerById?id=";
    String imageName = (String) session.getAttribute("agentImage");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }
%>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | Maids, Driver, Nanny, Butler, Security, Gardener, Housegirl, Houseboy</title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_page.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table_jui.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables_themeroller.css" rel="stylesheet">
    <link href="asset/js/jquery-ui-1.11.4/jquery-ui.css" media="all" rel="stylesheet" type="text/css">
    <script src="asset/js/datatables/js/jquery.dataTables.js"></script>

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">

    <script src="asset/js/datatables/js/jquery.js"></script>
    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />
                <%--<div class="profile_info">


                </div>--%>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <%--<h3 class=""></h3>--%>
                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <div class="wrapper">
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/AllWorkers.jsp">Dashboard</a>
                            </li>

                            <li >
                                <a href="/min/AddWorker.jsp">Add Staff</a>
                            </li>
                            <li >
                                <a href="/min/AddGuarantor.jsp">Add Guarantor</a>
                            </li>

                            <li >
                                <a href="/min/BackgroundCheck.jsp">Background Check</a>
                            </li>

                            <li>
                                <a href="/min/mail.jsp">Contact Us</a>
                            </li>

                            <li>
                                <a href="/min/about.jsp">About Us</a>
                            </li>

                        </ul>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="<%=editUrl%>">Add Logo</a>
                                </li>
                                <li>
                                    <a href="/min/AgentHomePage.jsp">Profile</a>
                                </li>
                                <li>
                                    <a href="#">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3></h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="height:700px;">
                            <div class="x_title">
                                <h2>All Domestic Staff Table</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" id="dt_example" >
                                <div class="x_panel">

                                    <div class="x_content"  >

                                        <table class=" table table-striped" id="mytable">

                                            <tr>
                                                <th>#</th>
                                                <th>Full Name</th>
                                                <th>Age</th>
                                                <th>Occupation</th>
                                                <th>Location</th>
                                                <th>Mobile</th>
                                                <th>Gender</th>
                                                <th>Employment Status</th>
                                                <th>Date Hired</th>
                                                <th>Employer</th>

                                            </tr>


                                            <tbody>



                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>

                            <div class="ln_solid"></div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria"></a></li>

                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->

                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. ALL Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
                <div class="foot">
                    <p class="pull-right">MaidInNigeria. |
                    </p>
                </div>
                <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<!-- bootstrap progress js -->
<!-- icheck -->

<script src="asset/js/custom.js"></script>

<!-- moris js-->
<!-- PNotify -->
<script type="text/javascript" src="asset/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.nonblock.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script type="text/javascript">

//    $(document).ready(function(){
//    $('#mytable').dataTable({
//        sPaginationType: 'full_numbers',
//                bJQueryUI: true
//    })
//    });

    $(document).ready(function(){
        var i =0;

        $.ajax({
            type:"POST",
            url:"/min/getAgentsWorkers?agentCompName=<%=company%>",
            success: function(response){
                var jsonResponse = JSON.parse(response);

                while(jsonResponse.length > i){
                    var id = jsonResponse[i].id;
                    var first = jsonResponse[i].first;
                    var age = jsonResponse[i].age;
                    var occupation = jsonResponse[i].occupation;
                    var location = jsonResponse[i].location;
                    var phone = jsonResponse[i].phone;
                    var employed = jsonResponse[i].employed;
                    var date = jsonResponse[i].date;
                    var employer = jsonResponse[i].employer;
                    var sex = jsonResponse[i].sex;

                 $('#mytable').append('<tr> <td scope="row"><a href="/min/getWorkerById?id='+id+'\"">'+id+'</a></td><td>'+first+'</td><td>'+age+'</td><td>'+occupation+'</td><td>'+location+'</td><td>'+phone+'</td><td>'+sex+'</td><td>'+employed+'</td><td>'+date+'</td><td>'+employer+'</td></tr>');

                    i++;

                }
            },
            error: function(){
                console.log("error")

            }
        })
    })

</script>

<script type="text/javascript">
    var permanotice, tooltip, _alert;
    $(function () {
        new PNotify({
            title: "TIPS",
            type: "info",
            text: "Agents whose wards are being requested should confirm and re-confirm appointments with clients to avoid misunderstanding.",
            nonblock: {
                nonblock: true
            },
            before_close: function (PNotify) {
                // You can access the notice's options with this. It is read only.
                //PNotify.options.text="This is another";

                // You can change the notice's options after the timer like this:
                PNotify.update({
                    title: PNotify.options.title + " - Enjoy your Stay",
                    before_close: '1000'
                });
                PNotify.queueRemove();
                return false;
            }
        });

    });
</script>

<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>

<script>
    NProgress.done();
</script>
</body>

</html>


