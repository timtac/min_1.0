<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/9/2016
  Time: 2:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String employed = (String) session.getAttribute("employed");
    String username = (String) session.getAttribute("User_username");
    String userId = (String) session.getAttribute("userId");
    String workerId = (String) session.getAttribute("workerId");
    String firstname = (String) session.getAttribute("fullname");
    String age = (String) session.getAttribute("age");
    String sex = (String) session.getAttribute("sex");
    String agent = (String) session.getAttribute("agent");

    String occupation = (String) session.getAttribute("occupation");
    String mobile = (String) session.getAttribute("mobile");
    String imageName = (String) session.getAttribute("userImage");
    String name = (String) session.getAttribute("employerName");
    String mail = (String) session.getAttribute("mail");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">


    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->
                <%--<div class="profile">
                    <div class="profile_pic">
                        <img src="asset/images/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2></h2>
                    </div>
                </div>--%>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <%--<h3>General</h3>--%>
                        <ul class="nav side-menu">

                            </li>

                            </li>

                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                <div class="wrapper">
                    <ul class="nav navbar-nav navbar-left menu">
                        <li >
                            <a href="/min/UserHomePage.jsp">Dashboard</a>
                        </li>
                        <li >
                            <a href="/min/Requests.jsp">Requests</a>
                        </li>
                        <li >
                            <a href="/min/Hired.jsp">Hires</a>
                        </li>
                        <li >
                            <a href="/min/Disengaged.jsp">Disengaged</a>
                        </li>

                        <li >
                            <a href="/min/index.jsp">Home Page</a>
                        </li>

                        <li>
                            <a href="/min/mail.jsp">Contact Us</a>
                        </li>

                    </ul>
                </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="admin/images/user.png" alt=""><%=username%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3> Profile</h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row" style="height: 700px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <div class="col-md-5 col-sm-4 col-xs-12 profile_left">

                                    <div class="profile_img">

                                        <!-- end of image cropping -->
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="<%=workerId%>">
                                                <img src="<%=imageUrl%>" alt="Domestic-staff" >
                                            </div>

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                        <!-- end of image cropping -->

                                    </div>

                                    <div class="col-md-5 col-sm-5 col-xs-12" id="block">
                                    <h3>Min0<%=workerId%> </h3>

                                        <ul class="list-unstyled user_data">



                                            <li>
                                                <i class="fa fa-briefcase user-profile-icon"></i> <%=occupation%>
                                            </li>

                                            <li>
                                                <i class="fa fa-child user-profile-icon"></i> <%=age%>
                                            </li>

                                            <li>
                                                <i class="fa fa-user user-profile-icon"></i> <%=sex%>
                                            </li>

                                            <li>
                                                <i class="fa fa-briefcase user-profile-icon"></i><a href=""> <%=agent%></a>
                                            </li>


                                        </ul>
                                            <input type="hidden" name="employed" id="employed" value="<%=employed%>">
                                        <a class="btn btn-success" id="link" href="/min/sendMailReqWorker?agentName=<%=agent%>&workerName=<%=firstname%>&occupation=<%=occupation%>&mobile=<%=mobile%>&employerName=<%=name%>&mail=<%=mail%>&workerid=<%=workerId%>&userId=<%=userId%>"><i class="fa fa-edit m-right-xs"></i>Request</a>
                                        <br />

                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--                        <li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="#"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="#"></a></li>
                        <%--<li><a class="fa fa-google-plus-square" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="fa fa-linkedin-square" data-original-title="linkedin" href="#"></a></li>
                        <li><a class="fa fa-youtube-square" data-original-title="youtube" href="#"></a></li>
                        <li><a class="fa fa-vimeo-square" data-original-title="vimeo" href="#"></a></li>
                        <li><a class="fa fa-skype" data-original-title="skype" href="#"></a></li>--%>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <%--<div class="col-md-6 col-sm-6 ">
                    <div class="pre-footer-subscribe-box pull-right">
                        <h2>Newsletter</h2>
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="youremail@mail.com" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </span>
                            </div>
                        </form>
                    </div>
                </div>--%>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © Novatia Ltd. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">
                                <!-- <li><img src="/frontend/assets/img/payments/western-union.jpg" alt="We accept Western Union" title="We accept Western Union"></li>-->
                                <%--<li><img src="asset/images/american-express.png" alt="We accept American Express" title="We accept American Express"></li>
                                <li><img src="asset/images/mastercard.png" alt="We accept MasterCard" title="We accept MasterCard"></li>
                                <li><img src="asset/images/paypal2.png" alt="We accept PayPal" title="We accept PayPal"></li>
                                <li><img src="asset/images/visa.png" alt="We accept Visa" title="We accept Visa"></li>--%>
                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="admin/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="admin/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="admin/js/icheck/icheck.min.js"></script>

<script src="admin/js/custom.js"></script>

<!-- image cropping -->
<script src="admin/js/cropping/cropper.min.js"></script>
<script src="admin/js/cropping/main.js"></script>


<!-- daterangepicker -->
<script type="text/javascript" src="admin/js/moment.min.js"></script>
<script type="text/javascript" src="admin/js/datepicker/daterangepicker.js"></script>
<!-- moris js -->
<script src="admin/js/moris/raphael-min.js"></script>
<script src="admin/js/moris/morris.js"></script>
<script>

</script>
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function(){
        var employed = $('#employed').val();
        if (employed == "employed"){
            $('#link').attr('disabled',true);
            $('#block').append('<span class="badge" style="color: #ff0000">* Sorry this domestic staff has been engaged by another employer</span>')
        }else if(employed == "Unemployed"){
            $('#link').attr('disabled',false)
        }
    })
</script>
<script>
    NProgress.done();
</script>
<!-- /datepicker -->
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>
