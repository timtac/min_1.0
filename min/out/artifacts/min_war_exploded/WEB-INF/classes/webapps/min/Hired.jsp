<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/7/2016
  Time: 8:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
     if(session != null){
         String status = (String) session.getAttribute("login");
         if(status == null){
             response.sendRedirect("/min/UserLogin.jsp");
         }
     }
     else{response.sendRedirect("/min/UserLogin.jsp");}

    String username = (String) session.getAttribute("User_username");
    String userfullname = (String) session.getAttribute("Userfullname");
    String userId = (String) session.getAttribute("userId");
    String mobile = (String) session.getAttribute("mobile");

    session.setAttribute("employerName",userfullname);
    String mail = (String) session.getAttribute("mail");
    String imageName = (String) session.getAttribute("userImage");
    String location = (String) session.getAttribute("location");

    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+imageName;
    }else{
        imageUrl = "asset/images/user.png";
    }
    String editUrl = "/min/editUserAction?username="+ username;
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MaidInNigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">

    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <%--<div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />


                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                       <!-- <h3>GENERAL</h3>-->

                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>

            </div>
        </div>--%>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <div class="wrapper">
                    <ul class="nav navbar-nav navbar-left menu">
                        <li >
                            <a href="/min/UserHomePage.jsp">Dashboard</a>
                        </li>
                        <li >
                            <a href="/min/Requests.jsp">Requests</a>
                        </li>
                        <li >
                            <a href="/min/Hired.jsp">Hires</a>
                        </li>
                        <li >
                            <a href="/min/Disengaged.jsp">Disengaged</a>
                        </li>

                        <li >
                            <a href="/min/index.jsp">Home Page</a>
                        </li>

                        <li>
                            <a href="/min/mail.jsp">Contact Us</a>
                        </li>


                    </ul>
                    </div>

                    <ul class="nav navbar-nav navbar-right ">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=username%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="<%=editUrl%>"> Update Profile</a>
                                </li>
                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <!--    <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span></span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                            <a>
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>-->

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="" >
                <div class="page-title">
                    <%--<div class="title_left">
                        <h3></h3>
                    </div>--%>


                    <%--<div class="" style="padding-left: 40%;">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group top_search" >
                            <label class="control-label"></label>
                            <form action="/min/search" method="get" id="form1">

                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control col-md-8" id="search" name="search" title="Enter Search Criteria Here"  placeholder="Search Domestic Worker" >
                                       <span class="input-group-btn">
                               <button class="btn btn-default" type="submit" id="go"  data-placement="bottom" data-toggle="tooltip" title="Search">Go!</button>

                           </span>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>--%>
                </div>
                <div class="clearfix"></div>

                <div class="row" style="height: 700px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">

                                <div class="col-md-4 col-sm-4 col-xs-12 profile_left" >

                                    <div class="profile_img">

                                        <!-- end of image cropping -->
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="Change the avatar">
                                                <img src="<%=imageUrl%>" title="<%=userfullname%>">
                                            </div>

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                        <!-- end of image cropping -->

                                    </div>

                                    <h3></h3>
                                    <div >
                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa user-profile-icon"></i>
                                            </li>

                                            <li>
                                                <i class="fa fa-user"></i>  <%=userfullname%>
                                            </li>

                                            <li>
                                                <i class="fa fa-map-marker"></i>   <%=location%>
                                            </li>
                                            <li >
                                                <i class="fa fa-envelope"></i>  <%=mail%>
                                            </li>
                                            <li >
                                                <i class="fa fa-phone"></i>  <%=mobile%>
                                            </li>
                                        </ul>
                                        <a class="btn btn-success" href="/min/editProfile?username=<%=userId%>"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                        <br />
                                    </div>
                                    <!-- start skills -->

                                    <!-- end of skills-->

                                </div>


                                <div class="col-md-8 col-sm-9 col-xs-12" id="searchResult">

                                    <div class="" >
                                        <div class="x_panel tile">
                                            <div class="x_title  ">
                                                <h2>Recent Hires</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content" id="hireBlock" >

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">

                </div>
                <div class="col-md-6 col-sm-6">
                    <fieldset>
                        <legend>Tip:</legend>
                        <i class="fa fa-check" style="color: green"></i> Available
                        <i class="fa fa-question" style="color: orange"></i> Available but Some Negative Info
                        <i class="fa fa-close" style="color: #ff0000"></i> Not Available
                    </fieldset>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria" title="Like Us On Facebook"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria" title="Like Us On Twitter"></a></li>
                        <%--<li><a class="fa fa-google-plus-square" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="fa fa-linkedin-square" data-original-title="linkedin" href="#"></a></li>
                        <li><a class="fa fa-youtube-square" data-original-title="youtube" href="#"></a></li>
                        <li><a class="fa fa-vimeo-square" data-original-title="vimeo" href="#"></a></li>
                        <li><a class="fa fa-skype" data-original-title="skype" href="#"></a></li>--%>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <%--<div class="col-md-6 col-sm-6 ">

                    <div class="pre-footer-subscribe-box pull-right">
                        <h2>Newsletter</h2>
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="youremail@mail.com" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </span>
                            </div>
                        </form>
                    </div>
                </div>--%>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">

                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">
                                <!-- <li><img src="/frontend/assets/img/payments/western-union.jpg" alt="We accept Western Union" title="We accept Western Union"></li>-->
                                <%--<li><img src="asset/images/american-express.png" alt="We accept American Express" title="We accept American Express"></li>
                                <li><img src="asset/images/mastercard.png" alt="We accept MasterCard" title="We accept MasterCard"></li>
                                <li><img src="asset/images/paypal2.png" alt="We accept PayPal" title="We accept PayPal"></li>
                                <li><img src="asset/images/visa.png" alt="We accept Visa" title="We accept Visa"></li>--%>
                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>

<script src="asset/js/custom.js"></script>

<!-- moris js -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<!-- PNotify -->
<script type="text/javascript" src="asset/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.nonblock.js"></script>

<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">



<!-- Load Hired Maids -->
<script>
    $(document).ready(function(){

        $.ajax({
            type:'POST',
            url:'/min/hiredMaid?userId=<%=userId%>',

            success: function(response){
                var initial = 0;
                var jsonResponse = JSON.parse(response);
                while(jsonResponse.length > initial){
                    var workerId = jsonResponse[initial].workerId;
                    var occupation = jsonResponse[initial].occupation;
                    var workerName = jsonResponse[initial].workerName;
                    var agency = jsonResponse[initial].compName;
                    var hireDate = jsonResponse[initial].hireDate;
                    var replace = jsonResponse[initial].replace;

                    if(replace == 'replace'){

                    $('#hireBlock').append('<div class="alert alert-info alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +

                    '<img src="asset/images/user.png" class="avatar" alt="Avatar">' +

                    '<div class="message_wrapper">' +
                    '<ul><li class="heading">'+workerName+'</li>' +
                    '<li>'+occupation+'</li><li> Hired '+hireDate+'</li><li> '+agency+'</li></ul>' +
                    '<p class="url">' +
                    '<a type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" href="/min/updateWorkerProf?id='+workerId+'\"" >Update Profile</a>' +
                    '<a type="button" class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="right" href="/min/writeReference?workerName='+workerId+'\"" >Disengage</a>' +
                    '<a type="button" class="btn btn-info btn-sm"  data-toggle="tooltip" data-placement="right" title="Cancel Replace Request" href="/min/confirmCanReplace?workerId='+workerId+'\"" >Pending Replacement</a>' +
                    '</p>' +
                    '</div>' +

                    '</div>');

                    initial++;
                    }
                    else {
                        $('#hireBlock').append('<div class="alert alert-info alert-dismissable">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +

                        '<img src="asset/images/user.png" class="avatar" alt="Avatar">' +

                        '<div class="message_wrapper">' +
                        '<ul><li class="heading">'+workerName+'</li>' +
                        '<li>'+occupation+'</li><li> Hired '+hireDate+'</li><li> '+agency+'</li></ul>' +
                        '<p class="url">' +
                        '<a type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" href="/min/updateWorkerProf?id='+workerId+'\"" >Update Profile</a>' +
                        '<a type="button" class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="right" href="/min/writeReference?workerName='+workerId+'\"" >Disengage</a>' +
                        '<a type="button" class="btn btn-info btn-sm"  data-toggle="tooltip" data-placement="right" href="/min/confirmReplace?workerId='+workerId+'\"" >Replace</a>' +
                        '</p>' +
                        '</div>' +

                        '</div>');

                        initial++;
                    }
                }

            },
            error: function(){

            }
        })
    });
</script>

<script>
    $(document).ready(function(){
        $.ajax({
            type:'GET',
            url:'/min/userPendingHires?userId=<%=userId%>',

            success: function(response){
                var initia =0;
                var jsonResponse = JSON.parse(response);

                while(jsonResponse.length > initia){
                    var workerId = jsonResponse[initia].workerId;
                    var occup = jsonResponse[initia].work;
                    var agency = jsonResponse[initia].agency;
                    var employed = jsonResponse[initia].employed;



                    $('#hireBlock').append('<div class="alert alert-info alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +

                    '<img src="asset/images/user.png" class="avatar" alt="Avatar">' +
                    '<div style="float: right; width: 200px " class="error-div"><span class="badge">Awaiting Confirmation</span> </div>'+
                    '<div class="message_wrapper" style="width: 200px;">' +
                    '<ul><li class="heading">Min0'+workerId+'</li>' +
                    '<li>'+occup+'</li><li> '+agency+'</li></ul>' +

                    '</div>' +

                    '</div>');


                    initia++

                }
            },
            error : function(){

            }
        })
    })
</script>

<!--Search Engine -->
<%--<script>

    $(document).ready(function(){

        $('#form1').submit(function(e){
            e.preventDefault();
        })

        $('#go').click(function(){
            $('#searchResult').empty()
            var initia = 0;
            var loginform = $('#form1')
            formData = loginform.serialize();

            $.ajax({
                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formData,

                success: function(response){

                    var jsonResponse = JSON.parse(response)

                    while(jsonResponse.length > initia) {
                        var firstname = jsonResponse[initia].first;

                        var age = jsonResponse[initia].age;
                        var sex = jsonResponse[initia].sex;
                        var work = jsonResponse[initia].work;
                        var agentName = jsonResponse[initia].agent;
                        //var image = jsonResponse[initia].image;
                        var idno = jsonResponse[initia].id;
                        var location = jsonResponse[initia].location;
                        var medical = jsonResponse[initia].medical;
                        var guarantor = 'Guarantor ' + '<i class="fa fa-check" style="color: green"></i>'
                        var agencyFee = jsonResponse[initia].agencyFee;
                        var expectedSal = jsonResponse[initia].expectedSal;
                        var name = firstname ;
                        var request = '/min/workerProfile?name='+firstname+'\"';

                        if(medical == 'none'){
                            medical = 'Medical Report ' + '<i class="fa fa-close" style="color: #ff0000"></i>'
                        }else{
                            medical = 'Medical Report ' + '<i class="fa fa-check" style="color: green"></i>'
                        }

                        $('#searchResult').append('<div class="col-md-7 col-sm-8 col-xs-10 animated fadeInDown">' +
                        '<div class="well profile_view">' +
                        '<div class="col-sm-12">' +
                        '<h4 class="brief"><i>MaidInNigeria</i></h4>' +
                        '<div class="left col-xs-7 ">' +
                        '<form >' +
                        '<h5><span id="name"></span> MIN 0' +idno + '</h5>' +

                        '<strong></strong> <span >' + work + '</span>' +
                        '<ul class="list-unstyled ">' +
                        '<li><span>  ' + location + '</span></li>' +
                        '<li></i><span >  ' + sex + ',</span><span> '+age+' </span></li>' +
                        '<li></i><span >  ' +  guarantor + '</span></li>' +
                        '<li></i><span >  ' + medical + '</span></li>' +
                        '<li>Agency Fee: <span >  '+agencyFee +'</span></li>' +
                        '<li>Expected Salary: <span >  '+expectedSal +'</span></li>' +
                        '</ul>' +
                        '</form>' +
                        '</div>' +
                        '<div class="right col-xs-5 text-center">' +
                        '<img src="asset/images/user.png" alt="' + work + '\"" class="img-circle img-responsive" title="'+name+'\"">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-12 bottom text-center">' +
                        '<div class="col-xs-12 col-sm-6 emphasis">' +
                        '<!--   <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">' +
                        '<!--</i> <i class="fa fa-comments-o"></i> </button>-->' +
                        '<a class="request" title="Edit Details" href="' + request + '\"" >  <button type="button" data-toggle="tooltip" data-placement="bottom" title="Request Worker" class="btn btn-primary btn-xs" id="requestBtn"> <i class="fa fa-user">' +
                        '</i> Review </button></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>')

                        initia++
                    }



                },
                error: function(){
                    $('#searchResult').append("Could not find result for your search..");
                }
            })
        })
    })

</script>--%>

<script>
    NProgress.done();
</script>
<style>
    h1{
        text-transform: uppercase;
        text-decoration: solid cornsilk;
    }
</style>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
    #searchResult{
        padding: 0px;
    }

    legend {
        display: block;
        padding-left: 2px;
        padding-right: 2px;

    }
</style>
</body>
</html>
