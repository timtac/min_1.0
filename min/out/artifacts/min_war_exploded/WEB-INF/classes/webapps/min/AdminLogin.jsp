<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2/10/2016
  Time: 3:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MaidInNigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="background:#F7F7F7;">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
        <div id="login" class="animate form">
            <section class="login_content">
                <form method="post" data-parsley-validate action="/min/adminLogon" id="form1">
                    <h1>Login</h1>
                    <div>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" required="required" />
                    </div>
                    <div>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required" />
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default submit" id="logon" >Log In</button>
                        <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">


                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <h1><i class="fa fa-paw" style="font-size: 26px;"></i> MaidInNigeria</h1>

                            <p>©2016 All Rights Reserved. MaidInNigeria. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>

    </div>
    <div class="foot">
        <p class="pull-right">MaidInNigeria. |
        </p>
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">
<script>
    jQuery(document).ready(function(){
        $('#form1').submit(function(e){
            e.preventDefault();
        });

        $('#logon').click(function(){
            var loginform = $('#form1');
            formdata = loginform.serialize();


            $.ajax({
                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formdata,
                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.success){
                        displaySuccessMessage("Welcome!!!");
                        window.location = "/min/adminPage.jsp";
                    }
                    else{
                        displayErrorDialog("Wrong Username/Password Combination");
                        $('#username').val("");
                        $('#password').val("")
                    }
                },
                error: function(jqXHR){
                    displayErrorDialog("Internal Server Error")

                }
            })
        });

        function displaySuccessMessage(message){
            jQuery.gritter.add({
                title: 'Authentication Status!',
                text: message,
                class_name: 'growl-success',
                image: '/min/asset/img/success.jpg',
                sticky: false,
                time: ''
            });
        }

        function displayErrorDialog(message){

            jQuery.gritter.add({
                title: 'Authentication Status!',
                text: message,
                class_name: 'growl-danger',
                image: '/min/asset/img/error.png',
                sticky: false,
                time: ''
            });

        }





    });
</script>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>
