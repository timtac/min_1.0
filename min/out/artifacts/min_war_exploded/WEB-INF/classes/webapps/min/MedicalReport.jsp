<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10/10/2015
  Time: 10:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(session!=null) {
        String state = (String) session.getAttribute("login");
        if (state == null) {
            response.sendRedirect("/min/AgentsLogin.jsp");
        }
    }
    else{ response.sendRedirect("/min/AgentsLogin.jsp");}
    String name = (String) session.getAttribute("Agent_username");
    String id = (String) session.getAttribute("id");
    String first = (String) session.getAttribute("first");
    String occupation = (String) session.getAttribute("occupation");
    String editUrl = "/min/editAgentAction?username="+ name ;
    String imageName = (String) session.getAttribute("agentImage");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }

%>
<!DOCTYPE html>
<html lang="en">

<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MaidInNigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">

    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria!</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="/min/AllWorkers.jsp" class=""><i class="fa fa-home active"></i> Home <!--<span class="fa fa-chevron-down"></span>--></a>
                                <!--  <ul class="nav child_menu" style="display: none">
                                      <li><a href="index.jsp">Dashboard</a>
                                      </li>
                                      <li><a href="index2.html">Dashboard2</a>
                                      </li>
                                      <li><a href="index3.html">Dashboard3</a>
                                      </li>
                                  </ul>-->
                            </li>
                            <li><a href="/min/AddWorker.jsp"><i class="fa fa-user"></i> Add Worker </a>
                                <!--  <ul class="nav child_menu" style="display: none">
                                      <li><a href="form.html">General Form</a>
                                      </li>
                                      <li><a href="form_advanced.html">Advanced Components</a>
                                      </li>
                                      <li><a href="form_validation.html">Form Validation</a>
                                      </li>
                                      <li><a href="form_wizards.html">Form Wizard</a>
                                      </li>
                                      <li><a href="form_upload.html">Form Upload</a>
                                      </li>
                                      <li><a href="form_buttons.html">Form Buttons</a>
                                      </li>
                                  </ul>-->
                            </li>

                            <!-- <li><a href="/min/AllWorkers.jsp"><i class="fa fa-group"></i> All Workers </a>
                                -   <ul class="nav child_menu" style="display: none">
                                        <li><a href="general_elements.html">General Elements</a>
                                        </li>
                                        <li><a href="media_gallery.html">Media Gallery</a>
                                        </li>
                                        <li><a href="typography.html">Typography</a>
                                        </li>
                                        <li><a href="icons.html">Icons</a>
                                        </li>
                                        <li><a href="glyphicons.html">Glyphicons</a>
                                        </li>
                                        <li><a href="widgets.html">Widgets</a>
                                        </li>
                                        <li><a href="invoice.html">Invoice</a>
                                        </li>
                                        <li><a href="inbox.html">Inbox</a>
                                        </li>
                                        <li><a href="calender.html">Calender</a>
                                        </li>
                                    </ul>
                            </li>-->
                            <li><a href="/min/AddGuarantor.jsp"><i class="fa fa-user"></i>Add Guarantor</a></li>
                            <li><a href="/min/BackgroundCheck.jsp"><i class="fa fa-group"></i> Background Check </a>
                            <!--  <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                     <li><a href="tables.html">Tables</a>
                                     </li>
                                     <li><a href="tables_dynamic.html">Table Dynamic</a>
                                     </li>
                                 </ul>
                            </li>-->
                            <!--   <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                                   <ul class="nav child_menu" style="display: none">
                                       <li><a href="chartjs.html">Chart JS</a>
                                       </li>
                                       <li><a href="chartjs2.html">Chart JS2</a>
                                       </li>
                                       <li><a href="morisjs.html">Moris JS</a>
                                       </li>
                                       <li><a href="echarts.html">ECharts </a>
                                       </li>
                                       <li><a href="other_charts.html">Other Charts </a>
                                       </li>
                                   </ul>
                               </li>-->
                        </ul>
                    </div>
                    <!--  <div class="menu_section">
                          <h3>Live On</h3>
                          <ul class="nav side-menu">
                              <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu" style="display: none">
                                      <li><a href="e_commerce.html">E-commerce</a>
                                      </li>
                                      <li><a href="projects.html">Projects</a>
                                      </li>
                                      <li><a href="project_detail.html">Project Detail</a>
                                      </li>
                                      <li><a href="contacts.html">Contacts</a>
                                      </li>
                                      <li><a href="profile.html">Profile</a>
                                      </li>
                                  </ul>
                              </li>
                              <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu" style="display: none">
                                      <li><a href="page_404.html">404 Error</a>
                                      </li>
                                      <li><a href="page_500.html">500 Error</a>
                                      </li>
                                      <li><a href="plain_page.html">Plain Page</a>
                                      </li>
                                      <li><a href="login.html">Login Page</a>
                                      </li>
                                      <li><a href="pricing_tables.html">Pricing Tables</a>
                                      </li>

                                  </ul>
                              </li>
                              <li><a><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>
                              </li>
                          </ul>
                      </div>-->

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <!--   <div class="sidebar-footer hidden-small">
                       <a data-toggle="tooltip" data-placement="top" title="Settings">
                           <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                       </a>
                       <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                           <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                       </a>
                       <a data-toggle="tooltip" data-placement="top" title="Lock">
                           <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                       </a>
                       <a data-toggle="tooltip" data-placement="top" title="Logout">
                           <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                       </a>
                   </div>-->
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="<%=editUrl%>">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="/min/AgentHomePage.jsp">Profile</a>
                                </li>
                                <li>
                                    <a href="">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <!--    <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span></span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                                <span class="image">
                                            <img src="Admin/images/img.jpg" alt="Profile Image" />
                                        </span>
                                                <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                            <a>
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>-->

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3></h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="height: 700px;">
                        <div class="x_panel" style="height: 700px;">

                            <div class="x_title">
                                <h2>Steward Details</h2>

                                <div class="clearfix"></div>
                            </div>
                            <form class="form-horizontal form-label-left"  action="/min/medicals"  role="form"  method="post" id="form1" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Worker ID <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="id" required="required" value="<%=id%>" id="id" class="form-control col-md-7 col-xs-12" contenteditable="false"  readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="first" required="required" value="<%=first%>" id="first" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Occupation<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" value="<%=occupation%>" id="occupation" class="form-control col-md-7 col-xs-12" name="occupation">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <i class="fa fa-file-image-o"></i><input class="form-control col-md-7 col-xs-12 file-name" id="file" type="file" name="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1 center-margin">
                                        <button type="submit" class="btn btn-success btn-block col-sm-3" id="logon" >Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria"></a></li>
                        <%--<li><a class="fa fa-google-plus-square" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="fa fa-linkedin-square" data-original-title="linkedin" href="#"></a></li>
                        <li><a class="fa fa-youtube-square" data-original-title="youtube" href="#"></a></li>
                        <li><a class="fa fa-vimeo-square" data-original-title="vimeo" href="#"></a></li>
                        <li><a class="fa fa-skype" data-original-title="skype" href="#"></a></li>--%>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <%--<div class="col-md-6 col-sm-6 ">
                    <div class="pre-footer-subscribe-box pull-right">
                        <h2>Newsletter</h2>
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="youremail@mail.com" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </span>
                            </div>
                        </form>
                    </div>
                </div>--%>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2015 © MaidInNigeria. ALL Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">
                                <!-- <li><img src="/frontend/assets/img/payments/western-union.jpg" alt="We accept Western Union" title="We accept Western Union"></li>-->
                               <%-- <li><img src="asset/images/american-express.png" alt="We accept American Express" title="We accept American Express"></li>
                                <li><img src="asset/images/mastercard.png" alt="We accept MasterCard" title="We accept MasterCard"></li>
                                <li><img src="asset/images/paypal2.png" alt="We accept PayPal" title="We accept PayPal"></li>
                                <li><img src="asset/images/visa.png" alt="We accept Visa" title="We accept Visa"></li>--%>
                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>

<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>

<script src="asset/js/custom.js"></script>
<script src="asset/js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">
<script type="text/javascript" src="asset/js/custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(){{
        $('#form1').submit(function(e){
            e.preventDefault();
        });

        $('#logon').click(function(){
            var form = $('#form1');
            formData = form.serialize();

            $.ajax({
                type:form.attr("method"),
                url:form.attr("action"),
                data:formData,
                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        displaySuccessMessage("Steward Details Successfully updated")
                    }else{
                        displayErrorDialog("Error Processing Upload")
                    }
                },
                error: function(jqXHR){
                    displayErrorDialog("Internal Server Error")
                }
            })
        });
        function displaySuccessMessage(message){
            jQuery.gritter.add({
                title: 'Authentication Status!',
                text: message,
                class_name: 'growl-success',
                image: '/min/asset/img/success.jpg',
                sticky: false,
                time: ''
            });
        }

        function displayErrorDialog(message){
            jQuery.gritter.add({
                title: 'Notice!',
                text: message,
                class_name: 'growl-danger',
                image: '/min/asset/img/error.png',
                sticky: false,
                time: ''
            });
        }

    }})
</script>
<script>
    NProgress.done();
</script>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>
