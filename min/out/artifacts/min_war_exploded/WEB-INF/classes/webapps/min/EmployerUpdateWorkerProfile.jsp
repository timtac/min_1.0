<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/9/2016
  Time: 3:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String name = (String) session.getAttribute("User_username");
    String id = (String) session.getAttribute("id");
    String first = (String) session.getAttribute("first");
    String age = (String) session.getAttribute("age");
    String mobile = (String) session.getAttribute("mobile");
    String compName = (String) session.getAttribute("compName");
    String occupation = (String) session.getAttribute("occupation");


    String imageName = (String) session.getAttribute("userImage");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }

%>
<!DOCTYPE html>
<html lang="en">

<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">

    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>--%>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3></h3>
                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <div class="wrapper">
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/UserHomePage.jsp">Dashboard</a>
                            </li>
                            <li >
                                <a href="/min/Requests.jsp">Requests</a>
                            </li>
                            <li >
                                <a href="/min/Hired.jsp">Hires</a>
                            </li>
                            <li >
                                <a href="/min/Disengaged.jsp">Disengaged</a>
                            </li>

                            <li >
                                <a href="/min/index.jsp">Home Page</a>
                            </li>

                            <li>
                                <a href="/min/mail.jsp">Contact Us</a>
                            </li>


                        </ul>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3></h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="height: 800px;">
                        <div class="x_panel" style="">

                            <div class="x_title">
                                <h2>Steward Details</h2>

                                <div class="clearfix"></div>
                            </div>

                            <form class="form-horizontal form-label-left" data-parsley-validate action="/min/updateWorkerDetail"  role="form"  method="post" id="form1">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Worker ID
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" name="id" required="required" value="<%=id%>"  class="form-control col-md-7 col-xs-12" contenteditable="false"  readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="first" value="<%=first%>"  class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Occupation
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="<%=occupation%>" class="form-control col-md-7 col-xs-12" name="occupation">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Age
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" value="<%=age%>" class="form-control col-md-7 col-xs-12" name="age">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" disabled value="<%=compName%>" class="form-control col-md-7 col-xs-12" name="compName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" required="required" value="<%=mobile%>" class="form-control col-md-7 col-xs-12" name="mobile">
                                    </div>
                                </div>
                                <div class="btn-toolbar " style="display: inline-block; padding-left: 40%;">
                                    <a class="btn btn-danger btn-sm" href="/min/Hired.jsp">Cancel</a>
                                        <button type="submit" class="btn btn-success btn-sm " id="submit" >Submit</button>
                                        <a href="/min/userAddGuan.jsp" type="button" class="btn btn-info btn-sm">Add Guarantor</a>
                                        <a href="/min/BackgroundCheck.jsp" type="button" class="btn btn-info btn-sm">Add Background Check</a>


                                </div>
                            </form>
                        </div>

                        <div class="x_panel" style="">

                            <div class="x_title">
                                <h2>Medical Report</h2>

                                <div class="clearfix"></div>
                            </div>
                            <form class="form-horizontal form-label-left" data-parsley-validate action="/min/medicals"  role="form"  method="post" id="form3" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Worker ID
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" name="id" required="required" value="<%=id%>"  class="form-control col-md-7 col-xs-12" contenteditable="false"  readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose File</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <i class="fa fa-file-image-o"></i><input class="form-control col-md-7 col-xs-12 file-name" id="file" type="file" name="medical">
                                    </div>
                                </div>
                                <div class="item form-group" style="padding-left: 62%">

                                    <a class="btn btn-danger btn-sm" href="/min/Hired.jsp">Cancel</a>
                                    <button type="submit" class="btn btn-info btn-sm" id="on" >Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria"></a></li>

                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <div class="col-md-6 col-sm-6 ">
                    <div class="pre-footer-subscribe-box pull-right">

                    </div>
                </div>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>

<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>

<script src="asset/js/custom.js"></script>
<script src="asset/js/validator/validator.js"></script>
<%--<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>--%>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">
<script type="text/javascript" src="asset/js/custom.js"></script>
<%--<script type="text/javascript">
    function displaySuccessMessage(message){
        jQuery.gritter.add({
            title: 'Authentication Status!',
            text: message,
            class_name: 'growl-success',
            image: '/min/asset/img/success.jpg',
            sticky: false,
            time: ''
        });
    }

    function displayErrorDialog(message){
        jQuery.gritter.add({
            title: 'Notice!',
            text: message,
            class_name: 'growl-danger',
            image: '/min/asset/img/error.png',
            sticky: false,
            time: ''
        });
    }
    $(document).ready(function() {
        $('#form1').submit(function (e) {
            e.preventDefault();
        })

        $('#submit').click(function () {
            var form = $('#form1')
            formData = form.serialize()

            $.ajax({
                type: form.attr("method"),
                url: form.attr("action"),
                data: formData,
                success: function (response) {
                    var jsonResponse = JSON.parse(response)
                    if (jsonResponse.success) {
                        displaySuccessMessage("Steward Details Successfully updated")
                    } else {
                        displayErrorDialog("Error Processing Upload")
                    }
                },
                error: function (jqXHR) {
                    displayErrorDialog("Internal Server Error")
                }
            });
        });

    })

    $(document).ready(function(){
        $('#form2').submit(function(e){
            e.preventDefault();
        });

        $('#logon').click(function(){
            var form2 = $('#form2')
            formData = form2.serialize()
        })

        $.ajax({
            type:form2.attr("method"),
            url:form2.attr('action'),
            data:formData,
            success: function(response){
                var jsonResponse = JSON.parse(response)
                if(jsonResponse.success){
                displaySuccessMessage("Uploaded Successfully")
                }
            },
            error:function(jqXHR){
                displayErrorDialog("Uploaded Successfully")
            }
        })
    })

    $(document).ready(function(){
        $('#form3').submit(function(e){
            e.preventDefault();
        });

        $('#on').click(function(){
            var form2 = $('#form2')
            formData = form2.serialize()
        })

        $.ajax({
            type:form2.attr("method"),
            url:form2.attr('action'),
            data:formData,
            success: function(response){
                var jsonResponse = JSON.parse(response);
                if(jsonResponse.success){
                displaySuccessMessage("Uploaded Successfully");
                }
            },
            error:function(jqXHR){
                displayErrorDialog("Uploaded Successfully");
            }
        })
    })
</script>--%>
<script>
    NProgress.done();
</script>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>
