<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/15/2016
  Time: 6:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String username = (String) session.getAttribute("User_username");
    //session.setAttribute("User_username",username);
    String userId = (String) session.getAttribute("userId");
    String workerId = (String) session.getAttribute("workerId");
    String workerName = (String) session.getAttribute("workername");
    String userfullname = (String) session.getAttribute("Userfullname");
    String agent = (String) session.getAttribute("agentName");
    String occupation = (String) session.getAttribute("occu");
    String hiredDate = (String) session.getAttribute("hiredDate");
    String imageName = (String) session.getAttribute("userImage");
    String employerMail = (String) session.getAttribute("mail");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }
    //String medicalUrl = "/min/medicalReport?fileName="+medical;

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">


    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->
                <%--<div class="profile">
                    <div class="profile_pic">
                        <img src="asset/images/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2></h2>
                    </div>
                </div>--%>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <%--<h3>General</h3>--%>
                        <ul class="nav side-menu">

                            </li>

                            </li>

                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <ul class="nav navbar-nav navbar-left">
                        <li >
                            <a href="/min/UserHomePage.jsp">Dashboard</a>
                        </li>
                        <li >
                            <a href="/min/Requests.jsp">Requests</a>
                        </li>
                        <li >
                            <a href="/min/Hired.jsp">Hires</a>
                        </li>
                        <li >
                            <a href="/min/Disengaged.jsp">Disengaged</a>
                        </li>

                        <li >
                            <a href="/min/index.jsp">Home Page</a>
                        </li>

                        <li>
                            <a href="/min/mail.jsp">Contact Us</a>
                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="admin/images/user.png" alt=""><%=username%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3> </h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row" style="height: 700px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <div class="col-md-4 col-sm-4 col-xs-12 profile_left">

                                    <div class="profile_img">

                                        <!-- end of image cropping -->
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="">
                                                <img src="<%=imageUrl%>" alt="" title="<%=workerId%>">
                                            </div>

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                        <!-- end of image cropping -->

                                    </div>

                                </div>

                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <h3>Min0<%=workerId%> </h3>

                                    <ul class="list-unstyled user_data">
                                        <li><i class="fa fa-user user-profile-icon"></i> <%=workerName%>
                                        </li>

                                        <li>
                                            <i class="fa fa-briefcase user-profile-icon"></i> <%=occupation%>
                                        </li>
                                        <li>
                                            <i class="fa fa-briefcase user-profile-icon"></i> <%=hiredDate%>
                                        </li>

                                        <li>
                                            <i class="fa fa-briefcase user-profile-icon"></i><span> <%=agent%></span>
                                        </li>

                                    </ul>
                                <i>NB: You can only replace a domestic staff of the same profession and of the same agent...Also you are entitled to 2 changes within a year</i> <br />
                              <a href="/min/Hired.jsp" class="btn btn-danger">Cancel</a>      <a class="btn btn-success" href="/min/replaceWorker?userId=<%=userId%>&workerId=<%=workerId%>&employerName=<%=userfullname%>&mail=<%=employerMail%>&agent=<%=agent%>&workerName=<%=workerName%>"></i>Replace</a>
                                    <br />

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="#"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="#"></a></li>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->
                <div class="col-md-6 col-sm-6 ">

                </div>
                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © Novatia Ltd. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="admin/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="admin/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="admin/js/icheck/icheck.min.js"></script>

<script src="admin/js/custom.js"></script>

<!-- image cropping -->
<script src="admin/js/cropping/cropper.min.js"></script>
<script src="admin/js/cropping/main.js"></script>

<!-- daterangepicker -->
<script type="text/javascript" src="admin/js/moment.min.js"></script>
<script type="text/javascript" src="admin/js/datepicker/daterangepicker.js"></script>
<!-- moris js -->
<script src="admin/js/moris/raphael-min.js"></script>
<script src="admin/js/moris/morris.js"></script>
<script>

</script>

<script>
    NProgress.done();
</script>
<!-- /datepicker -->
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
</body>
</html>

