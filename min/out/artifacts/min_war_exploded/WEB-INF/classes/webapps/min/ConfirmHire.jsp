<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/5/2016
  Time: 5:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String workerId = (String) session.getAttribute("worker_id");
    String userId = (String) session.getAttribute("userId");
    String username = (String) session.getAttribute("User_username");
    String employerName = (String) session.getAttribute("employerName");
    String employeeMail = (String) session.getAttribute("mail");
    String fullname = (String) session.getAttribute("fullname");
    String agent = (String) session.getAttribute("agent");
    String age = (String) session.getAttribute("age");
    String sex = (String) session.getAttribute("sex");
    String relationship = (String) session.getAttribute("relationship");
    String occupation = (String) session.getAttribute("occupation");
    String fee = (String) session.getAttribute("fee");
    String salary = (String) session.getAttribute("salary");
    String imageName = (String) session.getAttribute("image");
    String employed = (String) session.getAttribute("employed");
    String preference = (String) session.getAttribute("preference");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+imageName;
    }else{
        imageUrl = "asset/images/user.png";
    }
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | Register Maids, Nanny, Drivers, Chef, Security, Gardener..</title>

    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants.">
    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="asset/custom/selfMade.js"></script>
    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3></h3>
                        <ul class="nav side-menu">


                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                <div class="wrapper">
                    <ul class="nav navbar-nav navbar-left menu">
                        <li >
                            <a href="/min/UserHomePage.jsp">Dashboard</a>
                        </li>
                        <li >
                            <a href="/min/Requests.jsp">Requests</a>
                        </li>
                        <li >
                            <a href="/min/Hired.jsp">Hires</a>
                        </li>
                        <li >
                            <a href="/min/Disengaged.jsp">Disengaged</a>
                        </li>

                        <li >
                            <a href="/min/index.jsp">Home Page</a>
                        </li>

                        <li>
                            <a href="/min/mail.jsp">Contact Us</a>
                        </li>


                    </ul>
                </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=username%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <a href="/min/AgentHomePage.jsp">Profile</a>
                                </li>
                                <li>
                                    <a href="">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>CONFIRM HIRE</h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="height:720px;">
                            <div class="x_title">
                                <h2>Confirm Worker's Details</h2>
                                <ul class="nav navbar-right panel_toolbox">

                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-12 profile_left" >

                                <div class="profile_img">

                                    <!-- end of image cropping -->
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <div class="avatar-view" title="Change the avatar">
                                            <img src="<%=imageUrl%>" title="">
                                        </div>

                                        <!-- Loading state -->
                                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                    </div>
                                    <!-- end of image cropping -->

                                </div>
                            </div>

                            <div class="col-md-5 col-sm-5 col-xs-12" id="block">

                                <form class="form-horizontal form-label-left" data-parsley-validate method="post" action="/min/hire"  id="form1" >
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Worker ID</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" required="required" style="border: none;" class="form-control col-md-2 col-xs-12" name="" value="Min0<%=workerId%>" readonly>
                                        </div>
                                    </div>
                                    <input type="hidden" name="workerId" value="<%=workerId%>">
                                    <input type="hidden" name="maid" value="<%=fullname%>">
                                    <input type="hidden" name="userId" value="<%=userId%>">
                                    <input type="hidden" name="username" value="<%=username%>">
                                    <input type="hidden" name="mail" value="<%=employeeMail%>">
                                    <input type="hidden" name="name" value="<%=employerName%>">
                                    <input type="hidden" name="employed" id="employed" value="<%=employed%>">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Agency Name
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" required="required" class="form-control col-md-3 col-xs-12" name="agent" value="<%=agent%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Occupation
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="occupation" class="form-control col-md-3 col-xs-12" value="<%=occupation%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Age</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-3 col-xs-12" type="text" name="age" value="<%=age%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Sex</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-3 col-xs-12" type="text" name="sex" value="<%=sex%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Status
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="relationship" class="form-control col-md-3 col-xs-12" value="<%=relationship%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Agency Fee
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="agencyFee"  class="form-control col-md-3 col-xs-12" value="<%=fee%>" disabled>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Expected Salary
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="salary" class="form-control col-md-3 col-xs-12" value="<%=salary%>" readonly>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Preference </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <%=preference%>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="item form-group">

                                        <input type="checkbox" name="checkbox"  id="brand" value="cancel">
                                        <label for="brand"><span></span>Cancel all other <%=occupation%> requests</label>

                                    </div>
                                    <div class="item form-group">
                                        <a href="/min/Hired.jsp" class="btn btn-danger">Cancel</a>
                                        <input type="submit" class="btn btn-success btn-sm" id="link" value="Hire">
                                    </div>
                                </form>


                                <div class="ln_solid"></div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria"></a></li>
                        <%--<li><a class="fa fa-google-plus-square" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="fa fa-linkedin-square" data-original-title="linkedin" href="#"></a></li>
                        <li><a class="fa fa-youtube-square" data-original-title="youtube" href="#"></a></li>
                        <li><a class="fa fa-vimeo-square" data-original-title="vimeo" href="#"></a></li>
                        <li><a class="fa fa-skype" data-original-title="skype" href="#"></a></li>--%>
                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->

                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>
<!-- tags-->

<!-- PNotify -->
<script type="text/javascript" src="asset/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.nonblock.js"></script>

<!-- textarea resize -->
<script src="asset/js/textarea/autosize.min.js"></script>
<script>
    autosize($('.resizable_textarea'));
</script>
<!-- form validation -->
<script src="asset/js/validator/validator.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){
        var employed = $('#employed').val();
        if (employed == "employed"){
            $('#link').attr('disabled',true);
            $('#block').append('<span class="badge" style="color: dodgerblue;">* Sorry this domestic staff has been engaged by another employer </span><a href="/min/remove?username<%=userId%>&maid=<%=workerId%>"> Remove staff from requested maids</a>')
        }else if(employed == "Unemployed"){
            $('#link').attr('disabled',false)
        }else if(employed == 'pending'){
            $('#link').attr('disabled',false)
        }
    })
</script>

<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
<script>
    NProgress.done();
</script>
</body>
</html>

