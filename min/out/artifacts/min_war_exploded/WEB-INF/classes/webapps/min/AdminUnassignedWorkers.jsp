<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 6/18/2016
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(session != null){
        String state =(String) session.getAttribute("login");
        if(state==null){
            response.sendRedirect("/min/AdminLogin.jsp");
        }
    }
    else{
        response.sendRedirect("/min/AdminLogin.jsp");
    }
    String name = (String) session.getAttribute("username");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | </title>

    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_page.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table.css" rel="stylesheet">
    <link href="asset/css/datatables/css/demo_table_jui.css" rel="stylesheet">
    <link href="asset/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link href="asset/css/datatables/css/jquery.dataTables_themeroller.css" rel="stylesheet">
    <link href="asset/js/jquery-ui-1.11.4/jquery-ui.css" media="all" rel="stylesheet" type="text/css">
    <script src="asset/js/datatables/js/jquery.dataTables.js"></script>

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <script src="asset/js/datatables/js/jquery.js"></script>
    <script src="asset/js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <%--<div class="navbar nav_title" style="border: 0;">
                    <a class="site_title"> <span>MaidInNigeria</span></a>
                </div>
                <div class="clearfix"></div>--%>

                <!-- menu prile quick info -->

                <!-- /menu prile quick info -->

                <br />
                <%--<div class="profile_info">


                </div>--%>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">

                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <div class="wrapper">
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/adminPage.jsp">Dashboard</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllUsers.jsp">Users</a>
                            </li>

                            <li >
                                <a href="/min/AdminAllAgents.jsp">Agents</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllWorkers.jsp">Domestic Staffs</a>
                            </li>
                            <li >
                                <a href="/min/AdminAllHiredStaff.jsp">Hired Staffs</a>
                            </li>

                            <li>
                                <a href="/min/AdminUnassignedWorkers.jsp">Un-Assigned Staffs</a>
                            </li>
                            <li>
                                <a href="/min/PendingHire.jsp">Pending Hires</a>
                            </li>
                            <li>
                                <a href="/min/MailAllAgents.jsp">Mail All Agents</a>
                            </li>

                            <li><a href="/min/MailAllUsers.jsp"> Mail All Users</a>


                        </ul>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="asset/images/user.png" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="/min/AgentsReg.jsp">Register Agent</a>
                                </li>
                                <li>
                                    <a href=""></a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3></h3>
                    </div>


                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>Un-Assigned Staffs</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 " id="dt_example" >
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2><small></small></h2>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content"  >

                                        <table class=" table table-striped" id="Unworkertable">

                                            <tr>
                                                <th>#</th>
                                                <th>Full Name</th>
                                                <th>Occupation</th>
                                                <th>Location</th>
                                                <th>Gender</th>
                                                <th>Age</th>
                                                <th>Mobile</th>
                                                <th>E-mail</th>
                                                <th>Action</th>

                                            </tr>


                                            <tbody>



                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">

                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->

                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
            <div class="foot">
                <p class="pull-right">MaidInNigeria. |
                </p>
            </div>
            <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<!-- bootstrap progress js -->
<!-- icheck -->

<script src="asset/js/custom.js"></script>

<!-- moris js-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script type="text/javascript">

    //    $(document).ready(function(){
    //    $('#mytable').dataTable({
    //        sPaginationType: 'full_numbers',
    //                bJQueryUI: true
    //    })
    //    });

    $(document).ready(function(){

        $.ajax({
            type:"GET",
            url:"/min/unassign",
            success: function(response){
                var jsonResponse = JSON.parse(response);
                var i =0;
                while(jsonResponse.length > i){
                    var id = jsonResponse[i].id;
                    var name = jsonResponse[i].name;
                    var location = jsonResponse[i].location;
                    var occupation = jsonResponse[i].occupation;
                    var sex = jsonResponse[i].gender;
                    var age = jsonResponse[i].age;
                    var mobile = jsonResponse[i].mobile;
                    var email = jsonResponse[i].email;
                    var no = i + 1;
                    $('#Unworkertable').append('<tr> <td scope="row">'+no+'</td><td>'+name+'</td><td>'+occupation+'</td><td>'+location+'</td><td>'+sex+'</td><td>'+age+'</td><td>'+mobile+'</td><td>'+email+'</td><td><a class="btn btn-success" href="/min/deleteUnassign?id='+id+'"><i class="fa fa-edit m-right-xs"></i>Delete Profile</a></td></tr>');

                    i++;

                }
            },
            error: function(){
                console.log("error")
            }
        })


    })

</script>
<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
<script>
    NProgress.done();
</script>
<!-- Datatables -->
<script src="asset/js/datatables/js/jquery.dataTables.js"></script>
<script src="asset/js/datatables/tools/js/dataTables.tableTools.js"></script>

</body>

</html>

