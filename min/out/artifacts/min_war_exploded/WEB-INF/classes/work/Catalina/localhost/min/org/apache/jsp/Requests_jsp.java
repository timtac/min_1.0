/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.59
 * Generated at: 2016-08-05 10:55:06 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Requests_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");

     if(session != null){
         String status = (String) session.getAttribute("login");
         if(status == null){
             response.sendRedirect("/min/UserLogin.jsp");
         }
     }
     else{response.sendRedirect("/min/UserLogin.jsp");}

    String username = (String) session.getAttribute("User_username");
    String userfullname = (String) session.getAttribute("Userfullname");
    String userId = (String) session.getAttribute("userId");
    String mobile = (String) session.getAttribute("mobile");

    session.setAttribute("employerName",userfullname);
    String mail = (String) session.getAttribute("mail");
    String imageName = (String) session.getAttribute("userImage");
    String location = (String) session.getAttribute("location");

    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+imageName;
    }else{
        imageUrl = "asset/images/user.png";
    }
    String editUrl = "/min/editUserAction?username="+ username;

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <!-- Meta, title, CSS, favicons, etc. -->\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("\r\n");
      out.write("    <title>MaidInNigeria | </title>\r\n");
      out.write("\r\n");
      out.write("    <!-- Bootstrap core CSS -->\r\n");
      out.write("\r\n");
      out.write("    <link href=\"asset/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <link href=\"asset/fonts/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"asset/css/animate.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <!-- Custom styling plus plugins -->\r\n");
      out.write("    <link href=\"asset/css/custom.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"asset/css/icheck/flat/green.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <script src=\"asset/js/jquery.min.js\"></script>\r\n");
      out.write("    <script src=\"asset/js/nprogress.js\"></script>\r\n");
      out.write("    <script>\r\n");
      out.write("        NProgress.start();\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    <!--[if lt IE 9]>\r\n");
      out.write("    <script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>\r\n");
      out.write("    <![endif]-->\r\n");
      out.write("\r\n");
      out.write("    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->\r\n");
      out.write("    <!--[if lt IE 9]>\r\n");
      out.write("    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\r\n");
      out.write("    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\r\n");
      out.write("    <![endif]-->\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<body class=\"nav-md\">\r\n");
      out.write("\r\n");
      out.write("<div class=\"container body\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div class=\"main_container\">\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <!-- top navigation -->\r\n");
      out.write("        <div class=\"top_nav\">\r\n");
      out.write("\r\n");
      out.write("            <div class=\"nav_menu\">\r\n");
      out.write("                <nav class=\"\" role=\"navigation\">\r\n");
      out.write("                    <div class=\"nav toggle\">\r\n");
      out.write("                        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"navbar-left\">\r\n");
      out.write("                        <a href=\"/min/index.jsp\"><img src=\"images/MINLogo2.PNG\"></a>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"wrapper\">\r\n");
      out.write("                    <ul class=\"nav navbar-nav navbar-left menu\">\r\n");
      out.write("                        <li >\r\n");
      out.write("                            <a href=\"/min/UserHomePage.jsp\">Dashboard</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("                        <li >\r\n");
      out.write("                            <a href=\"/min/Requests.jsp\">Requests</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("                        <li >\r\n");
      out.write("                            <a href=\"/min/Hired.jsp\">Hires</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("                        <li >\r\n");
      out.write("                            <a href=\"/min/Disengaged.jsp\">Disengaged</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("\r\n");
      out.write("                        <li >\r\n");
      out.write("                            <a href=\"/min/index.jsp\">Home Page</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("\r\n");
      out.write("                        <li>\r\n");
      out.write("                            <a href=\"/min/mail.jsp\">Contact Us</a>\r\n");
      out.write("                        </li>\r\n");
      out.write("\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <ul class=\"nav navbar-nav navbar-right \">\r\n");
      out.write("                        <li class=\"\">\r\n");
      out.write("                            <a href=\"javascript:\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">\r\n");
      out.write("                                <img src=\"");
      out.print(imageUrl);
      out.write("\" alt=\"\">");
      out.print(username);
      out.write("\r\n");
      out.write("                                <span class=\" fa fa-angle-down\"></span>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">\r\n");
      out.write("                                <li><a href=\"");
      out.print(editUrl);
      out.write("\"> Update Profile</a>\r\n");
      out.write("                                </li>\r\n");
      out.write("                                <li>\r\n");
      out.write("                                    <a href=\"/min/about.jsp\">About</a>\r\n");
      out.write("                                </li>\r\n");
      out.write("                                <li><a href=\"/min/logout\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>\r\n");
      out.write("                                </li>\r\n");
      out.write("                            </ul>\r\n");
      out.write("                        </li>\r\n");
      out.write("\r\n");
      out.write("                        <!--    <li role=\"presentation\" class=\"dropdown\">\r\n");
      out.write("                                <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">\r\n");
      out.write("                                    <i class=\"fa fa-envelope-o\"></i>\r\n");
      out.write("                                    <span class=\"badge bg-green\">6</span>\r\n");
      out.write("                                </a>\r\n");
      out.write("                                <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list animated fadeInDown\" role=\"menu\">\r\n");
      out.write("                                    <li>\r\n");
      out.write("                                        <a>\r\n");
      out.write("                                                <span class=\"image\">\r\n");
      out.write("                                            <img src=\"Admin/images/img.jpg\" alt=\"Profile Image\" />\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                                <span>\r\n");
      out.write("                                            <span>John Smith</span>\r\n");
      out.write("                                                <span class=\"time\">3 mins ago</span>\r\n");
      out.write("                                                </span>\r\n");
      out.write("                                                <span class=\"message\">\r\n");
      out.write("                                            Film festivals used to be do-or-die moments for movie makers. They were where...\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                        </a>\r\n");
      out.write("                                    </li>\r\n");
      out.write("                                    <li>\r\n");
      out.write("                                        <a>\r\n");
      out.write("                                                <span class=\"image\">\r\n");
      out.write("                                            <img src=\"Admin/images/img.jpg\" alt=\"Profile Image\" />\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                                <span>\r\n");
      out.write("                                            <span></span>\r\n");
      out.write("                                                <span class=\"time\">3 mins ago</span>\r\n");
      out.write("                                                </span>\r\n");
      out.write("                                                <span class=\"message\">\r\n");
      out.write("                                            Film festivals used to be do-or-die moments for movie makers. They were where...\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                        </a>\r\n");
      out.write("                                    </li>\r\n");
      out.write("                                    <li>\r\n");
      out.write("                                        <a>\r\n");
      out.write("                                                <span class=\"image\">\r\n");
      out.write("                                            <img src=\"images/img.jpg\" alt=\"Profile Image\" />\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                                <span>\r\n");
      out.write("                                            <span>John Smith</span>\r\n");
      out.write("                                                <span class=\"time\">3 mins ago</span>\r\n");
      out.write("                                                </span>\r\n");
      out.write("                                                <span class=\"message\">\r\n");
      out.write("                                            Film festivals used to be do-or-die moments for movie makers. They were where...\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                        </a>\r\n");
      out.write("                                    </li>\r\n");
      out.write("                                    <li>\r\n");
      out.write("                                        <a>\r\n");
      out.write("                                                <span class=\"image\">\r\n");
      out.write("                                            <img src=\"Admin/images/img.jpg\" alt=\"Profile Image\" />\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                                <span>\r\n");
      out.write("                                            <span>John Smith</span>\r\n");
      out.write("                                                <span class=\"time\">3 mins ago</span>\r\n");
      out.write("                                                </span>\r\n");
      out.write("                                                <span class=\"message\">\r\n");
      out.write("                                            Film festivals used to be do-or-die moments for movie makers. They were where...\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                        </a>\r\n");
      out.write("                                    </li>\r\n");
      out.write("                                    <li>\r\n");
      out.write("                                        <div class=\"text-center\">\r\n");
      out.write("                                            <a>\r\n");
      out.write("                                                <strong>See All Alerts</strong>\r\n");
      out.write("                                                <i class=\"fa fa-angle-right\"></i>\r\n");
      out.write("                                            </a>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>-->\r\n");
      out.write("\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </nav>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("        <!-- /top navigation -->\r\n");
      out.write("\r\n");
      out.write("        <!-- page content -->\r\n");
      out.write("        <div class=\"right_col\" role=\"main\">\r\n");
      out.write("\r\n");
      out.write("            <div class=\"\" >\r\n");
      out.write("                <div class=\"page-title\">\r\n");
      out.write("                    ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    ");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"clearfix\"></div>\r\n");
      out.write("\r\n");
      out.write("                <div class=\"row\" style=\"height: 700px;\">\r\n");
      out.write("                    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n");
      out.write("                        <div class=\"x_panel\">\r\n");
      out.write("                            <div class=\"x_content\">\r\n");
      out.write("\r\n");
      out.write("                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile_left\" >\r\n");
      out.write("\r\n");
      out.write("                                    <div class=\"profile_img\">\r\n");
      out.write("\r\n");
      out.write("                                        <!-- end of image cropping -->\r\n");
      out.write("                                        <div id=\"crop-avatar\">\r\n");
      out.write("                                            <!-- Current avatar -->\r\n");
      out.write("                                            <div class=\"avatar-view\" title=\"Change the avatar\">\r\n");
      out.write("                                                <img src=\"");
      out.print(imageUrl);
      out.write("\" title=\"");
      out.print(userfullname);
      out.write("\">\r\n");
      out.write("                                            </div>\r\n");
      out.write("\r\n");
      out.write("                                            <!-- Loading state -->\r\n");
      out.write("                                            <div class=\"loading\" aria-label=\"Loading\" role=\"img\" tabindex=\"-1\"></div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                        <!-- end of image cropping -->\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("\r\n");
      out.write("                                    <h3></h3>\r\n");
      out.write("                                    <div >\r\n");
      out.write("                                        <ul class=\"list-unstyled user_data\">\r\n");
      out.write("                                            <li><i class=\"fa fa user-profile-icon\"></i>\r\n");
      out.write("                                            </li>\r\n");
      out.write("\r\n");
      out.write("                                            <li>\r\n");
      out.write("                                                <i class=\"fa fa-user\"></i>  ");
      out.print(userfullname);
      out.write("\r\n");
      out.write("                                            </li>\r\n");
      out.write("\r\n");
      out.write("                                            <li>\r\n");
      out.write("                                                <i class=\"fa fa-map-marker\"></i>   ");
      out.print(location);
      out.write("\r\n");
      out.write("                                            </li>\r\n");
      out.write("                                            <li >\r\n");
      out.write("                                                <i class=\"fa fa-envelope\"></i>  ");
      out.print(mail);
      out.write("\r\n");
      out.write("                                            </li>\r\n");
      out.write("                                            <li >\r\n");
      out.write("                                                <i class=\"fa fa-phone\"></i>  ");
      out.print(mobile);
      out.write("\r\n");
      out.write("                                            </li>\r\n");
      out.write("                                        </ul>\r\n");
      out.write("                                        <a class=\"btn btn-success\" href=\"/min/editProfile?username=");
      out.print(userId);
      out.write("\"><i class=\"fa fa-edit m-right-xs\"></i>Edit Profile</a>\r\n");
      out.write("                                        <br />\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <!-- start skills -->\r\n");
      out.write("\r\n");
      out.write("                                    <!-- end of skills-->\r\n");
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                                <div class=\"col-md-8 col-sm-9 col-xs-12\" id=\"searchResult\">\r\n");
      out.write("                                    <div class=\"\" >\r\n");
      out.write("                                        <div class=\"x_panel tile\">\r\n");
      out.write("                                            <div class=\"x_title  \">\r\n");
      out.write("                                                <h2>Requests</h2>\r\n");
      out.write("                                                <ul class=\"nav navbar-right panel_toolbox\">\r\n");
      out.write("                                                    <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\r\n");
      out.write("                                                    </li>\r\n");
      out.write("                                                    <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\r\n");
      out.write("                                                    </li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("                                                <div class=\"clearfix\"></div>\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                            <div class=\"x_content\" id=\"requestBlock\">\r\n");
      out.write("\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <!-- BEGIN SOCIAL ICONS -->\r\n");
      out.write("                <div class=\"col-md-6 col-sm-6\">\r\n");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"col-md-6 col-sm-6\">\r\n");
      out.write("                    <fieldset>\r\n");
      out.write("                        <legend>Tip:</legend>\r\n");
      out.write("                        <i class=\"fa fa-check\" style=\"color: green\"></i> Available\r\n");
      out.write("                        <i class=\"fa fa-question\" style=\"color: orange\"></i> Available but Some Negative Info\r\n");
      out.write("                        <i class=\"fa fa-close\" style=\"color: #ff0000\"></i> Not Available\r\n");
      out.write("                    </fieldset>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"col-md-6 col-sm-6\">\r\n");
      out.write("                    <ul class=\"product_social list-inline\">\r\n");
      out.write("                        ");
      out.write("\r\n");
      out.write("                        <li><a class=\"fa fa-facebook-square\" data-original-title=\"facebook\" href=\"https://www.facebook.com/maidinnigeria\" title=\"Like Us On Facebook\"></a></li>\r\n");
      out.write("                        <li><a class=\"fa fa-twitter-square\" data-original-title=\"twitter\" href=\"https://www.twitter.com/maidinnigeria\" title=\"Like Us On Twitter\"></a></li>\r\n");
      out.write("                        ");
      out.write("\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("                <!-- END SOCIAL ICONS -->\r\n");
      out.write("                <!-- BEGIN NEWLETTER -->\r\n");
      out.write("                ");
      out.write("\r\n");
      out.write("                <!-- END NEWLETTER -->\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("            <!-- footer content -->\r\n");
      out.write("            <div class=\"footer padding-top-15\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"row\">\r\n");
      out.write("                        <!-- BEGIN COPYRIGHT -->\r\n");
      out.write("                        <div class=\"col-md-6 col-sm-6 padding-top-10\">\r\n");
      out.write("                            2016 © MaidInNigeria. All Rights Reserved.\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <!-- END COPYRIGHT -->\r\n");
      out.write("                        <!-- BEGIN PAYMENTS -->\r\n");
      out.write("                        <div class=\"col-md-6 col-sm-6\">\r\n");
      out.write("\r\n");
      out.write("                            <ul class=\"list-unstyled list-inline pull-right margin-bottom-15\">\r\n");
      out.write("                                <!-- <li><img src=\"/frontend/assets/img/payments/western-union.jpg\" alt=\"We accept Western Union\" title=\"We accept Western Union\"></li>-->\r\n");
      out.write("                                ");
      out.write("\r\n");
      out.write("                            </ul>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <!-- END PAYMENTS -->\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!-- footer content -->\r\n");
      out.write("            <!--<footer>-->\r\n");
      out.write("            <div class=\"foot\">\r\n");
      out.write("                <p class=\"pull-right\">MaidInNigeria. |\r\n");
      out.write("                </p>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"clearfix\"></div>\r\n");
      out.write("            <!--</footer>-->\r\n");
      out.write("            <!-- /footer content -->\r\n");
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("        <!-- /page content -->\r\n");
      out.write("    </div>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">\r\n");
      out.write("    <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">\r\n");
      out.write("    </ul>\r\n");
      out.write("    <div class=\"clearfix\"></div>\r\n");
      out.write("    <div id=\"notif-group\" class=\"tabbed_notifications\"></div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<script src=\"asset/js/bootstrap.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<!-- chart js -->\r\n");
      out.write("<script src=\"asset/js/chartjs/chart.min.js\"></script>\r\n");
      out.write("<!-- bootstrap progress js -->\r\n");
      out.write("<script src=\"asset/js/progressbar/bootstrap-progressbar.min.js\"></script>\r\n");
      out.write("<script src=\"asset/js/nicescroll/jquery.nicescroll.min.js\"></script>\r\n");
      out.write("<!-- icheck -->\r\n");
      out.write("<script src=\"asset/js/icheck/icheck.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<script src=\"asset/js/custom.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<!-- moris js -->\r\n");
      out.write("<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"></script>\r\n");
      out.write("<script type=\"text/javascript\" src=\"asset/js/jquery-1.9.1.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<!-- PNotify -->\r\n");
      out.write("<script type=\"text/javascript\" src=\"asset/js/notify/pnotify.core.js\"></script>\r\n");
      out.write("<script type=\"text/javascript\" src=\"asset/js/notify/pnotify.buttons.js\"></script>\r\n");
      out.write("<script type=\"text/javascript\" src=\"asset/js/notify/pnotify.nonblock.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<script src=\"Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<link href=\"Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!-- Load Requested Maids-->\r\n");
      out.write("<script>\r\n");
      out.write("    $(document).ready(function(){\r\n");
      out.write("        $.ajax({\r\n");
      out.write("            type:'POST',\r\n");
      out.write("            url:'/min/requestedMaid?userId=");
      out.print(userId);
      out.write("',\r\n");
      out.write("\r\n");
      out.write("            success: function(response){\r\n");
      out.write("                var initial = 0;\r\n");
      out.write("                var jsonResponse = JSON.parse(response);\r\n");
      out.write("                while(jsonResponse.length > initial){\r\n");
      out.write("                    var occupation = jsonResponse[initial].occupation;\r\n");
      out.write("                    var workerName = jsonResponse[initial].workersName;\r\n");
      out.write("                    var worker_id = jsonResponse[initial].workerId;\r\n");
      out.write("                    var agency = jsonResponse[initial].agent;\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    $('#requestBlock').append('<div class=\"alert alert-info alert-dismissable\">' +\r\n");
      out.write("\r\n");
      out.write("                    '<img src=\"asset/images/user.png\" class=\"avatar\" alt=\"Avatar\">' +\r\n");
      out.write("\r\n");
      out.write("                    '<div class=\"message_wrapper\">' +\r\n");
      out.write("                    '<ul><li class=\"heading\">Min0'+worker_id+'</li>' +\r\n");
      out.write("                    '<li>'+occupation+'</li><li>'+agency+'</li></ul>' +\r\n");
      out.write("                    '<p class=\"url\">' +\r\n");
      out.write("                    '<a type=\"button\" class=\"btn btn-success\" data-toggle=\"tooltip\" data-placement=\"top\" href=\"/min/confirmHire?workerName='+worker_id+'\\\"\" >Hire</a>' +\r\n");
      out.write("                    '<a type=\"button\" class=\"btn btn-danger\" href=\"/min/remove?username=");
      out.print(userId);
      out.write("&maid='+worker_id+'\\\"\"  data-toggle=\"tooltip\" data-placement=\"right\">Remove</a>' +\r\n");
      out.write("                    '</p>' +\r\n");
      out.write("                    '</div>' +\r\n");
      out.write("                    '</div>');\r\n");
      out.write("\r\n");
      out.write("                    initial++;\r\n");
      out.write("\r\n");
      out.write("                }\r\n");
      out.write("            },\r\n");
      out.write("            error: function(){\r\n");
      out.write("\r\n");
      out.write("            }\r\n");
      out.write("        });\r\n");
      out.write("    });\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!--Search Engine -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("    NProgress.done();\r\n");
      out.write("</script>\r\n");
      out.write("<style>\r\n");
      out.write("    h1{\r\n");
      out.write("        text-transform: uppercase;\r\n");
      out.write("        text-decoration: solid cornsilk;\r\n");
      out.write("    }\r\n");
      out.write("</style>\r\n");
      out.write("<style>\r\n");
      out.write("    .foot{\r\n");
      out.write("        position:fixed;\r\n");
      out.write("        bottom: 0;\r\n");
      out.write("        right:0;\r\n");
      out.write("        padding-left: 5%;\r\n");
      out.write("    }\r\n");
      out.write("    #searchResult{\r\n");
      out.write("        padding: 0px;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    legend {\r\n");
      out.write("        display: block;\r\n");
      out.write("        padding-left: 2px;\r\n");
      out.write("        padding-right: 2px;\r\n");
      out.write("\r\n");
      out.write("    }\r\n");
      out.write("</style>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
