<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 5/25/2016
  Time: 2:47 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Maid In Nigeria | Domestic staffs, HouseHelp, Nanny, Chef, Maids in Lagos, Nigeria</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Maid, Househelp, HouseMaid, Driver's, Nanny, chef, gardener, security, housegirl, Lagos, Nigeria" />
    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic servant to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants(Maid, Househelp, Driver, Nanny, chef, gardener, security, housegirl, butler).">
    <meta name="author" content="Novatia">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="/min/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- pignose css -->
    <link href="/min/css/pignose.layerslider.css" rel="stylesheet" type="text/css" media="all" />


    <!-- //pignose css -->
    <link href="/min/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="/min/js/jquery-2.1.4.min.js"></script>
    <!-- //js -->
    <!-- cart -->
    <script src="js/simpleCart.min.js"></script>
    <!-- cart -->
    <!-- for bootstrap working -->
    <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
    <!-- //for bootstrap working -->
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
    <script src="/min/js/jquery.easing.min.js"></script>
</head>
<body>
<!-- header -->
<!-- <div class="header">
	<div class="container">
		<ul>
			<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Free and Fast Delivery</li>
			<li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Free shipping On all orders</li>
			<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">info@example.com</a></li>
		</ul>
	</div>
</div> -->
<!-- //header -->
<!-- header-bot -->
<div class="header-bot">
    <div class="container">
        <div class="col-md-3 header-left">
            <h1><a href="/min/index.html"><img src="/min/images/MINLogo.PNG"></a></h1>
        </div>
        <div class="col-md-6 header-middle">
            <form action="/min/search" method="get" id="form1">
                <div class="search">
                    <input type="search" name="search" value="Search for Domestic Staffs e.g House help, Housemaid, Steward, Cook, Driver e.t.c" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                </div>
                <!-- <div class="section_room">
                    <select id="country" onchange="change_country(this.value)" class="frm-field required">
                        <option value="null">All categories</option>
                        <option value="null">Electronics</option>
                        <option value="AX">kids Wear</option>
                        <option value="AX">Men's Wear</option>
                        <option value="AX">Women's Wear</option>
                        <option value="AX">Watches</option>
                    </select>
                </div> -->
                <div class="sear-sub">
                    <input type="submit" id="go" value="">
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        <div class="col-md-3 header-right footer-bottom">
            <ul>
                <li><a href="#" class="use1" data-toggle="modal" data-target="#myModal5"><span>Login</span></a>
                </li>
                <li style="color:#fda30e;"><a href="#"  data-toggle="modal" data-target="#myModal4"><span>Agent</span></a></li>
                <li><a class="fb" href="#"></a></li>
                <li><a class="twi" href="#"></a></li>

                <!--<li><a class="you" href="#"></a></li> -->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- //header-bot -->
<!-- banner -->
<div class="ban-top">
    <div class="container">
        <div class="top_nav_left">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <!-- <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav menu__list">
                        <li class="active menu__item menu__item--current"><a class="menu__link" href="index.jsp">Home <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">men's wear <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="mens.html"><img src="images/woo1.jpg" alt=" "/></a>
                                        </div>
                                        <div class="col-sm-3 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="mens.html">Clothing</a></li>
                                                <li><a href="mens.html">Wallets</a></li>
                                                <li><a href="mens.html">Footwear</a></li>
                                                <li><a href="mens.html">Watches</a></li>
                                                <li><a href="mens.html">Accessories</a></li>
                                                <li><a href="mens.html">Bags</a></li>
                                                <li><a href="mens.html">Caps & Hats</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="mens.html">Jewellery</a></li>
                                                <li><a href="mens.html">Sunglasses</a></li>
                                                <li><a href="mens.html">Perfumes</a></li>
                                                <li><a href="mens.html">Beauty</a></li>
                                                <li><a href="mens.html">Shirts</a></li>
                                                <li><a href="mens.html">Sunglasses</a></li>
                                                <li><a href="mens.html">Swimwear</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">women's wear <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-3 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="womens.html">Clothing</a></li>
                                                <li><a href="womens.html">Wallets</a></li>
                                                <li><a href="womens.html">Footwear</a></li>
                                                <li><a href="womens.html">Watches</a></li>
                                                <li><a href="womens.html">Accessories</a></li>
                                                <li><a href="womens.html">Bags</a></li>
                                                <li><a href="womens.html">Caps & Hats</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="womens.html">Jewellery</a></li>
                                                <li><a href="womens.html">Sunglasses</a></li>
                                                <li><a href="womens.html">Perfumes</a></li>
                                                <li><a href="womens.html">Beauty</a></li>
                                                <li><a href="womens.html">Shirts</a></li>
                                                <li><a href="womens.html">Sunglasses</a></li>
                                                <li><a href="womens.html">Swimwear</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="images/woo.jpg" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class=" menu__item"><a class="menu__link" href="electronics.html">Electronics</a></li>
                        <li class=" menu__item"><a class="menu__link" href="codes.html">Short Codes</a></li>
                        <li class=" menu__item"><a class="menu__link" href="contact.html">contact</a></li>
                      </ul>
                    </div> -->
                </div>
            </nav>
        </div>
        <!-- <div class="top_nav_right">
            <div class="cart box_1">
                        <a href="checkout.html">
                            <h3> <div class="total">
                                <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                                <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>

                            </h3>
                        </a>
                        <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>

            </div>
        </div> -->
        <div class="clearfix"></div>
    </div>
</div>
<!-- //banner-top -->
<!-- banner -->
<%--<div class="banner-grid">
    <div id="visual">
        <div class="slide-visual">
            <!-- Slide Image Area (1000 x 424) -->
            <ul class="slide-group">
                <li><img class="img-responsive" src="/min/images/chef.jpg" alt="Dummy Image" /></li>
                <li><img class="img-responsive" src="/min/images/cook.jpg" alt="Dummy Image" /></li>
                <li><img class="img-responsive" src="/min/images/clean.jpg" alt="Dummy Image" /></li>
                <li><img class="img-responsive" src="/min/images/Black_Car_Driver_Color.jpg" alt="Dummy Image" /></li>
            </ul>

            <!-- Slide Description Image Area (316 x 328) -->
            <!-- <div class="script-wrap">
                <ul class="script-group">
                    <li><div class="inner-script"><img class="img-responsive" src="images/baa1.jpg" alt="Dummy Image" /></div></li>
                    <li><div class="inner-script"><img class="img-responsive" src="images/baa2.jpg" alt="Dummy Image" /></div></li>
                    <li><div class="inner-script"><img class="img-responsive" src="images/baa3.jpg" alt="Dummy Image" /></div></li>
                </ul>
                <div class="slide-controller">
                    <a href="#" class="btn-prev"><img src="images/btn_prev.png" alt="Prev Slide" /></a>
                    <a href="#" class="btn-play"><img src="images/btn_play.png" alt="Start Slide" /></a>
                    <a href="#" class="btn-pause"><img src="images/btn_pause.png" alt="Pause Slide" /></a>
                    <a href="#" class="btn-next"><img src="images/btn_next.png" alt="Next Slide" /></a>
                </div>
            </div> -->
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript" src="/min/js/pignose.layerslider.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        $(window).load(function() {
            $('#visual').pignoseLayerSlider({
                play    : '.btn-play',
                pause   : '.btn-pause',
                next    : '.btn-next',
                prev    : '.btn-prev'
            });
        });
        //]]>
    </script>

</div>--%>
<!-- //banner -->
<!-- content -->

<div class="new_arrivals">
    <div class="container" id="searchResult">
        <c:forEach var="workers" items="${requestScope.workers}">
            <div class="col-md-7 col-sm-8 col-xs-10 animated fadeInDown">
            <div class="well profile_view">
            <div class="col-sm-12">
            <h4 class="brief"><i>MaidInNigeria</i></h4>
            <div class="left col-xs-7 ">
            <form >
            <h5><span id="name"></span> MIN 0 </h5>

            <strong></strong> <span >${workers.occupation}</span>
            <ul class="list-unstyled ">
            <li><span>  ${workers.location}</span></li>
            <li></i><span >  ${workers.sex}</span><span> ${workers.age} </span></li>
            <li></i><span >  ' +  guarantor + '</span></li>
            <li></i><span >  ' + medical + '</span></li>
            <li>Agency Fee: <span >  '+agencyFee +'</span></li>
            <li>Expected Salary: <span >  '+expectedSal +'</span></li>
            </ul>
            </form>
            </div>
            <div class="right col-xs-5 text-center">
            <img src="asset/images/user.png" alt="' + work + '\" class="img-circle img-responsive" title="'+name+'\">
            </div>
            </div>
            <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
            &lt;!&ndash;   <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
            &lt;!&ndash;</i> <i class="fa fa-comments-o"></i> </button>&ndash;&gt;
            <a class="request" title="Edit Details" href="/min/UserLogin.jsp">  <button type="button" data-toggle="tooltip" data-placement="bottom" title="Request Worker" class="btn btn-primary btn-xs" id="requestBtn"> <i class="fa fa-user">
            </i> Request </button></a>
            </div>
            </div>
            </div>
            </div>
        </c:forEach>
    </div>
</div>
<!-- //content -->

<!-- //content-bottom -->
<!-- product-nav -->

<div class="product-easy">
    <div class="container">

        <script src="/min/js/easyResponsiveTabs.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true   // 100% fit in a container
                });
            });

        </script>

    </div>
</div>
<!-- //product-nav -->


<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="col-md-3 footer-left">
            <h2><a href="/min/index.html"><img src="/min/images/MINLogo.PNG" alt=" " /></a></h2>
            <p>Maid in Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace.<a href="/min/about.jsp">Read More</a>.</p>
        </div>
        <div class="col-md-9 footer-right">
            <!-- <div class="col-sm-6 newsleft">
                <h3>SIGN UP FOR NEWSLETTER !</h3>
            </div>
            <div class="col-sm-6 newsright">
                <form>
                    <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Submit">
                </form>
            </div> -->
            <div class="clearfix"></div>
            <div class="sign-grds">
                <div class="col-md-4 sign-gd">
                    <h4>Information</h4>
                    <ul>
                        <li><a href="/min/index.html">Home</a></li>
                        <li><a href="/min/about.jsp">About</a></li>

                        <!-- <li><a href="electronics.html">Electronics</a></li>
                        <li><a href="codes.html">Short Codes</a></li> -->
                        <li><a data-toggle="modal" href="#myModal">Privacy Policy</a></li>
                    </ul>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" style="color: #fda30e;">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p style="color: #fda30e;">'All the information on this website is published in good-faith  and while we endeavor to keep the information up to date and correct we state that they have been provided by third
                                        parties and has not been independently verified and is  for general information purposes only, we do not make warranties about the completeness, reliability and accuracy of this information and any  action taken based on these information is strictly at your own risk. We are not liable for any losses and damages in connection with the use of our website'
                                        <br /><br />
                                        'In no event will we be liable for any loss or damage including without limitation,
                                        indirect or consequential loss or damage, or any loss or damage whatsoever including pecuniary financial losses or profit arising out of, or in connection with the use of this website.'
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4 sign-gd-two">
                    <h4>Office Information</h4>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : 147A Ogunlana drive, Surulere, <span>Lagos.</span></li>
                        <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:contact@maidinnigeria.com">contact@maidinnigeria.com</a></li>
                        <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +234 702 614 2888</li>
                    </ul>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <p class="copy-right">&copy 2017 MaidInNigeria. All rights reserved <!-- | Design by <a href="http://w3layouts.com/">W3layouts</a></p> -->
    </div>
</div>
<!-- //footer -->



<script>
	$(document).ready(function(){
            var initia = 0;
            $.ajax({

                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse != ""){
                    while(jsonResponse.length > initia) {

                        var age = jsonResponse[initia].age;
                        var sex = jsonResponse[initia].sex;
                        var work = jsonResponse[initia].work;
                        var agentName = jsonResponse[initia].agent;
                        var idno = jsonResponse[initia].id;
                        var location = jsonResponse[initia].location;
                        //var medical = jsonResponse[initia].medical;
                        var guarantor = 'Guarantor ' + '<i class="glyphicon glyphicon-ok"></i>';
                        var agencyFee = jsonResponse[initia].agencyFee;
                        var expectedSal = jsonResponse[initia].expectedSal;
                        var name = firstname ;


                        /*if(medical == 'none'){
                            medical = 'Medical Report ' + '<i class="glyphicon glyphicon-minus"></i>'
                        }else{
                            medical = 'Medical Report ' + '<i class="glyphicon glyphicon-ok"></i>'
                        }*/

                        $('#searchResult').append('<div class="col-md-7 col-sm-8 col-xs-10 animated fadeInDown">' +
                        '<div class="well profile_view">' +
                        '<div class="col-sm-12">' +
                        '<h4 class="brief"><i>MaidInNigeria</i></h4>' +
                        '<div class="left col-xs-7 ">' +
                        '<form >' +
                        '<h5><span id="name"></span> MIN 0' +idno + '</h5>' +

                        '<strong></strong> <span >' + work + '</span>' +
                        '<ul class="list-unstyled ">' +
                        '<li><span>  ' + location + '</span></li>' +
                        '<li></i><span >  ' + sex + ',</span><span> '+age+' </span></li>' +
                        '<li></i><span >  ' +  guarantor + '</span></li>' +
                        '<li></i><span >  ' + medical + '</span></li>' +
                        '<li>Agency Fee: <span >  '+agencyFee +'</span></li>' +
                        '<li>Expected Salary: <span >  '+expectedSal +'</span></li>' +
                        '</ul>' +
                        '</form>' +
                        '</div>' +
                        '<div class="right col-xs-5 text-center">' +
                        '<img src="asset/images/user.png" alt="' + work + '\"" class="img-circle img-responsive" title="'+name+'\"">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-12 bottom text-center">' +
                        '<div class="col-xs-12 col-sm-6 emphasis">' +
                        '&lt;!&ndash;   <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">' +
                        '&lt;!&ndash;</i> <i class="fa fa-comments-o"></i> </button>&ndash;&gt;' +
                        '<a class="request" title="Edit Details" href="/min/UserLogin.jsp">  <button type="button" data-toggle="tooltip" data-placement="bottom" title="Request Worker" class="btn btn-primary btn-xs" id="requestBtn"> <i class="fa fa-user">' +
                        '</i> Request </button></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');

                        initia++
                    }
                    }else{
                        $('#searchResult').append('<div style="padding-left: 45%;">No result met your search..Please try phrase like "Cook in lagos"</div>');
                    }
                },
                error: function(){
                        $('#searchResult').append("Could not find result for your search..");
                }
            })

    })

</script>
<script src="/min/Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="/min/Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="/min/Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">
<script>

    function displaySuccessMessage(message){
        jQuery.gritter.add({
            title: 'Authentication Status!',
            text: message,
            class_name: 'growl-success',
            image: '/min/asset/img/success.jpg',
            sticky: false,
            time: ''
        });
    }

    function displayErrorDialog(message){

        jQuery.gritter.add({
            title: 'Login Error!',
            text: message,
            class_name: 'growl-danger',
            image: '/min/asset/img/error.png',
            sticky: false,
            time: ''
        });

    }
    jQuery(document).ready(function(){

        $('#form3').submit(function(e){
            e.preventDefault();
        });


        $('#logon').click(function(){

            var loginform = $('#form3');
            formdata = loginform.serialize();

            $.ajax({
                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formdata,
                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        displaySuccessMessage("Welcome!!!");
                        window.location = "/min/AllWorkers.jsp";
                    }
                    else{
                        displayErrorDialog("Wrong Username/Password Combination");
                        $('#username').val("");
                        $('#password').val("")
                    }
                },
                error: function(jqXHR){
                    displayErrorDialog("Internal Server Error")
                }
            })
        })

    });
</script>
<script>

    jQuery(document).ready(function() {
        //var hook = false;
        $('#form2').submit(function (e) {
            e.preventDefault();
        });

        $('#signin').click(function () {
            var loginform = $('#form2');
            formData = loginform.serialize();

            //formData = "?username=" + userName + "&password="+ passWord;
            $.ajax({

                type: loginform.attr('method'),
                url: loginform.attr('action'),
                data: formData,
                //dataType:"json",

                success: function (response) {
                    var jsonResponse = JSON.parse(response);
                    if (jsonResponse.mark) {
                        displaySuccessMessage("Welcome");
                        window.location = "/min/UserHomePage.jsp";
                    }
                    else {
                        displayErrorDialog("Wrong Username/Password Combination");
                        $('#username').val('');
                        $('#password').val('');
                    }
                    //console.log(jsonResponse)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    displayErrorDialog("Internal Server Error");
                    console.log(jqXHR.statusText);

                }
            });


        });
    });
</script>
</body>
</html>

