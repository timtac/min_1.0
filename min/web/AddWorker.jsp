<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10/10/2015
  Time: 10:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    //confirm authentication
    if(session!=null) {
        String state = (String) session.getAttribute("login");
        if (state == null) {
            response.sendRedirect("/min/index.jsp");
        }
    }
    else{ response.sendRedirect("/min/AgentsLogin.jsp");}

    String name = (String) session.getAttribute("Agent_username");
    String company = (String) session.getAttribute("company");
    String editUrl = "/min/editAgentAction?username="+ name ;

    String imageName = (String) session.getAttribute("agentImage");
    String imageUrl;
    if(imageName != null){
        imageUrl = "/min/adminimagedownload?fileName="+ imageName;
    }else{
        imageUrl="asset/images/user.png";
    }
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | Register Maids, Nanny, Drivers, Chef, Security, Gardener..</title>

    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants.">
    <meta name="keywords" content="Maid in lagos, Driver, House Help, Nanny, Cleaner, Cook, Nigeria, Ogun, Abuja, Port Harcourt, Security, Gardener" />
    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="asset/custom/selfMade.js"></script>
    <script type="text/javascript" src="asset/js/custom.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">



                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3></h3>
                        <ul class="nav side-menu">

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->


            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="navbar-left">
                        <a href="/min/index.jsp"><img src="images/MINLogo2.PNG"></a>
                    </div>

                    <%--<div class="wrapper">--%>
                        <ul class="nav navbar-nav navbar-left menu">
                            <li >
                                <a href="/min/AllWorkers.jsp">Dashboard</a>
                            </li>

                            <li >
                                <a href="/min/AddWorker.jsp">Add Staff</a>
                            </li>
                            <li >
                                <a href="/min/AddGuarantor.jsp">Add Guarantor</a>
                            </li>
                            <li >
                                <a href="/min/BackgroundCheck.jsp">Background Check</a>
                            </li>
                            <li>
                                <a href="/min/mail.jsp">Contact Us</a>
                            </li>

                            <li>
                                <a href="/min/about.jsp">About Us</a>
                            </li>

                        </ul>
                    <%--</div>--%>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<%=imageUrl%>" alt=""><%=name%>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="<%=editUrl%>">Add Logo</a>
                                </li>
                                <li>
                                    <a href="/min/AgentHomePage.jsp">Profile</a>
                                </li>
                                <li>
                                    <a href="/min/about.jsp">About</a>
                                </li>
                                <li><a href="/min/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>ADD DOMESTIC STAFF</h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Househelp, Cook, Security, Driver, Cleaner e.t.c</h2>
                                <ul class="nav navbar-right panel_toolbox">

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <form class="form-horizontal form-label-left"  method="post" action="/min/adminaddnewmaids" role="form"  id="form1" >
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Full Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="firstname" required  id="first" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">Agent Company Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" required id="company" type="text" name="agentCompname" value="<%=company%>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="sex" class="radio" value="Male"> &nbsp; Male &nbsp;
                                            </label>
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="sex" value="Female" class="radio" > Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Age
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="age" name="age" data-validate-length-range="2" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Marital Status
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="relationship"  name="relationship" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Occupation</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="occupation">
                                            <option>Choose option</option>
                                            <option value="cleaner">Cleaner</option>
                                            <option value="driver">Driver</option>
                                            <option value="gate man">Gate Man</option>
                                            <option value="House Help">House Help</option>
                                            <option value="Nanny">Nanny</option>
                                        </select>
                                    </div>
                                </div>
                                <div>

                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input  class="form-control col-md-7 col-xs-12" id="location"  type="text" required name="location">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">Residential Address</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" type="text" id="address" required name="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">Religion</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" type="text" id="religion" required name="religion">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">Language(s)</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" type="text"  required name="language">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Physical-Challenge <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"   name="physical_challenge" id="physical" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-md-3 col-sm-3 col-xs-12">Educational-Level</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="education" class="form-control col-md-7 col-xs-12" type="text" required name="educationalLevel">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Expected Salary <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="expectedSalary" name="expectedSalary" data-validate-length-range="" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Agency Fee Type <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="radio" class="radio-inline" name="fee_type" onclick="annually()" value="Annual" id="fee_type"  ><label for="fee_type">Annual</label>
                                        <div class="reveal-if-active">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Agency Fee</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="number" id="agentFee" data-require-pair="#fee_type"  class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="radio" name="fee_type" class="radio-inline" value="Monthly" id="annual" onclick="monthly()"><label for="annual">Monthly</label>
                                        <div class="reveal-if-active">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Enter Percentage</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="number" id="percentage" data-require-pair="#annual"  onblur="calTotal()" class="form-control col-md-7 col-xs-12" placeholder="Enter Agency Monthly Percentage Of Staff's Salary">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Total</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="number" id="total" name="total" data-require-pair="#annual" data-validate-length-range="" readonly class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number"   name="phoneno" id="mobile" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Preference/Added Info.</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea required name="preference" cols="70"></textarea>
                                    </div>
                                </div>


                                <div class="form-group" style="padding-left: 60%">
                                    <a href="/min/AllWorkers.jsp" class="btn btn-danger">Back</a>
                                    <input type="reset" class="btn btn-info" value="Reset">
                                    <input   type="submit" class="btn btn-success submit" id="submit" value="OK" >

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- BEGIN SOCIAL ICONS -->
                <div class="col-md-6 col-sm-6">
                    <ul class="product_social list-inline">
                        <%--<li><a class="fa fa-rss-square" data-original-title="rss" href="#"></a></li>--%>
                        <li><a class="fa fa-facebook-square" data-original-title="facebook" href="https://www.facebook.com/maidinnigeria"></a></li>
                        <li><a class="fa fa-twitter-square" data-original-title="twitter" href="https://www.twitter.com/maidinnigeria"></a></li>

                    </ul>
                </div>
                <!-- END SOCIAL ICONS -->
                <!-- BEGIN NEWLETTER -->

                <!-- END NEWLETTER -->

            </div>
            <!-- footer content -->
            <div class="footer padding-top-15">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN COPYRIGHT -->
                        <div class="col-md-6 col-sm-6 padding-top-10">
                            2016 © MaidInNigeria. All Rights Reserved.
                        </div>
                        <!-- END COPYRIGHT -->
                        <!-- BEGIN PAYMENTS -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="list-unstyled list-inline pull-right margin-bottom-15">

                            </ul>
                        </div>
                        <!-- END PAYMENTS -->
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <!--<footer>-->
                <div class="foot">
                    <p class="pull-right">MaidInNigeria. |
                    </p>
                </div>
                <div class="clearfix"></div>
            <!--</footer>-->
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="asset/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="asset/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="asset/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="asset/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="asset/js/icheck/icheck.min.js"></script>
<!-- tags-->

<!-- PNotify -->
<script type="text/javascript" src="asset/js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.buttons.js"></script>
<script type="text/javascript" src="asset/js/notify/pnotify.nonblock.js"></script>

<!-- textarea resize -->
<script src="asset/js/textarea/autosize.min.js"></script>
<script>
    autosize($('.resizable_textarea'));
</script>
<!-- form validation -->
<script src="asset/js/validator/validator.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">


<script type="text/javascript">
    $(document).ready(function(){
        $('#form1').submit(function(e){
            e.preventDefault();
            return false;
        });
        $('#form1').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $('#submit').click(function(){
            var form = $('#form1');
            formData = form.serialize();

            $.ajax({
                type:form.attr('method'),
                url:form.attr('action'),
                data:formData,
                success: function(response){
                    $('#submit').attr('disabled', true)
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        displaySuccessMessage("Steward has been added..");
                        window.location = "/min/AddWorker.jsp";

                        //window.location = "/min/BackgroundCheck.jsp"
                    }else{
                        displayErrorDialog("Could not add steward...Phone number is unique to each staff");
                        $('#first').val("");
                        $('#last').val("");
                        $('#age').val("");
                        $('#occupation').val("");
                        $('#relationship').val("");
                        $('#mobile').val("");
                        $('fee_type').val("");
                        $('#location').val("");
                        $('#address').val("");
                        $('#religion').val("");
                        $('#company').val("");
                        $('#physical').val("");
                        $('#education').val("");
                        $('#agentFee').val("");
                        $('#expectedSalary').val("");
                        $('.radio').attr('checked',false)
                    }
                    $('#submit').attr('disabled', false)
                },
                error: function(jqXHR){
                    displayErrorDialog("Internal Server Error");
                    console.log(jqXHR.statusText);
                    $('#first').val("");
                    $('#last').val("");
                    $('#age').val("");
                    $('#occupation').val("");
                    $('#relationship').val("");
                    $('#mobile').val("");
                    $('#location').val("");
                    $('#address').val("");
                    $('fee_type').val("");
                    $('#religion').val("");
                    $('#company').val("");
                    $('#physical').val("");
                    $('#education').val("");
                    $('sex').deselect
                    $('#submit').attr('disabled', false)
                }
            })
        });

        function displaySuccessMessage(message){
            jQuery.gritter.add({
                title: 'Success!',
                text: message,
                class_name: 'growl-success',
                image: '/min/asset/img/success.jpg',
                sticky: false,
                time: ''
            });
        }

        function displayErrorDialog(message){

            jQuery.gritter.add({
                title: 'Error!',
                text: message,
                class_name: 'growl-light',
                image: '/min/asset/img/error.png',
                sticky: false,
                time: '60000'
            });

        }
    })
</script>

<script type="text/javascript">
    var permanotice, tooltip, _alert;
    $(function () {
        new PNotify({
            title: "TIPS",
            type: "info",
            text: "Domestic Workers will be added to your profile, make sure you enter your correct Company name to connect workers to your profile.",
            nonblock: {
                nonblock: true
            },
            before_close: function (PNotify) {
                // You can access the notice's options with this. It is read only.
                //PNotify.options.text="This is another";

                // You can change the notice's options after the timer like this:
                PNotify.update({
                    title: PNotify.options.title + " - Enjoy your Stay",
                    before_close: '1000'
                });
                PNotify.queueRemove();
                return false;
            }
        });

    });
</script>

<style>
    .foot{
        position:fixed;
        bottom: 0;
        right:0;
        padding-left: 5%;
    }
</style>
<script>
    NProgress.done();
</script>
</body>
</html>
