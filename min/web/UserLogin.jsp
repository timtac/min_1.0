<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10/10/2015
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Maid In Nigeria | User Login Page</title>
    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants.">
    <meta name="keywords" content="Maid in lagos, Driver, House Help, Nanny, Cleaner, Cook, Nigeria, Ogun, Abuja, Port Harcourt, Security, Gardener" />
    <meta name="description" content="Maid In Nigeria was created out of a need to provide verifiable, vetted and reliable domestic staff to the Nigerian populace. Maid In Nigeria helps to provide access to vetted domestic servants.">
    <!-- Bootstrap core CSS -->

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <link href="asset/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="asset/css/custom.css" rel="stylesheet">
    <link href="asset/css/icheck/flat/green.css" rel="stylesheet">
    <script src="asset/js/wizard/nerd.js"></script>


    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="background:#F7F7F7;">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
        <div id="login" class="animate form">
            <section class="login_content">
                <form method="post" action="/min/usersignIn" id="form1">
                    <h1>Sign In</h1>
                    <div>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <button class="btn btn-info submit" type="submit" id="logon">Log <span class="badge">In</span></button>
                        <a class="reset_pass" href="/min/reset.jsp">Lost your password?</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">


                        New to MaidInNigeria?   <a href="/min/UserReg.jsp" class="btn btn-primary"><span class="badge"> Create Account </span></a>

                        <div class="clearfix"></div>
                        <br />
                        <br />
                        <br />
                        <div>
                            <a href="#"><img src="images/MINLogo.PNG"></a>

                          <p>© 2017 All Rights Reserved. <a href="#" onclick="windo();">Privacy and Terms</a></p>
                        </div>
                    </div>
                </form>
                <!-- form -->

            </section>
            <!-- content -->





        </div>

    </div>

</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery-1.9.1.min.js"></script>

<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
<link href="Classic-Growl-like-Notification-Plugin-For-jQuery-Gritter/css/jquery.gritter.css" rel="stylesheet">

<script>

    jQuery(document).ready(function(){
        //var hook = false;
        $('#form1').submit(function(e){
            e.preventDefault();
        });

        $('#logon').click( function(){
            var loginform = $('#form1');
             formData = loginform.serialize();

           //formData = "?username=" + userName + "&password="+ passWord;
            $.ajax({

                type:loginform.attr('method'),
                url:loginform.attr('action'),
                data:formData,
                //dataType:"json",

                success: function(response){
                    var jsonResponse = JSON.parse(response);
                    if(jsonResponse.mark){
                        displaySuccessMessage("Welcome");
                        window.location = "/min/index.jsp";
                    }
                    else{
                        displayErrorDialog("Wrong Username/Password Combination");
                        $('#username').val('');
                        $('#password').val('');
                    }
                    //console.log(jsonResponse)
                },
                error: function (jqXHR,textStatus, errorThrown) {
                   displayErrorDialog("Internal Server Error");
                    console.log(jqXHR.statusText);

                }
            });


        });

        function displaySuccessMessage(message){
            jQuery.gritter.add({
                title: 'Authentication Status!',
                text: message,
                class_name: 'growl-success',
                image: '/min/asset/img/success.jpg',
                sticky: false,
                time: ''
            });
        }

        function displayErrorDialog(message){

            jQuery.gritter.add({
                title: 'Login Error!',
                text: message,
                class_name: 'growl-danger',
                image: '/min/asset/img/error.png',
                sticky: false,
                time: ''
            });

        }

    });

</script>

</body>
</html>
